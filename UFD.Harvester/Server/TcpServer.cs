﻿using System;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using NLog;

namespace UFD.Harvester.Server
{
    public class TcpServer
    {
        private Socket _listener;
        private readonly TcpServiceProvider _provider;
        private readonly ArrayList _connections;
        private int _maxConnections = 100;

        private readonly AsyncCallback _connectionReady;
        private readonly WaitCallback _acceptConnection;
        private readonly AsyncCallback _receivedDataReady;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <SUMMARY>
        /// Initializes server. To start accepting
        /// connections call Start method.
        /// </SUMMARY>
        public TcpServer(TcpServiceProvider provider, string address, int port)
        {
            _provider = provider;
            Port = port;
            Address = address;
            _listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            _connections = new ArrayList();
            _connectionReady = ConnectionReady_Handler;
            _acceptConnection = AcceptConnection_Handler;
            _receivedDataReady = ReceivedDataReady_Handler;
        }


        /// <SUMMARY>
        /// Start accepting connections.
        /// A false return value tell you that the port is not available.
        /// </SUMMARY>
        public bool Start()
        {
            try
            {
                _listener.Bind(new IPEndPoint(IPAddress.Parse(Address), Port));
                _listener.Listen(100);
                _listener.BeginAccept(_connectionReady, null);
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <SUMMARY>
        /// Callback function: A new connection is waiting.
        /// </SUMMARY>
        private void ConnectionReady_Handler(IAsyncResult ar)
        {
            lock (this)
            {
                if (_listener == null) return;
                var conn = _listener.EndAccept(ar);
                if (_connections.Count >= _maxConnections)
                {
                    //Max number of connections reached.
                    string msg = "SE001: Server busy";
                    conn.Send(Encoding.UTF8.GetBytes(msg), 0,
                        msg.Length, SocketFlags.None);
                    conn.Shutdown(SocketShutdown.Both);
                    conn.Close();
                }
                else
                {
                    //Start servicing a new connection
                    var st = new ConnectionState
                    {
                        _conn = conn,
                        _server = this,
                        _provider = (TcpServiceProvider) _provider.Clone(),
                        _buffer = new byte[4]
                    };
                    _connections.Add(st);
                    //Queue the rest of the job to be executed latter
                    ThreadPool.QueueUserWorkItem(_acceptConnection, st);
                }
                //Resume the listening callback loop
                _listener.BeginAccept(_connectionReady, null);
            }
        }


        /// <SUMMARY>
        /// Executes OnAcceptConnection method from the service provider.
        /// </SUMMARY>
        private void AcceptConnection_Handler(object state)
        {
            ConnectionState st = state as ConnectionState;
            try
            {
                if (st != null) st._provider.OnAcceptConnection(st);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                //report error in provider... Probably to the EventLog
            }
            //Starts the ReceiveData callback loop
            if (st != null && st._conn.Connected)
                st._conn.BeginReceive(st._buffer, 0, 0, SocketFlags.None, _receivedDataReady, st);
        }


        /// <SUMMARY>
        /// Executes OnReceiveData method from the service provider.
        /// </SUMMARY>
        private void ReceivedDataReady_Handler(IAsyncResult ar)
        {
            ConnectionState st = ar.AsyncState as ConnectionState;
            try
            {
                if (st != null) st._conn.EndReceive(ar);
            }
            catch (Exception)
            {
                // ignored
            }


            //Im considering the following condition as a signal that the
            //remote host droped the connection.
            if (st != null && st._conn.Available == 0) DropConnection(st);
            else
            {
                try {
                    if (st != null) st._provider.OnReceiveData(st);
                }
                catch
                {
                    //report error in the provider
                }
                //Resume ReceivedData callback loop
                if (st != null && st._conn.Connected)
                    st._conn.BeginReceive(st._buffer, 0, 0, SocketFlags.None,
                        _receivedDataReady, st);
            }
        }


        /// <SUMMARY>
        /// Shutdown the server
        /// </SUMMARY>
        public void Stop()
        {
            lock (this)
            {
                _listener.Close();
                _listener = null;
                //Close all active connections
                foreach (var st in from object obj in _connections select obj as ConnectionState)
                {
                    try
                    {
                        st._provider.OnDropConnection(st);
                    }
                    catch
                    {
                        //some error in the provider
                    }
                    if (st == null) continue;
                    st._conn.Shutdown(SocketShutdown.Both);
                    st._conn.Close();
                }
                _connections.Clear();
            }
        }

        /// <SUMMARY>
        /// Removes a connection from the list
        /// </SUMMARY>
        internal void DropConnection(ConnectionState st)
        {
            lock (this)
            {
                try
                {
                    st._conn.Shutdown(SocketShutdown.Both);
                    st._conn.Close();
                }
                catch
                {
                    // ignored
                }
                if (_connections.Contains(st))
                    _connections.Remove(st);
            }
        }

        public int MaxConnections
        {
            get
            {
                return _maxConnections;
            }
            set
            {
                _maxConnections = value;
            }
        }

        public int CurrentConnections
        {
            get
            {
                lock (this)
                {
                    return _connections.Count;
                }
            }
        }

        public string Address { get; private set; }

        public int Port { get; private set; }

        public ArrayList GetConnections
        {
            get
            {
                lock (this)
                {
                    return _connections;
                }
            }
        }
    }
}
