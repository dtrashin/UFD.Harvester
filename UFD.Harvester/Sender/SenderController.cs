﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using NLog;
using UFD.Harvester.Core.BL;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Server;

namespace UFD.Harvester.Sender
{
    public class SenderController
    {
        private static TcpServer _server;
        private readonly SenderConfiguration _config;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly Pack _pack = new Pack();
        private readonly Timer _timer = new Timer();

        public SenderController(ZoneActionController zoneActionController)
        {
            TcpServiceProvider provider = new SenderServiceProvider();
            _config = SenderConfiguration.GetConfig();
            _server = new TcpServer(provider, _config.Host, _config.Port);

            zoneActionController.ZoneActionSend += SendZoneAction;
            zoneActionController.DatagramSend += SendDatagram;

            if (_config.SendTimeout > 0)
            {
                _timer.Interval = _config.SendTimeout;
                _timer.Elapsed += OnTimerElapsed;
                _timer.AutoReset = true;
            }
        }

        public void Start()
        {
            _server.Start();
            _logger.Info("Sender is started on {0} port {1}", _server.Address, _server.Port);
        }


        private void SendZoneAction(object sender, ZoneActionSendEventArgs e)
        {
            _pack.Add(e.Acts);
            if (_config.SendTimeout == 0)
            {
                if (_server.CurrentConnections > 0)
                    foreach (var st in from object obj in _server.GetConnections select obj as ConnectionState into st where !st.Write(_pack.ToBuffer(), 0, _pack.GetSize()) select st)
                    {
                        Console.WriteLine("Faild to wrait -> " + st.RemoteEndPoint);
                    }
                _pack.Clear();
            }
        }

        private void SendDatagram(object sender, DatagramSendEventArgs e)
        {
            byte[] data = BaseDatagram.Serialize(e.Datagram);
            ushort size = sizeof(ushort);
            size += GetStringByteLength("Datagrams");
            size += GetStringByteLength(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            size += (ushort)(data.Length);
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(ms))
                {
                    var sz = BitConverter.GetBytes(size);
                    Array.Reverse(sz);
                    writer.Write(sz);

                    var type = GetStringBytes("Datagrams");
                    writer.Write(type);

                    var dt = GetStringBytes(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    writer.Write(dt);

                    writer.Write(data);
                }
                buffer = ms.ToArray();
            }
            if (_server.CurrentConnections <= 0) return;
            foreach (var st in from object obj in _server.GetConnections select obj as ConnectionState into st where !st.Write(buffer, 0, size) select st)
            {
                Console.WriteLine("Faild to wrait -> " + st.RemoteEndPoint);
            }
        }

        private static ushort GetStringByteLength(string str)
        {
            ushort size = 4;
            size += (ushort)(str.Length * sizeof(char));
            return size;
        }

        private static byte[] GetStringBytes(string str)
        {
            var bytes = new byte[str.Length * sizeof(char) + sizeof(int)];
            var strBytes = Encoding.BigEndianUnicode.GetBytes(str.ToCharArray());
            var size = BitConverter.GetBytes((str.Length * sizeof(char)));
            Array.Reverse(size);
            Buffer.BlockCopy(size, 0, bytes, 0, size.Length);
            Buffer.BlockCopy(strBytes, 0, bytes, size.Length, strBytes.Length);
            return bytes;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (_server.CurrentConnections <= 0) return;
            foreach (ConnectionState conn in _server.GetConnections)
            {
                conn.Write(_pack.ToBuffer(), 0, _pack.GetSize());
            }
            _pack.Clear();
        }


        private class Pack
        {
            private readonly List<ZoneAction> _acts = new List<ZoneAction>();

            public void Add(List<ZoneAction> acts)
            {
                _acts.AddRange(acts);
            }

            public void Clear()
            {
                _acts.Clear();
            }

            public ushort GetSize()
            {
                ushort size = sizeof(ushort);
                size += GetStringByteLength("Actions");
                size += GetStringByteLength(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                size += (ushort)(_acts.Count * sizeof(int));
                return size;
            }

            public byte[] ToBuffer()
            {
                using (var ms = new MemoryStream())
                {
                    using (BinaryWriter writer = new BinaryWriter(ms))
                    {
                        var size = BitConverter.GetBytes(GetSize());
                        Array.Reverse(size);
                        writer.Write(size);

                        var type = GetStringBytes("Actions");
                        writer.Write(type);

                        var dt = GetStringBytes(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        writer.Write(dt);

                        var points = _acts.Select(a => a.X).ToList();
                        foreach (int p in points)
                        {
                            var buff = BitConverter.GetBytes(p);
                            Array.Reverse(buff);
                            writer.Write(buff);
                        }
                    }
                    return ms.ToArray();
                }
            }


            private byte[] GetStringBytes(string str)
            {
                byte[] bytes = new byte[str.Length * sizeof(char) + sizeof(int)];
                byte[] strBytes = Encoding.BigEndianUnicode.GetBytes(str.ToCharArray());
                var size = BitConverter.GetBytes((str.Length * sizeof(char)));
                Array.Reverse(size);
                Buffer.BlockCopy(size, 0, bytes, 0, size.Length);
                Buffer.BlockCopy(strBytes, 0, bytes, size.Length, strBytes.Length);
                return bytes;
            }

            private ushort GetStringByteLength(string str)
            {
                ushort size = 4;
                size += (ushort)(str.Length * sizeof(char));
                return size;
            }
        }
    }
}
