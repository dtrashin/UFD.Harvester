﻿using NLog;
using UFD.Harvester.Server;

namespace UFD.Harvester.Sender
{
    class SenderServiceProvider : TcpServiceProvider
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public override object Clone()
        {
            return new SenderServiceProvider();
        }

        public override void OnAcceptConnection(ConnectionState state)
        {
            _logger.Trace("Accept connection from {0}", state.RemoteEndPoint);
        }

        public override void OnReceiveData(ConnectionState state)
        {
        }

        public override void OnDropConnection(ConnectionState state)
        {
            _logger.Trace("Drop connection from {0}", state.RemoteEndPoint);
        }
    }
}
