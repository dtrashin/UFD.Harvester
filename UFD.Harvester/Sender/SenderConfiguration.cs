﻿using System.Configuration;

namespace UFD.Harvester.Sender
{
    class SenderConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Port", IsRequired = true)]
        public int Port
        {
            get { return (int)this["Port"]; }
            set { this["Port"] = value; }
        }

        [ConfigurationProperty("Host", IsRequired = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        [ConfigurationProperty("SendTimeout", IsRequired = true)]
        public int SendTimeout
        {
            get { return (int)this["SendTimeout"]; }
            set { this["SendTimeout"] = value; }
        }

        public static SenderConfiguration GetConfig()
        {
            SenderConfiguration config = null;
            try
            {
                var execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config = (SenderConfiguration)execonfig.Sections["Harvester.Sender"];
            }
            catch
            {
                // ignored
            }

            return config;
        }
    }
}
