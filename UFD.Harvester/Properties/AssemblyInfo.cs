﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UFD.Harvester")]
[assembly: AssemblyDescription("Сервер")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Уникальные волокновые приборы")]
[assembly: AssemblyProduct("UFD.Harvester")]
[assembly: AssemblyCopyright("UFD Copyright ©  2016")]
[assembly: AssemblyTrademark("UF Harvester")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("470cb2e9-dc19-41ad-9f56-cc44900fb303")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.2.*")]
[assembly: AssemblyFileVersion("1.0.2.100")]
