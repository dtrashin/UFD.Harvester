﻿using System.Configuration;

namespace UFD.Harvester.Hub
{
    public class HubConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Port", IsRequired = true)]
        public int Port
        {
            get { return (int)this["Port"]; }
            set { this["Port"] = value; }
        }

        [ConfigurationProperty("Host", IsRequired = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        public static HubConfiguration GetConfig()
        {
            HubConfiguration config = null;
            try
            {
                var execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config = (HubConfiguration)execonfig.Sections["Harvester.Hub"];
            }
            catch
            {
                // ignored
            }
            return config;
        }
    }
}
