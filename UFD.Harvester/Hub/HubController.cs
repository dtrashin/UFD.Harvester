﻿using System;
using System.Threading;
using NLog;
using UFD.Harvester.Receivers;
using UFD.Harvester.Server;

namespace UFD.Harvester.Hub
{
    public class HubController
    {
        private static TcpServer _server;
        private static HubServiceProvider _provider;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public HubController()
        {
            _provider = new HubServiceProvider();
            var config = HubConfiguration.GetConfig();
            _server = new TcpServer(_provider, config.Host, config.Port);
        }

        public void Start()
        {
            _server.Start();
            _logger.Info("Hub is started on {0} port {1}", _server.Address, _server.Port);
            new Thread(CheckerStart).Start();
        }

        private void CheckerStart()
        {
            try
            {
                var checker = new CheckerController();
                checker.Start();
                checker.DatagramChecked += _provider.CheckData;
                checker.LineBroken += _provider.LineBroken;
            }
            catch (Exception ex)
            {
                _logger.Error("CheckerController: " + ex.Message);
            }
        }
    }
}
