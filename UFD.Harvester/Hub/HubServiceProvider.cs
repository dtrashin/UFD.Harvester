﻿using System.Collections.Generic;
using NLog;
using UFD.Harvester.Core.BL;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.DAL;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Receivers;
using UFD.Harvester.Receivers.Events;
using UFD.Harvester.Server;

namespace UFD.Harvester.Hub
{
    public class HubServiceProvider : TcpServiceProvider
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        //private readonly StatusController _statusController = StatusController.GetController();
        private readonly StateController _stateController = StateController.GetController();
        private static readonly IDataFacade Facade = DataFacadeFactory.GetDataFacade();

        public override object Clone()
        {
            return new HubServiceProvider();
        }

        public override void OnAcceptConnection(ConnectionState state)
        {
            _logger.Trace("Accept connection from {0}", state.RemoteEndPoint);
        }

        public override void OnReceiveData(ConnectionState state)
        {
            while (state.AvailableData > 0)
            {
                byte[] buffer = new byte[state.AvailableData];
                state.Read(buffer, 0, state.AvailableData);
                var datagram = BaseDatagram.Deserialize<Datagram>(buffer);
                if (datagram != null)
                {
                    _stateController.CheckDatagram(datagram);
                }
            }
        }

        public override void OnDropConnection(ConnectionState state)
        {
            _logger.Trace("Drop connection to {0}", state.RemoteEndPoint);
        }

        public void CheckData(object sender, DatagramReceivedEventArgs e)
        {
            if (e.Datagram != null)
            {
                _stateController.CheckDatagram(e.Datagram);
            }
        }

        public void LineBroken(object sender, LineBrokenEventArgs e)
        {
            _stateController.CheckDatagram(new Datagram() {DatagramType = 1, Impacts = { new Impact() {Start = e.X1, Stop = e.X2, Point = e.X1, Distance = 1} }, ReceiverId = e.Id});
            var systemAction = new SystemAction
            {
                Action = "Обрыв",
                Subject = "Прибор",
                Object = "Линия",
                Description = $"Координата обрыва {e.X1}"
            };

            Facade.LogSystemAction(systemAction);
        }
    }
}
