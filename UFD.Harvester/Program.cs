﻿using System;
using System.Linq;
using System.Threading;
using Nancy.Hosting.Self;
using NLog;
using UFD.Harvester.Core.BL;
using UFD.Harvester.Hub;
using UFD.Harvester.Sender;
using UFD.Harvester.Service;
using System.Collections.Generic;

namespace UFD.Harvester
{
    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main()
        {
            try
            {
                new Thread(HubStart).Start();
                new Thread(ServiceStart).Start();
                new Thread(TransmitterStart).Start();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
            Console.ReadLine();
        }

        private static void ServiceStart()
        {
            try
            {
                var config = ServiceConfiguration.GetConfig();
                var hostList = new List<string>();
                var uriList = new List<Uri>();
                for (int i = 0; i < config.NancyServices.Count; i++)
                {
                    uriList.Add(new Uri(config.NancyServices[i].Host));
                    hostList.Add(config.NancyServices[i].Host);
                }

                var conf = new HostConfiguration { AllowAuthorityFallback = true };
                
                using (var host = new NancyHost(conf, uriList.ToArray()))
                {
                    host.Start();
                    Logger.Info("Service is started on {0}", string.Join("; ", hostList));
                    Thread.Sleep(Timeout.Infinite);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Service: " + ex.Message);
            }
        }
        private static void TransmitterStart()
        {
            try
            {
                var senderController = new SenderController(ZoneActionController.GetController());
                senderController.Start();
                Thread.Sleep(Timeout.Infinite);
            }
            catch (Exception ex)
            {
                Logger.Error("Transmitter: " + ex.Message);
            }
        }
        private static void HubStart()
        {
            try
            {
                var hubController = new HubController();
                hubController.Start();
                Thread.Sleep(Timeout.Infinite);
            }
            catch (Exception ex)
            {
                Logger.Error("Hub: " + ex.Message);
            }
        }
    }
}
