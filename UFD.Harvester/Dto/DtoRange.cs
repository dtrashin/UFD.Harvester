﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFD.Harvester.Receivers;

namespace UFD.Harvester.Dto
{
    /// <summary>
    /// Отрезок контролируемой линии
    /// </summary>
    public class DtoRange
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Идентификатор ресивера
        /// </summary>
        public Guid ReceiverId { get; set; }

        /// <summary>
        /// Начало зоны контроля
        /// </summary>
        public int X1 { get; set; }

        /// <summary>
        /// Конец зоны контроля
        /// </summary>
        public int X2 { get; set; }

        /// <summary>
        /// Контралер
        /// </summary>
        public DtoChecker Checker { get; set; }

        public DtoRange() { }

        public DtoRange(Range range)
        {
            Id = range.Id;
            ReceiverId = range.ReceiverId;
            X1 = range.X1;
            X2 = range.X2;
            Checker = range.Checker == null ? null : new DtoChecker(range.Checker);
        }
    }
}
