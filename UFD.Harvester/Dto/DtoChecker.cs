﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFD.Harvester.Receivers;

namespace UFD.Harvester.Dto
{
    /// <summary>
    /// Контралер
    /// </summary>
    public class DtoChecker
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Идентификатор ресивера
        /// </summary>
        public Guid ReceiverId { get; set; }

        /// <summary>
        /// Начло зоны контроля
        /// </summary>
        public int X1 { get; set; }

        /// <summary>
        /// Конец зоны контроля
        /// </summary>
        public int X2 { get; set; }

        /// <summary>
        /// Разворот координат
        /// </summary>
        public bool Reverse { get; set; }

        public DtoChecker() { }

        public DtoChecker(Checker checker)
        {
            Id = checker.Id;
            ReceiverId = checker.ReceiverId;
            X1 = checker.X1;
            X2 = checker.X2;
            Reverse = checker.Reverse;
        }
    }
}
