﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace UFD.Harvester.Service
{
    /// <summary>
    /// Класс результата. Результат любого метода веб-сервиса должен быть этого типа. Целевой результат - это T.
    /// </summary>
    /// <typeparam name="T">Класс результата метода бизнес-логики</typeparam>
    public class Result<T>
    {
        /// <summary>
        /// Признак, что метод выполнился успешно
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Результат выполнения метода
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Строка с ошибкой или предупреждением
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Трассировка стека для отладки
        /// </summary>
        [JsonIgnore]
        public string StackTrace { get; set; }

        /// <summary>
        /// Словарь с логическими ошибками. Ключ - код ошибки (номер, путь в объекте), значение - список информационных сообщений
        /// </summary>
        [JsonIgnore]
        public Dictionary<string, List<string>> Errors { get; set; }
    }
}
