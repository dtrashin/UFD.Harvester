﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.IO;
using Nancy.ModelBinding.DefaultBodyDeserializers;
using Nancy.TinyIoc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UFD.Harvester.Core.BL;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.Helpers;
using UFD.Harvester.Dto;
using UFD.Harvester.Receivers;

namespace UFD.Harvester.Service
{
    public class ServiceModule : NancyModule
    {
        public class Bootstrapper : DefaultNancyBootstrapper
        {
            protected virtual NancyInternalConfiguration InteranalConfiguration => NancyInternalConfiguration.Default;

            protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
            {
                pipelines.AfterRequest.AddItemToEndOfPipeline(ctx =>
                {
                    ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                        .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                        .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
                });
                base.RequestStartup(container, pipelines, context);
            }
        }

        public ServiceModule()
        {
            Get["/"] = parameters => $"{DateTime.Now} Welcome to harvester service..." ;

            Post["/Login"] = parameters => Login(Request.Body);
            Post["/UserStatus"] = parameters => UserStatus(Request.Body);
            Post["/Logout"] = parameters => Logout(Request.Body);

            Post["/GetRegions"] = parameters => GetRegions(Request.Body);
            Post["/GetZones"] = parameters => GetZones(Request.Body);
            //Get["/GetGeoZones"] = parameters => GetGeoZones();

            Post["/GetRegionStatus"] = parameters => GetRegionStatus(Request.Body);
            Post["/SetRegionStatus"] = parameters => SetRegionStatus(Request.Body);

            Post["/GetZoneStatus"] = parameters => GetZoneStatus(Request.Body);
            Post["/SetZoneStatus"] = parameters => SetZoneStatus(Request.Body);

            Post["/GetActions"] = parameters => GetActions(Request.Body);
            Post["/GetActionsHistory"] = parameters => GetActionsHistory(Request.Body);

            Post["/ConfirmAction"] = parameters => ConfirmAction(Request.Body);
            //Post["/ActionConfirmationLog"] = parameters => "/ActionConfirmationLog";

            Post["/GetSystemLog"] = parameters => GetSystemActions(Request.Body);
            Post["/SystemActions/Log"] = parameters => LogSystemActions(Request.Body);
            Post["/SystemActions/Get"] = parameters => GetSystemActions(Request.Body);

            Post["/GetAll"] = parameters => GetAll(Request.Body);

            Post["/CortexState"] = parameters => CortexState(Request.Body);
            //Post["Sectors/Get"] = parameters => GetSectors(Request.Body);
            //Post["Sectors/Update"] = parameters => UpdateSector(Request.Body);
            //Post["Sectors/Delete"] = parameters => DeleteSector(Request.Body);

            //Post["Barriers/Get"] = parameters => GetBarriers(Request.Body);
            //Post["Barriers/Update"] = parameters => UpdateBarrier(Request.Body);
            //Post["Barriers/Delete"] = parameters => DeleteBarrier(Request.Body);

            
            Post["/Integration/GetZones"] = parameters => IntegrationGetZones(Request.Body);
            Post["/Integration/GetZoneStatus"] = parameters => IntegrationGetZoneState(Request.Body);
            Post["/Integration/SetZoneStatus"] = parameters => IntegrationSetZoneState(Request.Body);
            Post["/Integration/GetActions"] = parameters => IntegrationGetActions(Request.Body);

            Post["/Administration/GetUserSessions"] = parameters => AdministrationGetUserSessions(Request.Body);
            Post["/Administration/GetZones"] = parameters => AdministrationGetZones(Request.Body);
            Post["/Administration/GetZoneDetails"] = parameters => AdministrationGetZone(Request.Body);
            Post["/Administration/UpdateZone"] = parameters => AdministrationUpdateZone(Request.Body);

            Post["/Administration/GetRange"] = parameters => AdministrationGetRange(Request.Body);
            Post["/Administration/SetRange"] = parameters => AdministrationSetRange(Request.Body);
            //Post["/Administration/SetDamper"] = parameters => AdministrationSetDamper(Request.Body);

        }
        private string AdministrationGetRange(RequestStream body)
        {
            var result = new Result<List<DtoRange>>();
            var jBody = body.GetJObject();
            var userId = jBody.GetUserId();
            var rangeId = jBody.GetRangeId();

            if (!SecurityController.GetController().CheckUserAdmin(userId))
            {
                result.Success = false;
                result.Message = "Error: access denied.";
            }
            else
            {
                var ranges = SecurityController.GetController().GetRanges(rangeId);
                result.Data = ranges.Select(r => new DtoRange(r)).ToList();
                result.Success = true;

            }
            return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
        }

        private string AdministrationSetRange(RequestStream body)
        {
            var result = new Result<DtoRange>();
            var jBody = body.GetJObject();
            var userId = jBody.GetUserId();
            if (!SecurityController.GetController().CheckUserAdmin(userId))
            {
                result.Success = false;
                result.Message = "Error: access denied.";
            }
            else
            {
                try
                {
                    Checker checker = null;
                    var jRange = jBody["Range"];
                    Guid rangeId;
                    if (!Guid.TryParse(jRange["Id"]?.ToString() ?? string.Empty, out rangeId))
                        rangeId = Guid.NewGuid();
                    var jChecker = jRange["Checker"];
                    if (jChecker != null && (jChecker.ToString() != "null" && jChecker.ToString() != string.Empty))
                    {
                        Guid checkerReceiverId;
                        if (!Guid.TryParse(jChecker["ReceiverId"]?.ToString() ?? string.Empty, out checkerReceiverId))
                            throw new Exception("Error: Checker receiver Id incorrect.");
                        Guid checkerId;
                        if (!Guid.TryParse(jChecker["Id"]?.ToString() ?? string.Empty, out checkerId))
                            checkerId = Guid.NewGuid();
                        checker = new Checker()
                        {
                            Id = checkerId,
                            ReceiverId = checkerReceiverId,
                            X1 = (int)jChecker["X1"],
                            X2 = (int)jChecker["X2"],
                            Reverse = (bool)jChecker["Reverse"]
                        };
                    }
                    Guid receiverId;
                    if (!Guid.TryParse(jRange["ReceiverId"]?.ToString() ?? string.Empty, out receiverId))
                        throw new Exception("Error: Range receiver Id incorrect.");

                    if (rangeId == default(Guid))
                        rangeId = Guid.NewGuid();
                    if (checker != null) checker.CheckedId = rangeId;
                    var range = new Range(checker)
                    {
                        Id = rangeId,
                        ReceiverId = receiverId,
                        X1 = (int)jRange["X1"],
                        X2 = (int)jRange["X2"]
                    };

                    range = SecurityController.GetController().SetRange(range);

                    result.Success = true;
                    result.Data = new DtoRange(range);
                }
                catch (Exception exception)
                {
                    result.Success = false;
                    result.Message = exception.Message;
                }
            }
            return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
        }

        private string AdministrationGetUserSessions(RequestStream body)
        {
            var result = new Result<List<User>>();
            var jBody = body.GetJObject();
            var userId = jBody.GetUserId();
            if (!SecurityController.GetController().CheckUserAdmin(userId))
            {
                result.Success = false;
                result.Message = "Error: access denied.";
            }
            else
            {
                result.Success = true;
                result.Data = SecurityController.GetController().GetUsers();
            }
            return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
                
        }
        private string AdministrationGetZones(RequestStream body)
        {
            var result = new Result<List<Zone>>();
            try
            {
                var jBody = body.GetJObject();
                var userId = jBody.GetUserId();
                var zones = StateController.GetController().GetZones();

                result.Success = true;
                result.Data = zones;
            }
            catch (Exception exception)
            {
                result.Success = false;
                result.Message = exception.Message;
            }

            //var settings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Auto };
            //return JObject.Parse(JsonConvert.SerializeObject(result, Formatting.Indented, settings)).ToString();
            return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
        }

        private string AdministrationGetZone(RequestStream body)
        {
            var result = new Result<Zone>();
            try
            {
                var jBody = body.GetJObject();
                var userId = jBody.GetUserId();
                var zoneId = jBody.GetZoneId();
                var zone = StateController.GetController().GetZones().FirstOrDefault(z => z.Id == zoneId);
                if (zone == null)
                    throw new Exception($"Зона с идентификатором {zoneId} не найдена.");
                result.Success = true;
                result.Data = zone;
            }
            catch (Exception exception)
            {
                result.Success = false;
                result.Message = exception.Message;
            }
            return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();

        }

        private string AdministrationUpdateZone(RequestStream body)
        {
            var result = new Result<int>();
            var stateController = StateController.GetController();
            try
            {
                var jBody = body.GetJObject();
                var userId = jBody.GetUserId();
                //var zoneId = jBody.GetZoneId();
                var zone = (Zone)jBody.GetEntity(typeof(Zone));
                if (!stateController.UpdateZone(zone)) 
                    throw new Exception("Error update zone.");
                result.Success = true;
                result.Data = zone.Id;
            }
            catch (Exception exception)
            {
                result.Message = exception.Message;
                result.Success = false;
            }
            return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
        }


        private string LogSystemActions(Stream body)
        {

            var result = new Result<SystemAction>();
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    throw new Exception("Invalid parameters.");
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                if (!Guid.TryParse(strUserId, out userId))
                    throw new Exception("Invalid parameters UserId.");
                //string strSystemAction = jBody["SystemAction"] != null ? jBody["SystemAction"].ToString() : "";
                //if (string.IsNullOrEmpty(strUserId) || string.IsNullOrEmpty(strSystemAction))
                //    throw new Exception("Invalid parameters.");

                ////var systemAction = JsonConvert.DeserializeObject<SystemAction>(strSystemAction);
                //var jSystemAction = JObject.Parse(strSystemAction);
                //if (jSystemAction == null)
                //    return "Error: invalid parameter SystemAction.";
                var systemActionLogged = SecurityController.GetController().LogSystemAction(userId, "");
                result.Success = true;
                result.Data = systemActionLogged;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

        private string GetSystemActions(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameters.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                var from = jBody["From"]?.ToString() ?? "";
                var to = jBody["To"]?.ToString() ?? "";
                if (string.IsNullOrEmpty(strUserId) || string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to))
                    return "Error: invalid parameters.";

                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var systemActions = SecurityController.GetController().GetSystemActions(from, to);
                var arr = JArray.Parse(JsonConvert.SerializeObject(systemActions));
                var obj = new JObject { { "SystemLog", arr } };
                return obj.ToString();

            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private static string ConfirmAction(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var strActionId = jBody["ActionId"]?.ToString() ?? "";
                Guid actionId;
                if (!Guid.TryParse(strActionId, out actionId))
                    return "Error: invalid parameter ActionId.";
                var strConfirmedStatus = jBody["Confirmed"]?.ToString() ?? "";
                bool confirmedStatus;
                if (!bool.TryParse(strConfirmedStatus, out confirmedStatus))
                    confirmedStatus = false;
                var confirmActionStatus = SecurityController.GetController().ConfirmAction(actionId, confirmedStatus);
                var obj = new JObject { { "Confirmed", new JValue(confirmActionStatus) } };
                return obj.ToString();
            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private static string GetRegions(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var arr = JArray.Parse(JsonConvert.SerializeObject(StateController.GetController().GetRegions()));
                var obj = new JObject { { "Regions", arr } };
                return obj.ToString();
            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private static string GetZones(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var arr = JArray.Parse(JsonConvert.SerializeObject(StateController.GetController().GetZones()));
                var obj = new JObject { { "Zones", arr } };
                return obj.ToString();
            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string GetRegionStatus(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var strRegionId = jBody["RegionId"]?.ToString() ?? "";
                int regionId;
                if (!int.TryParse(strRegionId, out regionId))
                    return "Error: invalid parameters RegionId.";
                var statuses = StateController.GetController().GetRegionStatus(regionId);
                var arr = new JArray();
                foreach (var obj in statuses.Select(kvp => new JObject { { "RegionId", new JValue(kvp.Key) }, { "Status", new JValue(kvp.Value) } }))
                {
                    arr.Add(obj);
                }

                var jobj = new JObject { { "RegionStatus", arr } };
                return jobj.ToString();

            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string SetRegionStatus(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                var user = SecurityController.GetController().GetUser(userId);
                if (null == user)
                //if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";

                //TODO: Изменить логику ролей
                if (user.Role == "Operator")
                    return "Error: Access denied";


                var strZoneId = jBody["RegionId"]?.ToString() ?? "";
                int zoneId;
                if (!int.TryParse(strZoneId, out zoneId))
                    return "Error: invalid parameters RegionId.";
                var strRegionStatus = jBody["Status"]?.ToString() ?? "";
                int regionStatus;
                if (!int.TryParse(strRegionStatus, out regionStatus))
                    return "Error: invalid parameters Status.";
                var status = StateController.GetController().SetRegionStatus(zoneId, (Status)regionStatus);
                var jobj = new JObject { { "RegionId", new JValue(zoneId) }, { "Status", new JValue((int)status) } };
                return jobj.ToString();

            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string GetZoneStatus(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var strZoneId = jBody["ZoneId"]?.ToString() ?? "";
                int zoneId;
                if (!int.TryParse(strZoneId, out zoneId))
                    return "Error: invalid parameters ZoneId.";
                var statuses = StateController.GetController().GetZoneStatus(zoneId);
                var arr = new JArray();
                foreach (var obj in statuses.Select(kvp => new JObject { { "ZoneId", new JValue(kvp.Key) }, { "Status", new JValue(kvp.Value) } }))
                {
                    arr.Add(obj);
                }

                var jobj = new JObject { { "ZoneStatus", arr } };
                return jobj.ToString();
            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string SetZoneStatus(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                //if (!SecurityController.CheckUser(userId))
                //    return "Error: user not registered.";
                var user = SecurityController.GetController().GetUser(userId);
                if (user == null)
                    return "Error: user not registered.";

                //TODO: Изменить логику ролей
                if (user.Role == "Operator")
                    return "Error: Access denied";

                var strZoneId = jBody["ZoneId"]?.ToString() ?? "";
                int zoneId;
                if (!int.TryParse(strZoneId, out zoneId))
                    return "Error: invalid parameters ZoneId.";
                var strZoneStatus = jBody["Status"]?.ToString() ?? "";
                int zoneStatus;
                if (!int.TryParse(strZoneStatus, out zoneStatus))
                    return "Error: invalid parameters Status.";
                var zone = SecurityController.GetController().SetZoneStatus(user.Id, zoneId, (Status)zoneStatus);
                var status = zone?.Status ?? Status.Unknown;
                var jobj = new JObject { { "ZoneId", new JValue(zoneId) }, { "Status", new JValue((int)status) } };
                return jobj.ToString();

            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string GetActions(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var acts = SecurityController.GetController().GetUserActions(userId);
                var arr = JArray.Parse(JsonConvert.SerializeObject(acts));
                var obj = new JObject { { "Actions", arr } };
                return obj.ToString();

            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string GetActionsHistory(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameters.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                var from = jBody["From"]?.ToString() ?? "";
                var to = jBody["To"]?.ToString() ?? "";
                var strRegionId = jBody["RegionId"]?.ToString() ?? "";
                if (string.IsNullOrEmpty(strUserId) || string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to))
                    return "Error: invalid parameters.";
                
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                int actionType = int.Parse(jBody["ActionType"]?.ToString() ?? "0");
                int regionId;
                List<ZoneAction> reflexes;
                if (int.TryParse(strRegionId, out regionId))
                    reflexes = SecurityController.GetController().GetActionsHistory(from, to, actionType, regionId);
                else 
                    reflexes = SecurityController.GetController().GetActionsHistory(from, to, actionType);

                var arr = JArray.Parse(JsonConvert.SerializeObject(reflexes));
                var obj = new JObject { { "ActionsHistory", arr } };
                return obj.ToString();

            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private static string Login(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);

                if (jBody == null)
                    return "Error: user not registered.";

                var login = jBody["Login"]?.ToString() ?? "";
                var pass = jBody["Password"]?.ToString() ?? "";

                var user = SecurityController.GetController().RegisterUser(login, pass);
                var obj = new JObject { { "User", JToken.Parse(JsonConvert.SerializeObject(user)) } };

                return obj.ToString();
            }
            catch (Exception)
            {
                return "Error: user not registered.";
            }
        }

        private static string UserStatus(Stream body)
        {
            var errorMessage = "Error: invalid parameter UserId.";
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return errorMessage;
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                if (string.IsNullOrEmpty(strUserId))
                    return errorMessage;
                Guid userId;
                if (!Guid.TryParse(strUserId, out userId))
                    return errorMessage;
                var obj = new JObject { { "UserStatus", SecurityController.GetController().CheckUser(userId) } };
                return obj.ToString();
            }
            catch (Exception)
            {
                return errorMessage;
            }
        }

        private static string Logout(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);

                if (jBody == null)
                    return "Error: invalid parameter UserId.";

                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";

                SecurityController.GetController().UnregisterUser(userId);
                return "User has logged out.";
            }
            catch (Exception)
            {
                return "Error: invalid parameter UserId.";
            }
        }

        private string GetAll(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";
                var acts = SecurityController.GetController().GetUserActions(userId);
                var arrActions = JArray.Parse(JsonConvert.SerializeObject(acts));
                var statuses = StateController.GetController().GetRegionStatus(0);
                var arrStatuses = new JArray();
                foreach (var objStatus in statuses.Select(kvp => new JObject { { "RegionId", new JValue(kvp.Key) }, { "Status", new JValue(kvp.Value) } }))
                {
                    arrStatuses.Add(objStatus);
                }
                var zoneStatuses = StateController.GetController().GetZoneStatus(0);
                var arrZoneStatuses = new JArray();
                foreach (var objStatus in zoneStatuses.Select(kvp => new JObject { { "ZoneId", new JValue(kvp.Key) }, { "Status", new JValue(kvp.Value) } }))
                {
                    arrZoneStatuses.Add(objStatus);
                }
                var obj = new JObject
                {
                    {"Actions", arrActions},
                    {"RegionStatus", arrStatuses},
                    {"ZoneStatus", arrZoneStatuses}
                };
                var objResult = new JObject { { "Result", obj } };
                return objResult.ToString();
            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string CortexState(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter.";

                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!SecurityController.GetController().CheckUser(userId))
                    return "Error: user not registered.";

                var power = jBody["Power"]?.ToString() ?? "";
                var door = jBody["Door"]?.ToString() ?? "";
                var powerAct = SecurityController.GetController().LogCortexState("Power", power);
                var doorAct = SecurityController.GetController().LogCortexState("Door", door);

                return "Ok";
            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }


#region Integration
        private string IntegrationGetZones(Stream body)
        {
            var result = new Result<List<Zone>>();
            try
            {
                var jBody = body.GetJObject();
                var userId = jBody.GetUserId();
                var zones = SecurityController.GetController().IntegrationGetZones(userId); // SmsServiceGetZones(userId);
                result.Success = true;
                result.Data = zones;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

        private string IntegrationSetZoneState(Stream body)
        {
            var result = new Result<ZoneState>();
            try
            {
                var jBody = body.GetJObject();
                var userId = jBody.GetUserId();
                var zoneId = jBody.GetZoneId();
                var zoneState = jBody.GetZoneState();
                var subject = jBody.GetSubject();
                var description = jBody["Description"]?.ToString() ?? "";

                var zone = SecurityController.GetController().IntegrationSetZoneState(userId, zoneId, (Status)zoneState, string.Empty, description);
                result.Success = true;
                result.Data = zone.GetZoneState();
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

        private string IntegrationGetZoneState(Stream body)
        {
            var result = new Result<List<ZoneState>>();
            try
            {
                var jBody = body.GetJObject();
                var userId = jBody.GetUserId();
                var zoneId = jBody.GetZoneId();
                //var zoneState = jBody.GetZoneState();
                //var subject = jBody.GetSubject();
                var description = jBody["Description"]?.ToString() ?? "";

                var zoneStates = SecurityController.GetController().IntegrationGetZoneState(userId, zoneId, string.Empty, description);
                result.Success = true;
                result.Data = zoneStates;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception exception)
            {
                result.Success = false;
                result.Message = exception.Message;
                result.StackTrace = exception.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

        private string IntegrationGetActions(Stream body)
        {
            var result = new Result<List<ZoneAction>>();
            try
            {
                var jBody = body.GetJObject();
                Guid userId = jBody.GetUserId();
                var actions = SecurityController.GetController().IntegrationGetActions(userId);

                result.Success = true;
                result.Data = actions;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

#endregion Integration
        //private string GetSectors(Stream body)
        //{
        //    var result = new Result<List<Sector>>();
        //    try
        //    {
        //        string strBody;
        //        using (var rdr = new StreamReader(body))
        //            strBody = rdr.ReadToEnd();
        //        var jBody = JObject.Parse(strBody);
        //        if (jBody == null)
        //        {
        //            result.Success = false;
        //            result.Message = "Invalid parameter: UserId.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strUserId = jBody["UserId"]?.ToString() ?? "";
        //        Guid userId;
        //        Guid.TryParse(strUserId, out userId);
        //        if (!SecurityController.CheckUser(userId))
        //        {
        //            result.Success = false;
        //            result.Message = "User not registered.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        result.Data = StateController.GetController().Sectors;
        //        result.Success = true;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Success = false;
        //        result.Message = ex.Message + ex.StackTrace;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //}

        //private string UpdateSector(Stream body)
        //{
        //    var result = new Result<Sector>();
        //    try
        //    {
        //        string strBody;
        //        using (var rdr = new StreamReader(body))
        //            strBody = rdr.ReadToEnd();
        //        var jBody = JObject.Parse(strBody);
        //        if (jBody == null)
        //        {
        //            result.Success = false;
        //            result.Message = "Invalid parameter: UserId.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strUserId = jBody["UserId"]?.ToString() ?? "";
        //        Guid userId;
        //        Guid.TryParse(strUserId, out userId);
        //        if (!SecurityController.CheckUser(userId))
        //        {
        //            result.Success = false;
        //            result.Message = "User not registered.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strSector = jBody["Sector"]?.ToString() ?? "";
        //        if (string.IsNullOrEmpty(strSector))
        //            {
        //                result.Success = false;
        //                result.Message = "Invalid parameter: Sector.";
        //                return JsonConvert.SerializeObject(result);
        //            }
        //        var sector = JsonConvert.DeserializeObject<Sector>(strSector);
        //        result.Data = StateController.GetController().UpdateSector(sector);
        //        result.Success = true;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Success = false;
        //        result.Message = ex.Message + ex.StackTrace;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //}

        //private string DeleteSector(Stream body)
        //{
        //    var result = new Result<bool>();
        //    try
        //    {
        //        string strBody;
        //        using (var rdr = new StreamReader(body))
        //            strBody = rdr.ReadToEnd();
        //        var jBody = JObject.Parse(strBody);
        //        if (jBody == null)
        //        {
        //            result.Success = false;
        //            result.Message = "Invalid parameter: UserId.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strUserId = jBody["UserId"]?.ToString() ?? "";
        //        Guid userId;
        //        Guid.TryParse(strUserId, out userId);
        //        if (!SecurityController.CheckUser(userId))
        //        {
        //            result.Success = false;
        //            result.Message = "User not registered.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strSector = jBody["Sector"]?.ToString() ?? "";
        //        if (string.IsNullOrEmpty(strSector)) if (!SecurityController.CheckUser(userId))
        //            {
        //                result.Success = false;
        //                result.Message = "Invalid parameter: Sector.";
        //                return JsonConvert.SerializeObject(result);
        //            }
        //        var sector = JsonConvert.DeserializeObject<Sector>(strSector);
        //        result.Data = StateController.GetController().DeleteSector(sector);
        //        result.Success = true;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Success = false;
        //        result.Message = ex.Message + ex.StackTrace;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //}

        //private string GetBarriers(Stream body)
        //{
        //    var result = new Result<List<Barrier>>();
        //    try
        //    {
        //        string strBody;
        //        using (var rdr = new StreamReader(body))
        //            strBody = rdr.ReadToEnd();
        //        var jBody = JObject.Parse(strBody);
        //        if (jBody == null)
        //        {
        //            result.Success = false;
        //            result.Message = "Invalid parameter: UserId.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strUserId = jBody["UserId"]?.ToString() ?? "";
        //        Guid userId;
        //        Guid.TryParse(strUserId, out userId);
        //        if (!SecurityController.CheckUser(userId))
        //        {
        //            result.Success = false;
        //            result.Message = "User not registered.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        result.Data = StateController.GetController().Barriers;
        //        result.Success = true;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Success = false;
        //        result.Message = ex.Message + ex.StackTrace;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //}

        //private string UpdateBarrier(Stream body)
        //{
        //    var result = new Result<Barrier>();
        //    try
        //    {
        //        string strBody;
        //        using (var rdr = new StreamReader(body))
        //            strBody = rdr.ReadToEnd();
        //        var jBody = JObject.Parse(strBody);
        //        if (jBody == null)
        //        {
        //            result.Success = false;
        //            result.Message = "Invalid parameter: UserId.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strUserId = jBody["UserId"]?.ToString() ?? "";
        //        Guid userId;
        //        Guid.TryParse(strUserId, out userId);
        //        if (!SecurityController.CheckUser(userId))
        //        {
        //            result.Success = false;
        //            result.Message = "User not registered.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var strBarrier = jBody["Barrier"]?.ToString() ?? "";
        //        if (string.IsNullOrEmpty(strBarrier))
        //        {
        //            result.Success = false;
        //            result.Message = "Invalid parameter: Sector.";
        //            return JsonConvert.SerializeObject(result);
        //        }
        //        var barrier = JsonConvert.DeserializeObject<Barrier>(strBarrier);
        //        result.Data = StateController.GetController().UpdateBarrier(barrier);
        //        result.Success = true;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Success = false;
        //        result.Message = ex.Message + ex.StackTrace;
        //        return JsonConvert.SerializeObject(result);
        //    }
        //}
    }
}
