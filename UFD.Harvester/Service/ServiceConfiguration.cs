﻿using System;
using System.Configuration;

namespace UFD.Harvester.Service
{
    public class ServiceConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("NancyServices")]
        public NancyServiceCollection NancyServices => (NancyServiceCollection)base["NancyServices"];

        [ConfigurationProperty("Host", IsRequired = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        public static ServiceConfiguration GetConfig()
        {
            Configuration execonfig = null;

            try
            {
                execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            catch
            {
                // ignored
            }

            if (execonfig == null)
                return new ServiceConfiguration
                {
                    Host = "http://127.0.0.1:8001"
                };

            return (ServiceConfiguration)execonfig.Sections["Harvester.Service"];
        }
    }

    [ConfigurationCollection(typeof(NancyServiceConfiguration), AddItemName = "NancyService")]
    public class NancyServiceCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new NancyServiceConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((NancyServiceConfiguration)(element));
        }

        public NancyServiceConfiguration this[int idx] => (NancyServiceConfiguration)BaseGet(idx);
    }

    public class NancyServiceConfiguration : ConfigurationElement
    {
        [ConfigurationProperty("Host", IsRequired = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }
    }
}
