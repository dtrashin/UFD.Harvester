﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace UFD.Harvester.Client.Rest
{
    internal enum HttpVerb
    {
        Get,
        Post,
        Put,
        Delete
    }

    internal class RestClient
    {
        private readonly WebProxy _proxy;
        private readonly string _endPoint;
        private readonly HttpVerb _method;
        private readonly string _contentType;
        private readonly string _postData;

        public static RestClient Make() { return new RestClient("", HttpVerb.Get, "", null); }
        public static RestClient Make(string endpoint) { return new RestClient(endpoint, HttpVerb.Get, "", null); }
        public static RestClient Make(string endpoint, HttpVerb method) { return new RestClient(endpoint, method, "", null); }
        public static RestClient Make(string endpoint, HttpVerb method, string postData) { return new RestClient(endpoint, method, postData, null); }
        private RestClient(string endpoint, HttpVerb method, string postData, WebProxy proxy)
        {
            _endPoint = endpoint;
            _method = method;
            _postData = postData;
            _proxy = proxy;
            _contentType = "text/json";
        }

        public string MakeRequest()
        {
            return MakeRequest("");
        }
        public string MakeRequest(string parameters)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(_endPoint + parameters);
                request.Method = _method.ToString();
                request.ContentLength = 0;
                request.ContentType = _contentType;
                request.Proxy = _proxy;
                if (!string.IsNullOrEmpty(_postData) && _method == HttpVerb.Post)
                {
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(_postData);
                    request.ContentLength = bytes.Length;
                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = $"Request failed. Received HTTP {response.StatusCode}";
                        throw new ApplicationException(message);
                    }
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }
                    return responseValue;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}

