﻿using System;
using UFD.Harvester.Client.Contracts;

namespace UFD.Harvester.Client.Client
{
    public class HarvesterFactory : IHarvesterFactory
    {
        public IHarvesterClient MakeHarvesterClient()
        {
            var configuration = HarvesterClientConfiguration.GetConfig() ?? throw new NullReferenceException("Couldn't create configuration.");
            return new HarvesterClient(configuration);
        }
    }
}
