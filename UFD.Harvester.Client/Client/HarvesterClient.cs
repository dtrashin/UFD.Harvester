﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UFD.Harvester.Client.BO;
using UFD.Harvester.Client.Contracts;
using UFD.Harvester.Client.Rest;

namespace UFD.Harvester.Client.Client
{
    internal sealed class HarvesterClient : IHarvesterClient
    {
        private readonly HarvesterClientConfiguration _harvesterConfiguration;
        private readonly Timer _timer;

        private bool _isConnected;

        public event Action<HarvesterClientState> StateChanged = (_) => { };
        public event Action<List<ZoneAction>> ActionsReceived = (_) => { };
        public event Action<List<ZoneState>> ZoneStatesReceived = (_) => { };

        public bool IsConnected
        {
            get { return _isConnected; }
            private set
            {
                if (_isConnected != value)
                {
                    _isConnected = value;
                    OnStatusChange(value ? HarvesterClientState.Connected : HarvesterClientState.Disconnected);
                }
            }
        }

        /// <summary>
        /// .ctor
        /// </summary>
        internal HarvesterClient(HarvesterClientConfiguration configuration)
        {
            if (null == configuration)
                throw new ArgumentNullException("Configuration couldn't be null.");
            _harvesterConfiguration = configuration; 
            _timer = new Timer()
            {
                AutoReset = true,
                Interval = _harvesterConfiguration.RequestInterval
            };
            _timer.Elapsed += OnTimerElapsed;
            IsConnected = false;
        }

        public void Start()
        {
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
            IsConnected = false;
        }

        public List<ZoneState> GetZoneStatus()
        {
            return GetZoneStatus(0);
        }

        public List<ZoneState> GetZoneStatus(int zoneId)
        {
            var result = new List<ZoneState>();
            try
            {
                var requestBody = new JObject { { "UserId", _harvesterConfiguration.Id }, { "ZoneId", zoneId } };
                var client = RestClient.Make(_harvesterConfiguration.Host + "GetZoneStatus", HttpVerb.Post, requestBody.ToString());
                var responce = client.MakeRequest();
                if (string.IsNullOrEmpty(responce)) return result;
                var obj = JObject.Parse(responce);
                result = ((JArray)obj["ZoneStatus"]).Select(x => new ZoneState((int)x["ZoneId"], (State)(int)x["Status"])).ToList();
                IsConnected = true;
            }
            catch
            {
                IsConnected = false;
            }
            return result;
        }

        public ZoneState SetZoneStatus(ZoneState zoneState)
        {
            var result = new ZoneState(zoneState.ZoneId, State.Unknown);
            try
            {
                var requestBody = new JObject { { "UserId", _harvesterConfiguration.Id }, { "ZoneId", zoneState.ZoneId }, { "Status", (int)zoneState.Status } };
                var client = RestClient.Make(_harvesterConfiguration.Host + "SetZoneStatus", HttpVerb.Post, requestBody.ToString());
                var responce = client.MakeRequest();
                if (string.IsNullOrEmpty(responce)) return result;
                var obj = JObject.Parse(responce);
                State state = (State)obj["Data"]["State"].Value<int>();
                result = new ZoneState(zoneState.ZoneId, state);
                IsConnected = true;
            }
            catch
            {
                IsConnected = false;
            }
            return result;
        }

        public List<Zone> GetZones()
        {
            var zones = new List<Zone>();
            try
            {
                if (_harvesterConfiguration == null) return new List<Zone>();
                var requestBody = new JObject { { "UserId", _harvesterConfiguration.Id } };
                var client = RestClient.Make(_harvesterConfiguration.Host + "GetZones", HttpVerb.Post, requestBody.ToString());
                var json = client.MakeRequest();
                var jBody = JObject.Parse(json);
                if (jBody == null)
                    throw new Exception("Invalid parameters.");
                var strData = jBody["Data"]?.ToString() ?? "";
                if (string.IsNullOrEmpty(strData))
                    throw new Exception("Invalid parameters.");
                zones = JsonConvert.DeserializeObject<List<Zone>>(strData);
                IsConnected = true;
            }
            catch
            {
                IsConnected = false;
            }
            return zones;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                List<ZoneAction> zas = GetZoneActions();
                IsConnected = true;
                if (zas.Any())
                    OnActionsReceive(zas);
                List<ZoneState> states = GetZoneStatus();
                if (states.Any())
                    ZoneStatesReceived(states);
            }
            catch (Exception)
            {
                IsConnected = false;
            }
        }

        private List<ZoneAction> GetZoneActions()
        {
            if (_harvesterConfiguration == null) return new List<ZoneAction>();
            var requestBody = new JObject { { "UserId", _harvesterConfiguration.Id } };
            var client = RestClient.Make(_harvesterConfiguration.Host + "GetActions", HttpVerb.Post, requestBody.ToString());
            var json = client.MakeRequest();
            var jBody = JObject.Parse(json);
            if (jBody == null)
                throw new Exception("Invalid parameters.");
            var strData = jBody["Data"]?.ToString() ?? "";
            if (string.IsNullOrEmpty(strData))
                throw new Exception("Invalid parameters.");
            return JsonConvert.DeserializeObject<List<ZoneAction>>(strData);
        }

        private void OnStatusChange(HarvesterClientState state)
        {
            StateChanged.Invoke(state);
        }

        private void OnActionsReceive(List<ZoneAction> actions)
        {
            ActionsReceived.Invoke(actions);
        }
    }

}
