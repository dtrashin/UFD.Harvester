﻿using System;
using System.Configuration;

namespace UFD.Harvester.Client.Client
{
    internal sealed class HarvesterClientConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Host", IsRequired = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        [ConfigurationProperty("Id", IsRequired = true)]
        public Guid Id
        {
            get { return (Guid)this["Id"]; }
            set { this["Id"] = value; }
        }

        [ConfigurationProperty("RequestInterval", IsRequired = true)]
        public int RequestInterval
        {
            get { return (int)this["RequestInterval"]; }
            set { this["RequestInterval"] = value; }
        }

        public static HarvesterClientConfiguration GetConfig()
        {
            HarvesterClientConfiguration config = null;
            try
            {
                var execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config = (HarvesterClientConfiguration)execonfig.Sections["Harvester.Client"];
            }
            catch
            {
                throw new Exception("Configuration for harvester client not found.");
            }
            return config;
        }
    }
}
