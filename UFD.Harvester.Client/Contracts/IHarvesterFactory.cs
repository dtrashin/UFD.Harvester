﻿namespace UFD.Harvester.Client.Contracts
{
    interface IHarvesterFactory
    {
        IHarvesterClient MakeHarvesterClient();
    }
}
