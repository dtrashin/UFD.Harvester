﻿namespace UFD.Harvester.Client.Contracts
{
    /// <summary>
    /// Опсание состояния клиента.
    /// </summary>
    public enum HarvesterClientState
    {
        /// <summary>
        /// Клиент отключен.
        /// </summary>
        Disconnected,

        /// <summary>
        /// Клиент подключен.
        /// </summary>
        Connected
    }
}
