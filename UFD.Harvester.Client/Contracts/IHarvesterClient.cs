﻿using System;
using System.Collections.Generic;
using UFD.Harvester.Client.BO;

namespace UFD.Harvester.Client.Contracts
{
    public interface IHarvesterClient
    {
        event Action<HarvesterClientState> StateChanged;
        event Action<List<ZoneAction>> ActionsReceived;
        event Action<List<ZoneState>> ZoneStatesReceived;

        void Start();
        void Stop();
        List<ZoneState> GetZoneStatus();
        List<ZoneState> GetZoneStatus(int zoneId);
        ZoneState SetZoneStatus(ZoneState zoneState);
        List<Zone> GetZones();
    }
}
