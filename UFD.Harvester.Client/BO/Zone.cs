﻿using System;
using System.Runtime.Serialization;

namespace UFD.Harvester.Client.BO
{
    /// <summary>
    /// Зона контроля периметра.
    /// </summary>
    [DataContract]
    public sealed class Zone
    {
        /// <summary>
        /// Id зоны.
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Название зоны.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Координата начала зоны на сенсоре.
        /// </summary>
        [DataMember]
        public double FiberX1 { get; set; }

        /// <summary>
        /// Координата конца зоны на сенсоре.
        /// </summary>
        [DataMember]
        public double FiberX2 { get; set; }

        /// <summary>
        /// Id раздела, в который включена зона.
        /// </summary>
        [DataMember]
        public int RegionId { get; set; }

        /// <summary>
        /// Id канала, с которого проходят данные об октивности.
        /// </summary>
        [DataMember]
        public Guid ReceiverId { get; set; }

        /// <summary>
        /// Состояние зоны.
        /// </summary>
        [DataMember]
        public State Status { get; set; }
    }
}
