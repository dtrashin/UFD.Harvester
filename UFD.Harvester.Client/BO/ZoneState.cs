﻿using System.Runtime.Serialization;

namespace UFD.Harvester.Client.BO
{
    /// <summary>
    /// Описание состояния зоны.
    /// </summary>
    [DataContract]
    public sealed class ZoneState
    {
        /// <summary>
        /// Id зоны.
        /// </summary>
        [DataMember]
        public int ZoneId { get; }

        /// <summary>
        /// Описание зоны.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Состояние зоны.
        /// </summary>
        [DataMember]
        public State Status { get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="zoneId">Id зоны.</param>
        /// <param name="state">Состояние зоны.</param>
        public ZoneState(int zoneId, State state)
        {
            ZoneId = zoneId;
            Status = state;
        }
    }
}
