﻿namespace UFD.Harvester.Client.BO
{
    /// <summary>
    /// Состояния, которые может принимать зона.
    /// </summary>
    public enum State
    {
        /// <summary>
        /// Выключено (зона снята с охраны).
        /// </summary>
        Disabled = 0,

        /// <summary>
        /// Включено (зона поставлена на охрану).
        /// </summary>
        Enabled = 1,

        /// <summary>
        /// Внимание (зона в пограничном состоянии).
        /// </summary>
        Warning = 2,

        /// <summary>
        /// Тревога (зона находится в состоянии тревоги).
        /// </summary>
        Alarm = 3,

        /// <summary>
        /// Обрыв (зона находится на участке сенсора после обрыва).
        /// </summary>
        Broken = 4,

        /// <summary>
        /// Неизвестно (зона находится в соостоянии калибровки).
        /// </summary>
        Unknown = 255
    }
}
