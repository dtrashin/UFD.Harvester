﻿using System;
using System.Runtime.Serialization;

namespace UFD.Harvester.Client.BO
{
    /// <summary>
    /// Событие в зоне.
    /// </summary>
    [DataContract]
    public sealed class ZoneAction
    {
        /// <summary>
        /// Id события.
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Время возникновения события.
        /// </summary>
        [DataMember]
        public string Time { get; set; }

        /// <summary>
        /// Id зоны, в которой произошло событие.
        /// </summary>
        [DataMember]
        public int ZoneId { get; set; }

        /// <summary>
        /// Координата по сенсору, в которой произошло событие.
        /// </summary>
        [DataMember]
        public int X { get; set; }

        /// <summary>
        /// Состояние зоны.
        /// </summary>
        [DataMember]
        public State Status { get; set; }

        /// <summary>
        /// Подтверждение обработки события.
        /// </summary>
        [DataMember]
        public bool Confirmed { get; set; }

        /// <summary>
        /// Тип объекта, который вызвал событие.
        /// </summary>
        [DataMember]
        public int RecognizeType { get; set; }

        /// <summary>
        /// Направление движения.
        /// </summary>
        [DataMember]
        public int Direction { get; set; }
    }
}
