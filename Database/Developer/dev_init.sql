﻿delete from users;
delete from dampers;
delete from devices;
delete from zones;
delete from regions;
delete from checkers;

insert into regions (id, parent_id, svg, description)
values
(0,-1,'http://192.168.0.231/svg/ostafievo.svg','Ostafievo'),
(1,0,'http://192.168.0.231/svg/ostafievo.svg','Barrier 1');

insert into zones (id, region_id, fiber_x1, fiber_x2, description, x1, y1, x2, y2, receiver_id, status)
values
(1, 1, 478, 519, 'Zone_0', 500, 270, 225, 195, '0EBD712F-F078-414A-B2E3-E76B5BD1714B', 1),
(2, 1, 605, 648, 'Zone_1', 220, 220, 492, 295, '0EBD712F-F078-414A-B2E3-E76B5BD1714B', 1),
(3, 1, 725, 767, 'Zone_2', 485, 320, 215, 247, '0EBD712F-F078-414A-B2E3-E76B5BD1714B', 1);

insert into users (name, password, description)
values
('administrator', 'administrator', 'Администратор'),
('operator', 'operator', 'Оператор АРМ'),
('integrator', 'integrator', ''),
('guardgui', 'guardgui', 'АРМ');

insert into checkers
values
('7068B024-D799-4D74-81F3-290FA8888C1E', '0EBD712F-F078-414A-B2E3-E76B5BD1714B', '7068B024-D799-4D74-81F3-290FA8888C1E', 450, 800, false),
('A5043D81-C7ED-46D6-9E31-C0D72D0955EC', '88E8229F-B710-4C58-9328-E4DAF0A8DEE3', '7068B024-D799-4D74-81F3-290FA8888C1E', 450, 800, false);

insert into dampers
values
(1, 478, 519, 10, 1, 2, 5, true, true, true),
(2, 605, 648, 10, 1, 2, 5, true, true, true),
(3, 725, 767, 10, 1, 2, 5, true, true, true);
