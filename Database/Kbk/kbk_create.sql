﻿drop table if exists system_log;
drop table if exists receiver_log;
drop table if exists actions;
drop table if exists devices;
drop table if exists dampers;
drop table if exists users;
drop table if exists zones;
drop table if exists regions;
drop table if exists checkers;
drop sequence if exists regions_id_seq cascade;
drop sequence if exists users_id_seq cascade;
drop sequence if exists zones_id_seq cascade;

CREATE SEQUENCE regions_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE regions_id_seq
  OWNER TO postgres;
  
CREATE SEQUENCE zones_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE zones_id_seq
  OWNER TO postgres;
  
CREATE SEQUENCE users_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE zones_id_seq
  OWNER TO postgres;
  
CREATE TABLE receiver_log
(
  dt timestamp without time zone,
  receiver_id uuid,
  "raw" bytea
)
WITH (
  OIDS=FALSE
);
ALTER TABLE receiver_log
  OWNER TO postgres;
  
create table system_log (
	dt timestamp without time zone not null,
	"subject" text not null,
	"object" text not null,
	"action" text not null,
	"description" text
)
with (
	oids=false
);
alter table system_log
	owner to postgres;

CREATE TABLE actions
(
  id uuid not null,
  datetime timestamp without time zone not null,
  zone_id integer not null,
  x integer,
  status integer not null,
  receiver_id uuid,
  confirm boolean default false,
  subject text,
  recognizetype bigint default 0
)
WITH (
  OIDS=FALSE
);
ALTER TABLE actions
  OWNER TO postgres;
  
CREATE TABLE regions
(
  id integer NOT NULL DEFAULT nextval('regions_id_seq'::regclass),
  parent_id integer not null,
  svg text,
  description text,
  CONSTRAINT regions_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE regions
  OWNER TO postgres;

CREATE TABLE zones
(
  id integer NOT NULL DEFAULT nextval('zones_id_seq'::regclass),
  region_id integer,
  receiver_id uuid,
  status integer DEFAULT 1,
  fiber_x1 integer,
  fiber_x2 integer,
  address text,
  description text,
  x1 numeric,
  y1 numeric,
  x2 numeric,
  y2 numeric,
  recognize boolean DEFAULT false,
  CONSTRAINT zones_pkey PRIMARY KEY (id),
  CONSTRAINT region_id FOREIGN KEY (region_id)
      REFERENCES regions (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE zones
  OWNER TO postgres;

CREATE INDEX fki_regions_key
  ON zones
  USING btree
  (region_id);

CREATE TABLE dampers
(
  zoneid integer,
  x1 integer NOT NULL DEFAULT 0,
  x2 integer NOT NULL DEFAULT 0,
  timeout integer NOT NULL DEFAULT 1,
  warning_count integer NOT NULL DEFAULT 1,
  alert_count integer NOT NULL DEFAULT 1,
  range integer NOT NULL DEFAULT 10,
  split boolean NOT NULL DEFAULT false,
  enable boolean NOT NULL DEFAULT false,
  overflow boolean not null default false,
  CONSTRAINT zoneid FOREIGN KEY (zoneid)
      REFERENCES zones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dampers
  OWNER TO postgres;

CREATE INDEX fki_zoneid
  ON dampers
  USING btree
  (zoneid);

CREATE TABLE devices
(
  zone_id integer,
  address text,
  port integer,
  register integer,
  description text,
  CONSTRAINT zone_id FOREIGN KEY (zone_id)
      REFERENCES zones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE devices
  OWNER TO postgres;
  
CREATE TABLE users
(
  id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  name text NOT NULL,
  password text,
  description text,
  last_request timestamp without time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;

create table checkers
(
  id uuid not null,
  receiver_id uuid not null,
  check_id uuid not null,
  x1 integer not null,
  x2 integer not null,
  reverse boolean not null default false
)
with (
  oids=false
);
alter table checkers
  owner to postgres;
