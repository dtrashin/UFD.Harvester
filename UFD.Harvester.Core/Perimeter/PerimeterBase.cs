﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UFD.Harvester.Core.BL;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.DAL;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.Perimeter
{
    public class PerimeterBase
    {
        public static readonly IDataFacade DataFacade = DataFacadeFactory.GetDataFacade();
        public List<Region> Regions { get; } = DataFacade.GetRegions();
        public List<Zone> Zones { get; private set; } = DataFacade.GetZones();
        internal static readonly ZoneActionController ZoneActionController = ZoneActionController.GetController();

        public virtual void CheckDatagram(Datagram datagram)
        {
            DataFacade.LogRaw(datagram);
            var zones = Zones.Where(z => z.ReceiverId == datagram.ReceiverId).ToList();
            if (datagram.DatagramType == 1)
                SetBroken(datagram.Points.FirstOrDefault(), zones);
            else
            {
                zones = zones.Where(zone => zone.Status != Status.Disabled).ToList();
                zones.ForEach(zone => zone.BeginChecking());
                datagram.Impacts.ForEach(impact => zones.ForEach(zone => zone.AddImpact(impact)));
                zones.ForEach(zone => zone.EndChecking());
                ZoneActionController.SendAct(zones.Select(z => z.Actions).SelectMany(x => x).ToList());
            }
            foreach (var zone in zones)
            {
                var z = zone;
                DataFacade.UpdateZoneStatus(z.Id, z.Status);
            }
        }

        public virtual Dictionary<int, Status> GetRegionStatus(int regionId)
        {
            var result = new Dictionary<int, Status>();
            if (regionId > 0)
            {
                var region = Regions.FirstOrDefault(r => r.Id == regionId);
                if (region != null)
                {
                    var zones = Zones.Where(z => z.RegionId == region.Id).ToList();
                    result.Add(region.Id, zones.Any() ? zones.Max(z => z.Status) : Status.Unknown);
                }
                else
                    result.Add(regionId, Status.Unknown);
            }
            else
            {
                foreach (var region in Regions.Where(r => r.Id != 0).AsParallel())
                {
                    var zones = Zones.Where(z => z.RegionId == region.Id).ToList();
                    result.Add(region.Id, zones.Any() ? zones.Max(z => z.Status) : Status.Unknown);
                }
            }
            return result;
        }

        public virtual Status SetRegionStatus(int regionId, Status status, bool reset = false)
        {
            var region = Regions.FirstOrDefault(r => r.Id == regionId);
            if (region == null)
                return Status.Unknown;
            if (status != Status.Disabled && status != Status.Enabled)
                return region.Status;

            var regions = GetChildrenRegions(region);

            regions.ForEach(reg =>
            {
                lock (this)
                {
                    SetRegionStatus(reg, status);
                    Zones.Where(z => z.RegionId == reg.Id).ToList().ForEach(z => { z.SetStatus(reg.Status); DataFacade.UpdateZoneStatus(z.Id, reg.Status); });
                }
            });
            return region.Status;
        }

        public virtual Dictionary<int, Status> GetStatuses()
        {
            var result = new Dictionary<int, Status>();
            foreach (var region in Regions.Where(r => r.Id != 0))
            {
                var zones = Zones.Where(z => z.RegionId == region.Id).ToList();
                result[region.Id] = zones.Select(z => z.Status).Max();
            }
            return result;
        }

        public virtual Dictionary<int, Status> GetZoneStatus(int zoneId)
        {
            var result = new Dictionary<int, Status>();
            if (zoneId > 0)
            {
                var zone = Zones.FirstOrDefault(z => z.Id == zoneId);
                result.Add(zoneId, zone?.Status ?? Status.Unknown);
            }
            else
            {
                foreach (var zone in Zones)
                {
                    var z = zone;
                    result.Add(z.Id, z.Status);
                }
            }
            return result;
        }

        public virtual bool UpdateZone(Zone zone)
        {
            var result = DataFacade.UpdateZone(zone);
            if (result) Zones = DataFacade.GetZones();
            return result;
        }

        public virtual Status SetZoneStatus(int zoneId, Status status)
        {
            var zone = Zones.FirstOrDefault(z => z.Id == zoneId);
            if (zone == null)
                return Status.Unknown;
            if (status != Status.Disabled && status != Status.Enabled && status != Status.Observation)
                return zone.Status;
            zone.SetStatus(status);
            if (status == Status.Disabled || status == Status.Enabled || status == Status.Observation)
                ZoneActionController.ClearDirectionActions();
            DataFacade.UpdateZoneStatus(zone.Id, status);
            return zone.Status;
        }

        public virtual List<ZoneState> GetZoneState(int zoneId)
        {
            var result = new List<ZoneState>();
            if (zoneId > 0)
            {
                var zone = Zones.FirstOrDefault(z => z.Id == zoneId);
                if (zone != null)
                    result.Add(zone.GetZoneState());
            }
            else
            {
                foreach (var zone in Zones)
                {
                    var z = zone;
                    result.Add(z.GetZoneState());
                }
            }
            return result;
        }

        private List<Region> GetChildrenRegions(Region region)
        {
            var regions = Regions.Where(r => r.ParentId == region.Id).Concat(Regions.Where(r => r.ParentId == region.Id).SelectMany(GetChildrenRegions)).Distinct().ToList();
            regions.Add(region);
            return regions;
        }

        private static void SetRegionStatus(Region region, Status status)
        {
            switch (status)
            {
                case Status.Disabled:
                    if (region.Status == Status.Alarm || region.Status == Status.Enabled || region.Status == Status.Warning)
                        region.Status = Status.Disabled;
                    break;
                case Status.Enabled:
                    if (region.Status == Status.Alarm || region.Status == Status.Disabled || region.Status == Status.Warning)
                        region.Status = Status.Enabled;
                    break;
                case Status.Alarm:
                    if (region.Status == Status.Enabled || region.Status == Status.Warning)
                        region.Status = Status.Alarm;
                    break;
                case Status.Broken:
                    if (region.Status == Status.Alarm || region.Status == Status.Disabled || region.Status == Status.Enabled || region.Status == Status.Warning)
                        region.Status = Status.Broken;
                    break;
                case Status.Warning:
                    break;
            }
        }

        private static void SetBroken(int point, IEnumerable<Zone> zones)
        {
            var acts = new List<ZoneAction>();
            zones.Where(z => z.FiberX2 > point && z.Status != Status.Broken).ToList().ForEach(bz =>
            {
                bz.SetBroken(point);
                acts.Add(new ZoneAction
                {
                    TimeTicket = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                    ZoneId = bz.Id,
                    X = point,
                    Status = Status.Broken
                });
            });
            ZoneActionController.SendAct(acts);
        }

    }
}