﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.Perimeter
{
    public class PerimeterZones : PerimeterBase
    {
        public override void CheckDatagram(Datagram datagram)
        {
            var zones = Zones.Where(z => z.ReceiverId == datagram.ReceiverId).ToList();
            if (datagram.DatagramType == 1)
                SetBroken(datagram.Points.FirstOrDefault(), zones);
            else
            {
                zones = zones.Where(zone => zone.Status != Status.Disabled).ToList();
                zones.ForEach(zone => zone.BeginChecking());
                datagram.Impacts.ForEach(impact => zones.ForEach(zone => zone.AddImpact(impact)));
                zones.ForEach(zone => zone.EndChecking());
                ZoneActionController.SendAct(zones.Select(z => z.Actions).SelectMany(x => x).ToList());
            }
            foreach (var zone in zones)
            {
                var z = zone;
                DataFacade.UpdateZoneStatus(z.Id, z.Status);
            }
        }

        private static void SetBroken(int point, IEnumerable<Zone> zones)
        {
            var acts = new List<ZoneAction>();
            zones.Where(z => z.FiberX2 > point && z.Status != Status.Broken).ToList().ForEach(bz =>
            {
                bz.SetBroken(point);
                acts.Add(new ZoneAction
                {
                    TimeTicket = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                    ZoneId = bz.Id,
                    X = point,
                    Status = Status.Broken
                });
            });
            ZoneActionController.SendAct(acts);
        }
    }
}