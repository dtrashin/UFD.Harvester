﻿using System.Collections.Generic;
using System.Linq;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.Enums;

namespace UFD.Harvester.Core.Perimeter
{
    public class DirectionAction
    {
        private readonly object _locker = new object();
        private readonly Queue<List<ZoneAction>> _queueActions = new Queue<List<ZoneAction>>();
        public int Point { get; set; }
        public bool Changed { get; set; }

        public int Count()
        {
            lock (_locker)
            {
                //return _queueActions.Count(za => za.Count > 0);
                return _queueActions.Count();
            }
        }

        public void Push(List<ZoneAction> zoneActions)
        {
            lock (_locker)
            {
                //if (Changed || zoneActions.Count == 0) return;
                if (Changed) return;
                _queueActions.Enqueue(zoneActions);
                Changed = true;
            }
        }

        public void Pop()
        {
            lock (_locker)
            {
                if (_queueActions.Count > 100)
                    _queueActions.Dequeue();
                Changed = false;
                //int sumPoint = 0;
                //foreach (var queueActionList in _queueActions)
                //{
                //    foreach (var zoneAction in queueActionList)
                //    {
                //        sumPoint += zoneAction.X;
                //    }
                //}
                //Point = sumPoint / Count();
            }
        }

        public bool IsPointInAction(int point)
        {
            return point >= Point - 5 && point <= Point + 5;
        }

        public Direction GetDirection()
        {
            lock (_locker)
            {
                if (_queueActions.Count < 2) return Direction.Unknown;

                var i = 0;
                var x = 0;
                var sumXY = 0;
                var sumY = 0;
                var sumX2 = 0;
                var sumX = 0;
                foreach (var barrierActions in _queueActions)
                {
                    foreach (var barrierAction in barrierActions)
                    {
                        sumX += x;
                        sumY += barrierAction.ZoneId;
                        sumXY += x*barrierAction.ZoneId;
                        sumX2 += x*x;
                        i++;
                    }
                    x++;
                }

                var result = Direction.Unknown;
                if (i > 1)
                {
                    var k = (i*sumXY - sumX*sumY)/((float) (i*sumX2 - sumX*sumX));
                    if (k < 0)
                        result = Direction.Oncoming;
                    if (k > 0)
                        result = Direction.Receding;
                }
                _queueActions.ToArray()[_queueActions.Count - 1].ForEach(a => a.Direction = result);

                return result;
            }
        }
    }
}
