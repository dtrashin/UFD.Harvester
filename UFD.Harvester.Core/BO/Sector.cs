﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UFD.Harvester.Core.BO
{
    /// <summary>
    /// Лгический сектор для объединения барьеров.
    /// </summary>
    public class Sector
    {
        private readonly List<Barrier> _barriers = new List<Barrier>();
        private readonly List<Sector> _sectors = new List<Sector>();

        /// <summary>
        /// Id сектора.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id родительского сектора.
        /// </summary>
        public Guid? ParentId { get; set; }

        /// <summary>
        /// Наименование сектора.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание сектора.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подложки.
        /// </summary>
        public string Svg { get; set; }

        // .ctor
        public Sector() : this(Guid.NewGuid()) { }

        public Sector(Guid id)
        {
            Id = id;
        }


        /// <summary>
        /// Добавление сектора.
        /// </summary>
        /// <param name="sector"></param>
        public void AddSector(Sector sector)
        {
            if (_sectors.Any(s => s.Id == sector.Id)) return;
            _sectors.Add(sector);
        }

        public void DeleteSector(Sector sector)
        {
            sector.Delete();
        }

        /// <summary>
        /// Добавление барьера в сектор.
        /// </summary>
        /// <param name="barrier"></param>
        public void AddBarrier(Barrier barrier)
        {
            if (_barriers.Any(b => b.Id == barrier.Id)) return;
            _barriers.Add(barrier);
        }

        /// <summary>
        /// Удаление барьера из сектора.
        /// </summary>
        /// <param name="barrier"></param>
        public void DeleteBarrier(Barrier barrier)
        {
            _barriers.Remove(barrier);
        }

        /// <summary>
        /// Удаление сектора.
        /// </summary>
        public void Delete()
        {
        }
    }
}
