﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Core.BO
{
    /// <summary>
    /// Класс, описание зоны на оптическом волокне.
    /// Входит в состав барьера.
    /// </summary>
    public class Fiber
    {
        /// <summary>
        /// Id зоны на оптическом волокне.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id устройства, с которого приходит сигнал по волокну.
        /// </summary>
        public byte DeviceId { get; set; }

        /// <summary>
        /// Id канала, с которого приходит сигнал по волокну.
        /// </summary>
        public byte ChanelId { get; set; }

        /// <summary>
        /// Уникальный ключ какнала.
        /// </summary>
        public int Key => DeviceId * 1000 + ChanelId;

        /// <summary>
        /// Начало зоны по оптическому волокну (метры).
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// Конец зоны по оптическому волокну (метры).
        /// </summary>
        public int Stop { get; set; }

        /// <summary>
        /// Id барьера, в составе которого находится зона.
        /// </summary>
        public Guid BarrierId { get; set; }

        /// <summary>
        /// Порядковый номер зоны в барьере.
        /// Использоуется для оперделения направления активности в барьере.
        /// </summary>
        public int Position { get; set; }
    }
}
