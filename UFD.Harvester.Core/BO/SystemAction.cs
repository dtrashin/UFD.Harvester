﻿using System;

namespace UFD.Harvester.Core.BO
{
    public class SystemAction
    {
        public DateTime DateTime { get; private set; }
        public string Object { get; set; }
        public string Subject { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }

        public SystemAction()
            : this(DateTime.Now)
        { }

        public SystemAction(DateTime dt)
        {
            DateTime = dt;
            Object = string.Empty;
            Subject = string.Empty;
            Action = string.Empty;
            Description = string.Empty;
        }
    }
}
