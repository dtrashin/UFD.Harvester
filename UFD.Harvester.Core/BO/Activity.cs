﻿using System.Collections.Generic;
using System.Reflection;
using UFD.Harvester.Core.Enums;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.BO
{
    public class Activity
    {
        private readonly object _locker = new object();
        private readonly Queue<List<BarrierAction>> _actions;
        private readonly int _range;
        private readonly int _whileDeterminingDirection;
        private readonly int _deep;

        public int Point { get; private set; }
        public bool Changed { get; private set; }
        

        public void Push(List<BarrierAction> action)
        {
            lock (_locker)
            {
                if (!Changed)
                {
                    _actions.Enqueue(action);
                    Changed = true;
                }
            }
        }
        public void Pop()
        {
            lock (_locker)
            {
                _actions.Dequeue();
                Changed = false;
            }
        }

        public Direction GetDirection()
        {
            if (_actions.Count < _whileDeterminingDirection) return Direction.Unknown;

            int i = 0;
            int x = 0;
            int sumXY = 0;
            int sumY = 0;
            int sumX2 = 0;
            int sumX = 0;
            foreach (var barrierActions in _actions)
            {
                foreach (var barrierAction in barrierActions)
                {
                    sumX += x;
                    sumY += barrierAction.Position;
                    sumXY += x*barrierAction.Position;
                    sumX2 += x*x;

                    i++;
                }
                x++;
            }
            var k = (i*sumXY - sumX*sumY)/((float)(i*sumX2 - sumX*sumX));
            if (k < 0) return Direction.Oncoming;
            if (k > 0) return Direction.Receding;
            return Direction.Unknown;

        }
    }
}
