﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Timers;
using UFD.Harvester.Core.Attributes;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.BO
{
    public class Damper : ValidatableObject
    {
        private readonly object _locker = new object();
        private readonly List<Pipe> _pipes = new List<Pipe>();
        private bool _calibrated;
        private readonly Timer _timer = new Timer();
        private int _deepCount;
        private int _deepOverflow;

        [NotNegativeNumber]
        public int X1 { get; set; }
        [NotNegativeNumber]
        public int X2 { get; set; }
        public int Timeout { get; set; }
        //public int TimeoutOwerflow { get; set; }
        public int WarningStatusCount { get; set; }
        public int AlarmStatusCount { get; set; }
        public int Range { get; set; }
        public bool Split { get; set; }
        public bool Enable { get; set; }
        public Status Status { get; private set; }
        public bool Overflow { get; set; }

        public void BeginChecking()
        {
            if (!_calibrated)
            {
                _deepCount++;
                if (!_timer.Enabled)
                {
                    _timer.AutoReset = false;
                    _timer.Interval = Timeout * 1000;
                    _timer.Elapsed += OnTimerElapsed;
                    _timer.Enabled = true;
                }
                Status = Status.Unknown;
                return;
            }
            lock (_locker)
            {
                var pipes = _pipes.Where(p => p.Changed).ToList();
                pipes.ForEach(p => p.Pop());
            }
        }

        public bool IsPointInDamper(int point)
        {
            return point >= X1 && point <= X2;
        }

        public Status CheckStatusPoint(Impact impact)
        {
            lock (_locker)
            {
                if (!Enable)
                    return Status.Alarm;
                if (!_calibrated)
                    return Status.Unknown;
                Pipe pipe = GetPipe(impact.Point);
                if (pipe != null)
                    pipe.Push(true);
                else
                    pipe = AddPipe(impact.Point);
                var status = GetPipeStatus(pipe);
                Status = status > Status ? status : Status;
                return status;
            }
        }

        [Obsolete]
        public Status CheckStatusPoint(int point)
        {
            lock (_locker)
            {
                if (!Enable)
                    return Status.Alarm;
                if (!_calibrated)
                    return Status.Unknown;
                Pipe pipe = GetPipe(point);
                if (pipe != null)
                    pipe.Push(true);
                else
                    pipe = AddPipe(point);
                var status = GetPipeStatus(pipe);
                Status = status > Status ? status : Status;
                return status;
            }
        }

        public void Reset()
        {
            lock (_locker)
            {
                if (!_calibrated) return;
                _pipes.Clear();
                Status = Status.Enabled;
            }
        }

        public void EndChecking()
        {
            if (!_calibrated) return;
            lock (_locker)
            {
                _pipes.RemoveAll(p => p.Count == 0 && !p.IsOverflow);
                //var pipes = _pipes.Where(p => !p.Changed).ToList();
                _pipes.ForEach(p => p.Push(false));
            }
        }

        private Pipe AddPipe(int point)
        {
            var pipe = new Pipe(point, _deepCount, Range, Split, _deepOverflow);
            _pipes.Add(pipe);
            return pipe;
        }

        private Status GetPipeStatus(Pipe pipe)
        {
            if (pipe.IsOverflow)
                return Status.Enabled;
            if (pipe.Count >= AlarmStatusCount)
            {
                if (Overflow) pipe.Overflow();
                return Status.Alarm;
            }
            if (pipe.Count >= WarningStatusCount)
                return Status.Warning;

            return Status.Enabled;
        }

        private Pipe GetPipe(int point)
        {
            lock (_locker)
            {
                var pipes = _pipes.Where(p => p.IsPointInPipe(point)).ToArray();
                if (pipes.Any())
                {
                    Pipe result = null;
                    int distance = int.MaxValue;
                    foreach (Pipe pipe in pipes)
                    {
                        int locDistance = Math.Abs(point - pipe.Point);
                        if (locDistance < distance)
                        {
                            result = pipe;
                            distance = locDistance;
                        }
                    }
                    return result;
                }
                else
                    return null;
            }
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _calibrated = true;
            _timer.Enabled = false;
#if MGTS
            _deepOverflow = (_deepCount*3610)/Timeout;
#else
            _deepOverflow = (_deepCount*19)/Timeout;
#endif
            Status = Status.Enabled;
        }

        private class Pipe
        {
            private readonly object _locker = new object();
            private readonly bool _split;
            private readonly Queue<bool> _impulses;
            private bool _changed;
            private readonly int _range;
            private bool _isOverflow;
            private int _deepOverflow;
            private int _overflowTimeout;

            public int Point { get; private set; }
            public int Count
            {
                get
                {
                    int result = 0;
                    if (_isOverflow)
                        return result;
                    lock (_locker)
                    {
                        bool prevImpulse = false;
                        if (!_split)
                            foreach (bool impulse in _impulses)
                            {
                                if (impulse && !prevImpulse)
                                    result++;
                                prevImpulse = impulse;
                            }
                        else
                            result = _impulses.Count(i => i);
                    }
                    return result;
                }
            }
            public bool Changed { get { return _changed; } }
            public bool IsOverflow { get { return _isOverflow; } }

            public Pipe(int point, int deep, int range, bool split, int deepOverflow)
            {
                Point = point;
                _split = split;
                _range = range;
                _deepOverflow = deepOverflow;
                _overflowTimeout = 0;
                _impulses = new Queue<bool>(deep);
                for (int i = 0; i < deep - 1; i++)
                {
                    _impulses.Enqueue(false);
                }
                _impulses.Enqueue(true);
                _changed = true;
            }

            public void Push(bool impulse)
            {
                lock (_locker)
                {
                    if (!_changed)
                    {
                        _impulses.Enqueue(impulse);
                        _changed = true;
                    }
                }
            }
            public void Pop()
            {
                lock (_locker)
                {
                    _impulses.Dequeue();
                    _changed = false;
                    if (_isOverflow && _overflowTimeout > 0)
                    {
                        _overflowTimeout--;
                        _isOverflow = _overflowTimeout > 0;
                    }
                }
            }
            public bool IsPointInPipe(int point)
            {
                return point >= Point - _range && point <= Point + _range;
            }

            public void Overflow()
            {
                _isOverflow = true;
                _overflowTimeout = _deepOverflow;
            }
        }
    }
}
