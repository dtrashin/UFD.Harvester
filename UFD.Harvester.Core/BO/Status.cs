﻿namespace UFD.Harvester.Core.BO
{
    public enum Status
    {
        Disabled = 0,
        Enabled = 1,
        Warning = 2,
        Alarm = 3,
        Broken = 4,
        Observation = 5,
        Activity = 6,
        Unknown = 255
    }
}
