﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace UFD.Harvester.Core.BO
{
    /// <summary>
    /// Барьер, логическая зоная для периметра.
    /// </summary>
    public class Barrier
    {
        private Dictionary<int, Fiber> _fibers = new Dictionary<int, Fiber>();
        private List<Activity> _activities = new List<Activity>();

        private Status _status;
        private int _brokenStart = int.MinValue;
        private int _brokenStop = int.MaxValue;

        /// <summary>
        /// Id барьера.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование барьера.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание барьера.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Расположение барьера на мнемосхеме.
        /// </summary>
        public Location Location { get; set; }

        /// <summary>
        /// Статус барьера.
        /// </summary>
        public Status Status { get; private set; }

        /// <summary>
        /// Id сектора, в который входит барьер.
        /// </summary>
        public Guid SectorId { get; set; }

        /// <summary>
        /// Активности в барьере.
        /// </summary>
        public List<Activity> Activities { get; } = new List<Activity>();

        // .ctor
        public Barrier() : this(Guid.NewGuid(), Status.Enabled) { }

        public Barrier(Guid id) : this(id, Status.Enabled) { }

        public Barrier(Status status) : this(Guid.NewGuid(), status) { }

        public Barrier(Guid id, Status status)
        {
            Id = id;
            Status = status;
        }

        /// <summary>
        /// Добавление события в барьер.
        /// </summary>
        public void AddImpact()
        {
            Status = Status.Alarm;
            Activities.Add(new Activity());
        }

        /// <summary>
        /// Смена статуса барьера.
        /// </summary>
        /// <param name="status">Статус барьера.</param>
        /// <returns></returns>
        public Status SetStatus(Status status)
        {
            try
            {
                lock (this)
                {
                    Status = status;
                    Activities.Clear();
                    //if (status == Status.Enabled || status == Status.Disabled)
                    //    _dampers.ForEach(d => d.Reset());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ": " + ex.StackTrace);
            }
            return Status;
        }
    }


    /// <summary>
    /// Структура для обределения координат барьера на мнемосхеме.
    /// </summary>
    public struct Location
    {
        public decimal X1 { get; set; }
        public decimal X2 { get; set; }
        public decimal Y1 { get; set; }
        public decimal Y2 { get; set; }
    }
}
