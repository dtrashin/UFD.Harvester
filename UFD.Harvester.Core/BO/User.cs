﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UFD.Harvester.Core.BO
{
    public class User
    {
        private readonly Queue<ZoneAction> _acts;

        public Guid Id { get; private set; }
        public string Login { get; private set; }
        public string Description { get; set; }
        //TODO: Изменить логику ролей
        public string Role { get; set; }
        [JsonIgnore]
        public List<ZoneAction> Acts
        {
            get
            {
                var acts = new List<ZoneAction>(_acts);
                _acts.Clear();
                return acts;
            }
        }

        public User(string login, List<ZoneAction> acts = null)
        {
            Login = login;
            Id = Guid.NewGuid();
            if (null == acts)
                _acts = new Queue<ZoneAction>();
            else
                _acts = new Queue<ZoneAction>(acts);
            //TODO: Изменить логику ролей
            if (login.ToLower().Contains("operator"))
                Role = "Operator";
            else
                Role = "Administrator";
        }

        //TODO: СУКА ВСЕ ПОМЕНЯТЬ !!!
        public User(Guid integrationId)
        {
            Id = integrationId;
            _acts = new Queue<ZoneAction>();
        }

        public void AddAct(ZoneAction act)
        {
            _acts.Enqueue(act);
            while (_acts.Count > 100)
                _acts.Dequeue();
        }
    }
}
