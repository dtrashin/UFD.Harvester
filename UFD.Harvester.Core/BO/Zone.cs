﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using NLog;
using UFD.Harvester.Core.Attributes;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.BO
{
    public class Zone : ValidatableObject
    {
        private readonly object _locker = new object();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly List<ZoneAction> _actions = new List<ZoneAction>();

        private Status _status;
        private int _brokenCount;
        private int _brokenStart = int.MinValue;
        private int _brokenStop = int.MaxValue;

        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [NotNegativeNumber]
        public double FiberX1 { get; set; }
        [NotNegativeNumber]
        public double FiberX2 { get; set; }
        public int RegionId { get; set; }
        public Guid ReceiverId { get; set; }
        //public ushort Device { get; set; }
        //public byte Chanel { get; set; }
        //public decimal sceneX_x1 { get; set; }
        //public decimal sceneY_x1 { get; set; }
        //public decimal sceneX_x2 { get; set; }
        //public decimal sceneY_x2 { get; set; }
        [JsonProperty(PropertyName = "sceneX_x1")]
        public decimal X1 { get; set; }
        [JsonProperty(PropertyName = "sceneY_x1")]
        public decimal Y1 { get; set; }
        [JsonProperty(PropertyName = "sceneX_x2")]
        public decimal X2 { get; set; }
        [JsonProperty(PropertyName = "sceneY_x2")]
        public decimal Y2 { get; set; }
        public bool Recognize { get; set; }
        [JsonProperty]
        internal List<Damper> Dampers { get; private set; } = new List<Damper>();
        [JsonProperty]
        internal DateTime ChangeStatus { get; set; } 

        public Status Status
        {
            get
            {
                if (IsBroken && _status != Status.Alarm)
                    return Status.Broken;
                return _status;
            }
            private set
            {
                if (_status != value)
                {
                    ChangeStatus = DateTime.Now;
                    //Logger.Info($@"zone {Id} change status {value}");
                }
                _status = value;
            }
        }
        [JsonIgnore]
        public bool IsBroken => _brokenCount > 0;

        [JsonIgnore]
        public List<ZoneAction> Actions => _actions;

        public Zone(Status status)
        {
            Status = status;
        }

        public void InitDampers(List<Damper> dampers)
        {
            Dampers = dampers;
        }

        public void BeginChecking()
        {
            if (Status == Status.Disabled) return;
            try
            {
                lock (_locker)
                {
                    _actions.Clear();
                    if (Dampers.Count > 0)
                    {
                        var status = Dampers.Max(d => d.Status);
                        status = status == 0 ? Status.Unknown : status;

                        if (Status != Status.Observation && Status != Status.Activity)
                            Status = Status == Status.Alarm ? Status.Alarm : IsBroken ? _status : status;
                        Dampers.ForEach(damper => damper.BeginChecking());
                    }
                    _brokenCount--;
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"BeginChecking(): {ex.Message}. {ex.StackTrace}");
            }
        }

        [Obsolete]
        public void AddPoint(int point)
        {
            if (Status == Status.Disabled) return;
            lock (_locker)
            {
                if (Status == Status.Disabled || Status == Status.Broken) return;
                try
                {
                    if (point >= FiberX1 && point <= FiberX2)
                    {
                        var damper = Dampers.FirstOrDefault(d => d.IsPointInDamper(point));
                        if (damper != null)
                        {
                            Status status = damper.CheckStatusPoint(point);
                            if (status == Status.Unknown)
                            {
                                Status = status;
                                return;
                            }
                            if (Status == Status.Alarm && status == Status.Alarm)
                                AddAction(point, status);
                            else if (status != Status &&
                                     (status == Status.Alarm || status == Status.Warning || status == Status.Broken))
                                AddAction(point, status);
                            if (status > Status)
                                Status = status;
                        }
                        else
                        {
                            AddAction(point, Status.Alarm);
                            if (Status < Status.Alarm)
                                Status = Status.Alarm;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message + ": " + ex.StackTrace);
                }
            }
        }

        public void AddImpact(Impact impact)
        {
            if (Status == Status.Disabled) return;
            if (impact.Point < FiberX1 || impact.Point > FiberX2) return;
            //if (impact.Point > 1120 && impact.Point < 1130) Debug.WriteLine("Point: " + impact.Point);
            lock (_locker)
            {
                try
                {
                    Status status = Status.Disabled;
                    var damper = Dampers.FirstOrDefault(d => d.IsPointInDamper(impact.Point));

                    if (damper != null)
                        status = damper.CheckStatusPoint(impact);
                    else
                    {
                        if (Status == Status.Observation || Status == Status.Activity)
                            Status = status = Status.Activity;
                        else
                            Status = status = Status.Alarm;
                    };

                    if (!Recognize) impact.RecognizeType = 255;
                    if (Status == Status.Alarm && status == Status.Alarm)
                        AddAction(impact, Status.Alarm);
                    else if ((Status == Status.Observation || Status == Status.Activity) && (status == Status.Alarm || status == Status.Warning))
                        AddAction(impact, Status.Activity);
                    else if (status != Status &&
                             (status == Status.Alarm || status == Status.Warning || status == Status.Broken || status == Status.Activity))
                        AddAction(impact, status);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message + ": " + ex.StackTrace);
                }
            }
        }

        public void EndChecking()
        {
            if (Status == Status.Disabled) return;
#if MGTS
            if (Status == Status.Alarm)
            {
                if (ChangeStatus < DateTime.Now.AddMinutes(-60))
                {
                    if (Dampers.Count > 0)
                        Dampers.ForEach(d => d.Reset());
                    Status = Status.Enabled;
                }
                return;
            }
#endif
            try
            {
                if (Dampers.Count > 0)
                {
                    var status = Dampers.Max(damper => damper.Status);
                    if ((Status == Status.Observation || Status == Status.Activity) && (status == Status.Alarm || status == Status.Warning))
                        status = Status.Activity;
                    if ((Status == Status.Observation || Status == Status.Activity) && (status == Status.Enabled))
                        status = Status.Observation;
                    Status = status;
                    Dampers.ForEach(d => d.EndChecking());
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message + ": " + ex.StackTrace);
            }
        }

        public Status SetStatus(Status status)
        {
            try
            {
                lock (_locker)
                {
                    Status = status;
                    //Logger.Info($@"zone {Id} SetStatus {Status}");
                    _actions.Clear();
                    if (status == Status.Enabled || status == Status.Disabled || status == Status.Observation)
                        Dampers.ForEach(d => d.Reset());
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message + ": " + ex.StackTrace);
            }
            return Status;
        }

        public void SetBroken(int start = 0, int stop = 0)
        {
            if (!(start <= FiberX2)) return;
            _brokenCount = 3;
            if (start != 0)
                _brokenStart = start;
            if (stop != 0)
                _brokenStop = stop;
        }

        public ZoneState GetZoneState()
        {
            return new ZoneState { ZoneId = Id, Description = Description, Status = (int)Status };
        }

        private void AddAction(Impact impact, Status status)
        {
            _actions.Add(new ZoneAction()
            {
                TimeTicket = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                ZoneId = Id,
                X = impact.Point,
                Status = status,
                RecognizeType = impact.RecognizeType
            });
        }

        [Obsolete]
        private void AddAction(int point, Status status)
        {
            _actions.Add(new ZoneAction
            {
                TimeTicket = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                ZoneId = Id,
                X = point,
                Status = status
            });
        }

    }

}
