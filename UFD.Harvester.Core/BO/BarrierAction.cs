﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UFD.Harvester.Core.Enums;

namespace UFD.Harvester.Core.BO
{
    public class BarrierAction
    {
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Time")]
        public string TimeTicket { get; set; }

        public Guid FiberId { get; set; }

        /// <summary>
        /// Координата по оптическому волокну
        /// </summary>
        public int FiberX { get; set; }

        /// <summary>
        /// Координата от начала барьера
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Порядковый номер зоны в барьере.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Текущее состояние
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Подтвержение
        /// </summary>
        public bool Confirmed { get; set; }

        /// <summary>
        /// Тип активност
        /// </summary>
        public RecognizeType RecognizeType { get; set; }

        /// <summary>
        /// Ткущее направление движения активности
        /// </summary>
        public Direction Direction { get; set; }

        public BarrierAction()
        {
            Id = Guid.NewGuid();
        }
    }
}
