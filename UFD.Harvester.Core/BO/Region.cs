﻿namespace UFD.Harvester.Core.BO
{
    public class Region
    {
        public int Id { get; set; }
        
        public int ParentId { get; set; }
        
        public Status Status { get; set; }
        
        public string Svg { get; set; }
        
        public string Description { get; set; }
    }
}
