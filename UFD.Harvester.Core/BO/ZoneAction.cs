﻿using System;
using System.IO;
using Newtonsoft.Json;
using UFD.Harvester.Core.Enums;

namespace UFD.Harvester.Core.BO
{
    public class ZoneAction
    {
        public Guid Id { get; set; }
        [JsonProperty(PropertyName = "Time")]
        public string TimeTicket { get; set; }
        public int ZoneId { get; set; }
        public int X { get; set; }
        public Status Status { get; set; }
        public bool Confirmed { get; set; }
        public long RecognizeType { get; set; }
        public Direction Direction { get; set; }
        //[JsonIgnore]
        //public ushort Size { get; private set; }
        //[JsonIgnore]
        //public byte[] Data { get; private set; }

        public ZoneAction()
        {
            Id = Guid.NewGuid();
        }

        public byte[] ToBuffer()
        {
            using (var ms = new MemoryStream())
            {
                using (var writer = new BinaryWriter(ms))
                {
                    writer.Write(BitConverter.GetBytes(GetSize()));
                    writer.Write(BitConverter.GetBytes(ZoneId));
                    writer.Write(BitConverter.GetBytes(X));
                    writer.Write(BitConverter.GetBytes((int)Status));
                    writer.Write(GetStringBytes(TimeTicket));
                }
                return ms.ToArray();
            }
        }

        public ushort GetSize()
        {
            var size = (ushort) (sizeof(int) + sizeof(int) + sizeof(Status));
            size += (ushort)(TimeTicket.Length * sizeof(char) + sizeof(char));
            return size;
        }

        private byte[] GetStringBytes(string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
