﻿namespace UFD.Harvester.Core.BO
{
    public class ZoneState
    {
        public int ZoneId { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
    }
}
