﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Core.Exceptions
{
    public class InvalidRequestParametersException : Exception
    {
        public InvalidRequestParametersException()
        {
        }

        public InvalidRequestParametersException(string message) : base(message)
        {
        }

        public InvalidRequestParametersException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidRequestParametersException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
