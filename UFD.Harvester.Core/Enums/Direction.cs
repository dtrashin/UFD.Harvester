﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Core.Enums
{
    public enum Direction
    {
        /// <summary>
        /// Направление не определено
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Приближающийся объект
        /// </summary>
        Oncoming = 1,

        /// <summary>
        /// Удаляющийся объект
        /// </summary>
        Receding = 2
    }
}
