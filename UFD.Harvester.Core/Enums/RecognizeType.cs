﻿namespace UFD.Harvester.Core.Enums
{
    public enum RecognizeType
    {
        Unknown = 0,
        Human = 1,
        Vehicle = 2
    }
}
