﻿using System;
using System.Collections.Generic;
using System.Linq;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.DAL;
using UFD.Harvester.Core.Perimeter;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.BL
{
    public class ZoneActionSendEventArgs : EventArgs
    {
        public readonly List<ZoneAction> Acts;
        public ZoneActionSendEventArgs(List<ZoneAction> acts)
        {
            Acts = acts;
        }
    }

    public class DatagramSendEventArgs : EventArgs
    {
        public readonly Datagram Datagram;
        public DatagramSendEventArgs(Datagram datagram)
        {
            Datagram = datagram;
        }
    }

    public class ZoneActionController
    {
        private static ZoneActionController _instance;
        private static readonly IDataFacade Facade = DataFacadeFactory.GetDataFacade();
        private readonly SecurityController _userController = SecurityController.GetController();
        private static List<DirectionAction> _directionActions = new List<DirectionAction>();

        public event EventHandler<ZoneActionSendEventArgs> ZoneActionSend;
        protected virtual void OnZoneActionSend(ZoneActionSendEventArgs e) => ZoneActionSend?.Invoke(this, e);

        public event EventHandler<DatagramSendEventArgs> DatagramSend;
        protected virtual void OnDatagramSend(DatagramSendEventArgs e)
        {
            DatagramSend?.Invoke(this, e);
        }

        private ZoneActionController()
        {
        }

        public static ZoneActionController GetController()
        {
            if (_instance != null) return _instance;
            try
            {
                _instance = new ZoneActionController();
            }
            catch (Exception)
            {
                _instance = null;
            }
            return _instance;
        }

        public DirectionAction GetDirectionAction(int point)
        {
            var directionActions = _directionActions.Where(a => a.IsPointInAction(point)).ToArray();
            if (!directionActions.Any()) return null;

            DirectionAction result = null;
            var distance = int.MaxValue;

            foreach (var act in directionActions)
            {
                var locDistantion = Math.Abs(point - act.Point);
                if (locDistantion >= distance) continue;
                result = act;
                distance = locDistantion;
            }
            return result;
        }

        public void UpdateDirectionActions(List<ZoneAction> actions)
        {
            var directionActions = new Dictionary<DirectionAction, List<ZoneAction>>();
            foreach (var directionAction in _directionActions)
            {
                if (directionAction.Count() > 0) continue;
                _directionActions.Remove(directionAction);
            }
            foreach (var zoneAction in actions)
            {
                var point = zoneAction.X - (1000 + 100 * zoneAction.ZoneId);
                //var point = zoneAction.ZoneId == 1
                //    ? zoneAction.X - 467
                //    : zoneAction.ZoneId == 2 ? 647 - zoneAction.X : zoneAction.X - 712;
                var directionAction = GetDirectionAction(point);

                if (directionAction == null)
                {
                    directionAction = new DirectionAction() {Point = point};
                    _directionActions.Add(directionAction);
                }
                List<ZoneAction> zoneActions;
                if (!directionActions.Keys.Contains(directionAction))
                {
                    zoneActions = new List<ZoneAction>();
                    directionActions[directionAction] = zoneActions;
                }
                zoneActions = directionActions[directionAction];
                zoneActions.Add(zoneAction);
            }
            foreach (var directionAction in directionActions)
            {
                directionAction.Key.Push(directionAction.Value);
            }
            _directionActions.ForEach(da => da.Push(new List<ZoneAction>()));
            _directionActions.ForEach(da => da.GetDirection());
            _directionActions.ForEach(da => da.Pop());
        }

        public void ClearDirectionActions()
        {
            _directionActions.Clear();
        }

        public void SendAct(List<ZoneAction> acts)
        {
#if MO
            UpdateDirectionActions(acts);
#endif
            acts.ForEach(a =>
            {
                Facade.LogAction(a);
                _userController.AddAct(a);
            });

            OnZoneActionSend(new ZoneActionSendEventArgs(acts));
        }

        public void SendDatagram(Datagram datagram)
        {
            OnDatagramSend(new DatagramSendEventArgs(datagram));
        }
    }
}
