﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using NLog;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.DAL;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.BL
{
    public class StatusController
    {
        private static StatusController _instance;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly object _locker = new object();
        private static readonly ZoneActionController ZoneActionController = ZoneActionController.GetController();
        private static readonly IDataFacade Facade = DataFacadeFactory.GetDataFacade();
        private static List<Region> _regions = Facade.GetRegions();
        private static List<Zone> _zones = Facade.GetZones();
        private readonly Stopwatch _sw = new Stopwatch();

        public List<Zone> Zones { get { return _zones.OrderBy(z => z.Id).ToList(); } }
        public List<Region> Regions { get { return _regions.OrderBy(r => r.Id).ToList(); } }

        private StatusController()
        {
        }

        public void Reload()
        {
            lock (_locker)
            {
                _regions = Facade.GetRegions();
                _zones = Facade.GetZones();
            }
        }

        public static StatusController GetController()
        {
            if (_instance != null) return _instance;
            try
            {
                _instance = new StatusController();
            }
            catch (Exception)
            {
                throw new Exception("Error on create status controller.");
            }

            return _instance;
        }

        public void CheckDatagram(Datagram datagram)
        {
            try
            {
                _sw.Reset();
                _sw.Start();
                if (datagram == null) return;
                Facade.LogRaw(datagram);
                var zones = _zones.Where(z => z.ReceiverId == datagram.ReceiverId).ToList();
                if (datagram.DatagramType == 1)
                    SetBroken(datagram.Points.FirstOrDefault(), zones);
                else
                {
                    zones = zones.Where(zone => zone.Status != Status.Disabled).ToList();
                    zones.ForEach(zone => zone.BeginChecking());
                    datagram.Impacts.ForEach(impact => zones.ForEach(zone => zone.AddImpact(impact)));
                    zones.ForEach(zone => zone.EndChecking());
                    ZoneActionController.SendAct(zones.Select(z => z.Actions).SelectMany(x => x).ToList());
                }
                foreach (var zone in zones)
                {
                    var z = zone;
                    Facade.UpdateZoneStatus(z.Id, z.Status);
                }
                _sw.Stop();
                //Debug.WriteLine("Check datagram time (ms): {0}", _sw.ElapsedMilliseconds);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        public Dictionary<int, Status> GetRegionStatus(int regionId)
        {
            var result = new Dictionary<int, Status>();
            if (regionId > 0)
            {
                var region = _regions.FirstOrDefault(r => r.Id == regionId);
                if (region != null)
                {
                    var zones = _zones.Where(z => z.RegionId == region.Id).ToList();
                    result.Add(region.Id, zones.Any() ? zones.Max(z => z.Status) : Status.Unknown);
                }
                else
                    result.Add(regionId, Status.Unknown);
            }
            else
            {
                foreach (var region in _regions.Where(r => r.Id != 0).AsParallel())
                {
                    var zones = _zones.Where(z => z.RegionId == region.Id).ToList();
                    result.Add(region.Id, zones.Any() ? zones.Max(z => z.Status) : Status.Unknown);
                }
            }
            return result;
        }

        public Status SetRegionStatus(int regionId, Status status, bool reset = false)
        {
            var region = _regions.FirstOrDefault(r => r.Id == regionId);
            if (region == null)
                return Status.Unknown;
            if (status != Status.Disabled && status != Status.Enabled)
                return region.Status;

            var regions = GetChildrenRegions(region);

            regions.ForEach(reg =>
            {
                lock (_locker)
                {
                    SetRegionStatus(reg, status);
                    _zones.Where(z => z.RegionId == reg.Id).ToList().ForEach(z => { z.SetStatus(reg.Status); Facade.UpdateZoneStatus(z.Id, reg.Status); });
                }
            });
            return region.Status;
        }

        public Dictionary<int, Status> GetStatuses()
        {
            var result = new Dictionary<int, Status>();
            foreach (var region in _regions.Where(r => r.Id != 0))
            {
                var zones = _zones.Where(z => z.RegionId == region.Id).ToList();
                result[region.Id] = zones.Select(z => z.Status).Max();
            }
            return result;
        }

        public Dictionary<int, Status> GetZoneStatus(int zoneId)
        {
            var result = new Dictionary<int, Status>();
            if (zoneId > 0)
            {
                var zone = _zones.FirstOrDefault(z => z.Id == zoneId);
                result.Add(zoneId, zone?.Status ?? Status.Unknown);
            }
            else
            {
                foreach (var zone in _zones)
                {
                    var z = zone;
                    result.Add(z.Id, z.Status);
                }
            }
            return result;
        }

        public Status SetZoneStatus(int zoneId, Status status)
        {
            var zone = _zones.FirstOrDefault(z => z.Id == zoneId);
            if (zone == null)
                return Status.Unknown;
            if (status != Status.Disabled && status != Status.Enabled)
                return zone.Status;
            zone.SetStatus(status);
            Facade.UpdateZoneStatus(zone.Id, status);
            return zone.Status;
        }

        private List<Region> GetChildrenRegions(Region region)
        {
            var regions = _regions.Where(r => r.ParentId == region.Id).Concat(_regions.Where(r => r.ParentId == region.Id).SelectMany(GetChildrenRegions)).Distinct().ToList();
            regions.Add(region);
            return regions;
        }

        private static void SetRegionStatus(Region region, Status status)
        {
            switch (status)
            {
                case Status.Disabled:
                    if (region.Status == Status.Alarm || region.Status == Status.Enabled || region.Status == Status.Warning)
                        region.Status = Status.Disabled;
                    break;
                case Status.Enabled:
                    if (region.Status == Status.Alarm || region.Status == Status.Disabled || region.Status == Status.Warning)
                        region.Status = Status.Enabled;
                    break;
                case Status.Alarm:
                    if (region.Status == Status.Enabled || region.Status == Status.Warning)
                        region.Status = Status.Alarm;
                    break;
                case Status.Broken:
                    if (region.Status == Status.Alarm || region.Status == Status.Disabled || region.Status == Status.Enabled || region.Status == Status.Warning)
                        region.Status = Status.Broken;
                    break;
                case Status.Warning:
                    break;
            }
        }

        private static void SetBroken(int point, IEnumerable<Zone> zones)
        {
            var acts = new List<ZoneAction>();
            zones.Where(z => z.FiberX2 > point && z.Status != Status.Broken).ToList().ForEach(bz =>
            {
                bz.SetBroken(point);
                acts.Add(new ZoneAction
                {
                    TimeTicket = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                    ZoneId = bz.Id,
                    X = point,
                    Status = Status.Broken
                });
            });
            ZoneActionController.SendAct(acts);
        }
    }
}
