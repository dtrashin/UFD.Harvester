﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NLog;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.Perimeter;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Core.BL
{
    public class StateController
    {
        private static StateController _instance;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Stopwatch _sw = new Stopwatch();

        private readonly PerimeterBase _perimeter = new PerimeterBase();

        private StateController() { }

        public static StateController GetController()
        {
            if (_instance != null) return _instance;
            try
            {
                _instance = new StateController();
            }
            catch (Exception)
            {
                throw new Exception("Error on create state controller.");
            }
            return _instance;
        }

        //public Sector UpdateSector(Sector sector)
        //{
        //    //if (!DataFacade.UpdateSector(sector)) return null;
        //    //var sect = Sectors.FirstOrDefault(s => s.Id == sector.Id);
        //    //if (sect != null)
        //    //    Sectors.Remove(sect);
        //    //Sectors.Add(sector);
        //    return sector;
        //}

        //public bool DeleteSector(Sector sector)
        //{
        //    return true;
        //}

        //public Barrier UpdateBarrier(Barrier barrier)
        //{
        //    //if (!DataFacade.UpdateBarrier(barrier)) return null;
        //    //var bar = Barriers.FirstOrDefault(b => b.Id == barrier.Id);
        //    //if (bar != null)
        //    //    Barriers.Remove(bar);
        //    //Barriers.Add(barrier);
        //    return barrier;
        //}

        //public bool DeleteBarrier(Barrier barrier)
        //{
        //    return true;
        //}

        public void CheckDatagram(Datagram datagram)
        {
            try
            {
#if DEBUG
                StackTrace stackTrace = new StackTrace();           // get call stack
                StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

                // write call stack method names
                foreach (StackFrame stackFrame in stackFrames)
                {
                    Console.WriteLine(stackFrame.GetMethod().Name);   // write method name
                }
#endif

                _sw.Reset();
                _sw.Start();
                if (datagram == null) return;
                //DataFacade.LogRaw(datagram);
                _perimeter.CheckDatagram(datagram);
                _sw.Stop();
                //Debug.WriteLine($"datagram {datagram.Impacts.Count}");
                //Debug.WriteLine("Check datagram time (ms): {0}", _sw.ElapsedMilliseconds);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        //public Dictionary<Guid, Status> GetSectorStatus(Guid sectorId)
        //{
        //    var result = new Dictionary<Guid, Status>();
        //    if (sectorId != Guid.Empty)
        //    {
        //        var sector = Sectors.FirstOrDefault(s => s.Id == sectorId);
        //        if (sector != null)
        //        {
        //            var barriers = Barriers.Where(b => b.SectorId == sector.Id).ToList();
        //            result.Add(sector.Id, barriers.Any() ? barriers.Max(b => b.Status) : Status.Unknown);
        //        }
        //        else
        //            result.Add(sectorId, Status.Unknown);
        //    }
        //    else
        //    {
        //        foreach (var region in Sectors.Where(s => s.Id != Guid.Empty).AsParallel())
        //        {
        //            var barriers = Barriers.Where(b => b.SectorId == region.Id).ToList();
        //            result.Add(region.Id, barriers.Any() ? barriers.Max(b => b.Status) : Status.Unknown);
        //        }
        //    }
        //    return result;
        //}

        public List<Region> GetRegions()
        {
            return _perimeter.Regions;
        }

        public Dictionary<int, Status> GetRegionStatus(int regionId)
        {
            return _perimeter.GetRegionStatus(regionId);
        }

        public Status SetRegionStatus(int regionId, Status status, bool reset = false)
        {
            return _perimeter.SetRegionStatus(regionId, status, reset);
        }

        public List<Zone> GetZones()
        {
            return _perimeter.Zones.OrderBy(z => z.FiberX1).ToList();
        }


        public Dictionary<int, Status> GetZoneStatus(int zoneId)
        {
            return _perimeter.GetZoneStatus(zoneId);
        }

        public List<ZoneState> GetZoneState(int zoneId)
        {
            return _perimeter.GetZoneState(zoneId);
        }

        public Status SetZoneStatus(int zoneId, Status status)
        {
            return _perimeter.SetZoneStatus(zoneId, status);
        }

        public bool UpdateZone(Zone zone)
        {
            return _perimeter.UpdateZone(zone);
        }


        public List<Damper> GetDampers(int zoneId)
        {
            var result = _perimeter.Zones.FirstOrDefault(z => z.Id == zoneId);
            return result?.Dampers;
        }     

        //public bool SetSectorStatus(Guid sectorId, Status status, bool reset = false)
        //{
        //    var sector = Sectors.FirstOrDefault(s => s.Id == sectorId);
        //    if (sector == null)
        //        return false;
        //    if (status != Status.Disabled && status != Status.Enabled)
        //        return false;

        //    var sectors = GetChildrenSectors(sector);

        //    sectors.ForEach(s =>
        //    {
        //        lock (this)
        //        {
        //            SetSectorStatus(s.Id, status);
        //            Barriers.Where(b => b.SectorId == s.Id).ToList().ForEach(b => { b.SetStatus(s.Status); Facade.UpdateZoneStatus(z.Id, reg.Status); });
        //        }
        //    });
        //    return true;
        //}

        //private List<Sector> GetChildrenSectors(Sector sector)
        //{
        //    var sectors = Sectors.Where(s => s.ParentId == sector.Id).Concat(Sectors.Where(s => s.ParentId == sector.Id).SelectMany(GetChildrenSectors)).Distinct().ToList();
        //    sectors.Add(sector);
        //    return sectors;
        //}

        //public Dictionary<Guid, Status> GetBarrierStatus(Guid barrierId)
        //{
        //    var result = new Dictionary<Guid, Status>();
        //    //if (barrierId != Guid.Empty)
        //    //{
        //    //    var barrier = Barriers.FirstOrDefault(b => b.Id == barrierId);
        //    //    result.Add(barrierId, barrier?.Status ?? Status.Unknown);
        //    //}
        //    //else
        //    //{
        //    //    foreach (var barrier in Barriers)
        //    //    {
        //    //        var b = barrier;
        //    //        result.Add(b.Id, b.Status);
        //    //    }
        //    //}
        //    return result;
        //}

        //public Status SetBarrierStatus(Guid barrierId, Status status)
        //{
        //    var barrier = Barriers.FirstOrDefault(b => b.Id == barrierId);
        //    if (barrier == null)
        //        return Status.Unknown;
        //    if (status != Status.Disabled && status != Status.Enabled)
        //        return barrier.Status;
        //    barrier.SetStatus(status);
        //    DataFacade.UpdateBarrierStatus(barrier);
        //    return barrier.Status;
        //}
    }
}
