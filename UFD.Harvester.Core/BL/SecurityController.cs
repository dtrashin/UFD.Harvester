﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Core.DAL;
using UFD.Harvester.Core.Helpers;
using UFD.Harvester.Receivers;
using System.Net;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Security.Permissions;

namespace UFD.Harvester.Core.BL
{
    public class SecurityController
    {
#if DEBUG
        public static Guid AdminSession = new Guid("FFD910E9-D71B-4809-84D2-C84A8A060269");
#endif
        private static readonly Guid SmsServiceId = Guid.Parse("E461B5FF-87D9-4095-BD29-2706D8818007");
        private static SecurityController _instance;
        private static readonly List<User> Users = new List<User>();
        private static readonly Dictionary<Guid, User> DictUsers = new Dictionary<Guid, User>();
        private readonly IDataFacade _facade = DataFacadeFactory.GetDataFacade();

        private SecurityController()
        {
        }

        public bool CheckUserAdmin(Guid userId)
        {
#if DEBUG 
            if (userId == AdminSession) return true;
#endif
            if (!CheckUser(userId)) return false;
            if (GetUser(userId).Login != "administrator") return false;
            return true;
        }
        public static SecurityController GetController()
        {
            return _instance ?? (_instance = new SecurityController());
        }

        public bool CheckUser(Guid userId)
        {
            return Users.Any(u => u.Id == userId);
        }

        public User GetUser(Guid userId)
        {
            User user;
            return DictUsers.TryGetValue(userId, out user) ? user : null;
        }

        public List<Range> GetRanges(Guid rangeId)
        {
            if (rangeId == default(Guid))
                return _facade.GetRanges();
            else
                return new List<Range>() { _facade.GetRange(rangeId) };
        }
        public Range SetRange(Range range)
        {
            return _facade.SetRange(range);
        }
        public List<User> GetUsers()
        {
            return DictUsers.Values.ToList();
        }

        public User RegisterUser(string login, string password)
        {
            if (!_facade.Login(login, password)) return null;

            var usr = Users.FirstOrDefault(u => login == u.Login);
            if (usr != null)
                UnregisterUser(usr.Id);

            DateTime timeToLastLogin = _facade.TimeToLastRequest(login);
            List<ZoneAction> actions = _facade.GetActionsHistory(timeToLastLogin.ToString(CultureInfo.InvariantCulture), DateTime.Now.ToString(CultureInfo.InvariantCulture));
            var user = new User(login, actions);
            user = _facade.GetUserInfo(user);
            DictUsers.Add(user.Id, user);
            Users.Add(user);
            LogSystemAction(user.Login, "Система", "Вход в систему");
            return user;
        }

        public void UnregisterUser(Guid userId)
        {
            var user = Users.FirstOrDefault(u => u.Id == userId);
            if (user != null)
            {
                LogSystemAction(user.Login, "Система", "Выход из системы");
                Users.Remove(user);
            }
        }

        public List<ZoneAction> GetUserActions(Guid userId)
        {
            List<ZoneAction> actions;
            var user = Users.FirstOrDefault(u => u.Id == userId);
            if (user != null)
            {
                
                actions = user.Acts.Where(ac => ac.Status == Status.Alarm).ToList();
                // HARDCODE для Льва. :(
                if (user.Login.Contains("guardgui") && actions.Any())
                {
                    actions = actions.Where(act => DateTime.Parse(act.TimeTicket) > DateTime.Now.AddSeconds(-2)).ToList();
                }
                _facade.UpdateLastRequest(user.Login);
            }
            else
            {
                actions = new List<ZoneAction>();
            }
            return actions;
        }

        public List<ZoneAction> GetActionsHistory(string from, string to, int actionType = 0, int? regionId = null)
        {
            return _facade.GetActionsHistory(from, to, actionType, regionId);
        }

        public void AddAct(ZoneAction act)
        {
            Users.ForEach(u => u.AddAct(act));
        }

        public bool ConfirmAction(Guid actionId, bool status)
        {
            try
            {
                ZoneAction action = _facade.GetAction(actionId);
                if (action == null) return false;
                action.Confirmed = status;
                _facade.UpdateAction(action);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<SystemAction> GetSystemActions(string from, string to)
        {
            return _facade.GetSystemActions(from, to);
        }

        public Zone SetZoneStatus(Guid userId, int zoneId, Status status)
        {
            User user;
            if (!DictUsers.TryGetValue(userId, out user)) return null;
            var zone = StateController.GetController().GetZones().FirstOrDefault(z => z.Id == zoneId);
            if (zone == null) return null;
            var zoneStatus = StateController.GetController().SetZoneStatus(zone.Id, status);
            if (zoneStatus == Status.Disabled)
                LogSystemAction(user.Login, zone.Description, "Снятие с охраны");
            if (zoneStatus == Status.Enabled)
                LogSystemAction(user.Login, zone.Description, "Постановка на охрану");
            return zone;
        }

        public SystemAction LogSystemAction(Guid userId, string data)
        {
            var user = GetUser(userId);
            if (user == null)
                throw new Exception("User not registered.");
            //return systemAction; 

            //var obj = systemAction["Object"] != null ? systemAction["Object"].ToString() : "";
            ////var subj = systemAction["Subject"] != null ? systemAction["Subject"].ToString() : "";
            //var act = systemAction["Action"] != null ? systemAction["Action"].ToString() : "";

            var systemAction = LogSystemAction("Test Object", user.Login, "Test Action");
            return systemAction;
        }

        public Zone SmsServiceSetZoneStatus(Guid userId, int zoneId, Status status, string subject, string description)
        {
            if (userId != SmsServiceId)
                throw new Exception("SmsService not registered.");
            var zone = StateController.GetController().GetZones().FirstOrDefault(z => z.Id == zoneId);
            if (zone == null)
                throw new Exception($"Zone {zoneId} not found.");
            var zoneStatus = StateController.GetController().SetZoneStatus(zone.Id, status);
            if (zoneStatus == Status.Disabled)
                LogSystemAction(subject, zone.Description, "Снятие с охраны", description);
            if (zoneStatus == Status.Enabled)
                LogSystemAction(subject, zone.Description, "Постановка на охрану", description);
            return zone;
        }

        public List<Zone> SmsServiceGetZones(Guid userId)
        {
            if (userId != SmsServiceId)
                throw new Exception("SmsService not registered.");
            return StateController.GetController().GetZones();
        }

        public SystemAction LogCortexState(string obj, string act, string desc = "")
        {
            return LogSystemAction("cortex", obj, act);
        }

        private SystemAction LogSystemAction(string subj, string obj, string act, string desc = "")
        {
            if (string.IsNullOrEmpty(obj)) throw new ArgumentNullException("obj");
            //if (string.IsNullOrEmpty(subj)) throw new ArgumentNullException("Subject");
            if (string.IsNullOrEmpty(act)) throw new ArgumentNullException("act");
            var systemAction = new SystemAction
            {
                Subject = subj,
                Object = obj,
                Action = act,
                Description = desc
            };
            _facade.LogSystemAction(systemAction);
            return systemAction;
        }

        #region Integration
        public List<Zone> IntegrationGetZones(Guid integrationId)
        {
            if (!IntegrationHelper.CheckId(integrationId))
                throw new Exception("Integration service not registered.");
            return StateController.GetController().GetZones();
        }

        public List<ZoneState> IntegrationGetZoneState(Guid integrationId, int zoneId, string subject, string description)
        {
            if (!IntegrationHelper.CheckId(integrationId))
                throw new Exception("Integration service not registered.");
            
            //    var zone = StateController.GetController().GetZones().FirstOrDefault(z => z.Id == zoneId);
            //    if (zone == null && zoneId > 0)
            //        throw new Exception("Zone not found.");
            

            //LogSystemAction(subject, zoneId == 0 ? "Все зоны" : zone.Description, "Запрос состояния зон(ы).", description);
            return StateController.GetController().GetZoneState(zoneId);
        }

        public Zone IntegrationSetZoneState(Guid integrationId, int zoneId, Status state, string subject, string description)
        {
            if (!IntegrationHelper.CheckId(integrationId))
                throw new Exception("Integration service not registered.");
            var zone = StateController.GetController().GetZones().FirstOrDefault(z => z.Id == zoneId);
            if (zone == null)
                throw new Exception($"Zone {zoneId} not found.");
            var zoneStatus = StateController.GetController().SetZoneStatus(zone.Id, state);
            if (zoneStatus == Status.Disabled)
                LogSystemAction(subject, zone.Description, "Снятие с охраны", description);
            if (zoneStatus == Status.Enabled)
                LogSystemAction(subject, zone.Description, "Постановка на охрану", description);
            if (zoneStatus == Status.Observation)
                LogSystemAction(subject, zone.Description, "Постановка под наблюдение", description);
            return zone;
        }

        public List<ZoneAction> IntegrationGetActions(Guid integrationId)
        {
            if (!IntegrationHelper.CheckId(integrationId))
                throw new Exception("Integration service not registered.");
            List<ZoneAction> actions;

            var user = Users.FirstOrDefault(u => u.Id == integrationId);
            if (user != null)
            {
                actions = user.Acts;
            }
            else
            {
                Users.Add(new User(integrationId));
                actions = new List<ZoneAction>();
            }
            return actions;

        }
        #endregion Integration

    }
}
