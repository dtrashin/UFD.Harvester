﻿using System.Configuration;

namespace UFD.Harvester.Core.DAL
{
    class DataFacadeConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Id", IsRequired = true, DefaultValue = "Test")]
        public string Id
        {
            get { return (string)this["Id"]; }
            set { this["Id"] = value; }
        }

        public static DataFacadeConfiguration GetConfig()
        {
            DataFacadeConfiguration config = null;
            try
            {
                var execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config = (DataFacadeConfiguration)execonfig.Sections["Harvester.DataFactory"];
            }
            catch
            {
                // ignored
            }
            return config;
        }
    }
}
