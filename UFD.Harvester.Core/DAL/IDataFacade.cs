﻿using System;
using System.Collections.Generic;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Receivers;

namespace UFD.Harvester.Core.DAL
{
    public interface IDataFacade
    {
        //void LogDatagram(Datagram datagram);
        
        void LogAction(ZoneAction action);
        
        void UpdateAction(ZoneAction action);
        
        void LogRaw(Datagram datagram);

        void LogSystemAction(SystemAction message);
        
        List<SystemAction> GetSystemActions(string from, string to);

        List<Region> GetRegions();
        
        List<Zone> GetZones();

        Zone GetZone(int zoneid);

        bool UpdateZone(Zone zone);

        bool DeleteZone(Zone zone);

        List<ZoneAction> GetActionsHistory(string from, string to, int actionType = 0, int? regionId = null);
        
        ZoneAction GetAction(Guid actionId);

        Dictionary<int, Status> GetZoneStatus(int zoneId);
        
        void UpdateZoneStatus(int zoneId, Status status);

        bool Login(string login, string pass);
        
        void Logout(string login);
        
        void UpdateLastRequest(string login);
        
        DateTime TimeToLastRequest(string login);
        
        User GetUserInfo(User user);

        List<Sector> GetSectors();
        bool UpdateSector(Sector sector);
        bool DeleteSector(Sector sector); 

        List<Barrier> GetBarriers();
        bool UpdateBarrier(Barrier barrier);
        bool DeleteBarrier(Barrier barrier);
        bool UpdateBarrierStatus(Barrier barrier);

        Range GetRange(Guid rangeId);
        List<Range> GetRanges();
        Range SetRange(Range range);
    }
}
