﻿using System;
using System.Collections.Generic;
using System.Linq;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Receivers;

namespace UFD.Harvester.Core.DAL.Local
{
    public class LocalDataFacade : IDataFacade
    {
        private static readonly List<Region> Regions = InitRegions();
        private static readonly List<Zone> Zones = InitZones();
        private static readonly List<User> Users = InitUsers();
        private static readonly List<ZoneAction> Actions = new List<ZoneAction>();
        private static readonly List<SystemAction> SystemActions = new List<SystemAction>();

        public bool DeleteZone(Zone zone)
        {
            throw new NotImplementedException();
        }

        public List<ZoneAction> GetActionsHistory(string from, string to, int actionType = 0, int? regionId = null)
        {
            var dtFrom = DateTime.Parse(from);
            var dtTo = DateTime.Parse(to);
            return Actions.Where(a => DateTime.Parse(a.TimeTicket) >= dtFrom && DateTime.Parse(a.TimeTicket) <= dtTo).ToList();
        }


        public ZoneAction GetAction(Guid actionId)
        {
            return Actions.FirstOrDefault(a => a.Id == actionId);
        }

        public List<Region> GetRegions()
        {
            return Regions;
        }
        public User GetUserInfo(User user)
        {
            var usr = Users.FirstOrDefault(u => u.Login == user.Login);
            user.Description = usr == null ? "Неизвестный" : usr.Description;
            return user;
        }

        public List<Sector> GetSectors()
        {
            throw new NotImplementedException();
        }

        public bool UpdateSector(Sector sector)
        {
            throw new NotImplementedException();
        }

        public bool DeleteSector(Sector sector)
        {
            throw new NotImplementedException();
        }

        public List<Barrier> GetBarriers()
        {
            throw new NotImplementedException();
        }

        public bool UpdateBarrier(Barrier barrier)
        {
            throw new NotImplementedException();
        }

        public bool DeleteBarrier(Barrier barrier)
        {
            throw new NotImplementedException();
        }

        public bool UpdateBarrierStatus(Barrier barrier)
        {
            return true;
        }

        public List<Zone> GetZones()
        {
            return Zones;
        }

        public Zone GetZone(int zoneid)
        {
            throw new NotImplementedException();
        }

        public bool UpdateZone(Zone zone)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, Status> GetZoneStatus(int zoneId)
        {
            var result = new Dictionary<int, Status>();
            Zones.ForEach(z => result.Add(z.Id, z.Status));
            return result;
        }

        public void LogAction(ZoneAction action)
        {
            Actions.Add(action);
        }

        public void UpdateAction(ZoneAction action)
        {
            var act = Actions.FirstOrDefault(a => a.Id == action.Id);
            if (act != null)
                act.Confirmed = action.Confirmed;
        }

        public bool Login(string login, string pass)
        {
            var user = Users.FirstOrDefault(u => u.Login == login);
            return user != null;
        }
        public void Logout(string login)
        {
        }
        public void LogRaw(Datagram datagram)
        {
        }
        //public void LogDatagram(Datagram datagram)
        //{
        //}
        public DateTime TimeToLastRequest(string login)
        {
            return DateTime.Now;
        }
        public void UpdateLastRequest(string login)
        {
        }
        public void UpdateZoneStatus(int zoneId, Status status)
        {
            var zone = Zones.FirstOrDefault(z => z.Id == zoneId);
            zone?.SetStatus(status);
        }

        private static List<Region> InitRegions()
        {
            var result = new List<Region>();
            var region = new Region
            {
                Id = 0,
                ParentId = -1,
                Description = "КБК",
                Svg = string.Empty
            };
            result.Add(region);

            for (int i = 1; i <= 20; i++)
            {
                region = new Region
                {
                    Id = i,
                    ParentId = 0
                };
                region.Description = "Раздел " + region.Id;
                region.Svg = string.Empty;
                region.Status = Status.Enabled;
                result.Add(region);
            }
            return result;
        }
        private static List<Zone> InitZones()
        {
            var result = new List<Zone>();
            var receiverId = Guid.Parse("0EBD712F-F078-414A-B2E3-E76B5BD1714B");

            for (var i = 1; i <= Regions.Count - 1; i++)
            {
                var zone = new Zone(Status.Enabled)
                {
                    Id = i,
                    FiberX1 = 1 + (i - 1) * 250,
                    FiberX2 = i * 250,
                    ReceiverId = receiverId,
                    RegionId = i,
                    Description = "Зона " + i
                };
                zone.X1 = zone.X2 = i + 10;
                zone.Y1 = zone.Y1 = zone.X1 + 50;
                InitDampers(zone);
                result.Add(zone);

                //var zone1 = new Zone(Status.Enabled)
                //{
                //    Id = i + Regions.Count - 1,
                //    FiberX1 = 1 + (i - 1) * 250,
                //    FiberX2 = i * 250,
                //    Chanel = chanel + 1,
                //    Device = device,
                //    RegionId = i,
                //    Description = "Зона " + (i + Regions.Count - 1)
                //};
                //zone1.X1 = zone1.X2 = i + 10;
                //zone1.Y1 = zone1.Y1 = zone1.X1 + 50;
                //InitDampers(zone1);
                //result.Add(zone1);
            }
            return result;
        }
        private static void InitDampers(Zone zone)
        {
            var dampers = new List<Damper>
            {
                new Damper
                {
                    X1 = (int)zone.FiberX1,
                    X2 = (int)zone.FiberX2,
                    Timeout = 10,
                    WarningStatusCount = 3,
                    AlarmStatusCount = 5,
                    Range = 10,
                    Split = false,
                    Enable = true,
                }
            };
            zone.InitDampers(dampers);
        }
        private static List<User> InitUsers()
        {
            var result = new List<User>();
            var user = new User("operator")
            {
                Description = "Оператор"
            };
            result.Add(user);
            return result;
        }

        public void LogSystemAction(SystemAction message)
        {
            SystemActions.Add(message);
        }

        public List<SystemAction> GetSystemActions(string from, string to)
        {
            var result = new List<SystemAction>();
            DateTime dtFrom;
            if (!DateTime.TryParse(from, out dtFrom)) return result;
            DateTime dtTo;
            return !DateTime.TryParse(to, out dtTo) ? result : SystemActions.Where(sa => sa.DateTime >= dtFrom && sa.DateTime <= dtTo).ToList();
        }

        public Range GetRange(Guid rangeId)
        {
            throw new NotImplementedException();
        }

        public List<Range> GetRanges()
        {
            throw new NotImplementedException();
        }

        public Range SetRange(Range range)
        {
            throw new NotImplementedException();
        }
    }
}
