﻿using UFD.Harvester.Core.DAL.Local;
using UFD.Harvester.Core.DAL.PSQL;

namespace UFD.Harvester.Core.DAL
{
    public class DataFacadeFactory
    {
        public static IDataFacade GetDataFacade()
        {
            var config = DataFacadeConfiguration.GetConfig();
            if (config == null) return new LocalDataFacade();

            IDataFacade facade;
            switch (config.Id)
            {
                case "PSQL":
                    facade = new PgsqlDataFacade();
                    break;
                default:
                    facade = new LocalDataFacade();
                    break;
            }
            return facade;
        }
    }
}
