﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Npgsql;
using NpgsqlTypes;
using UFD.Harvester.Core.BO;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Receivers;
using System.Linq;

namespace UFD.Harvester.Core.DAL.PSQL
{
    public class PgsqlDataFacade : IDataFacade
    {
        private static readonly PgsqlDataFacadeConfiguration Config = PgsqlDataFacadeConfiguration.GetConfig();

        public void LogSystemAction(SystemAction message)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                const string query = "insert into system_log values(:dt, :subject, :object, :action, :description)";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.AddWithValue("dt", NpgsqlDbType.Timestamp, message.DateTime.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.Parameters.AddWithValue("subject", NpgsqlDbType.Text, message.Subject);
                cmd.Parameters.AddWithValue("object", NpgsqlDbType.Text, message.Object);
                cmd.Parameters.AddWithValue("action", NpgsqlDbType.Text, message.Action);
                cmd.Parameters.AddWithValue("description", NpgsqlDbType.Text, message.Description);
                cmd.ExecuteNonQuery();
            }
        }

        public List<SystemAction> GetSystemActions(string from, string to)
        {
            var result = new List<SystemAction>();
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                var query = "select dt, subject, object, action, description from system_log where dt between :from and :to order by dt desc";
                var cmd = new NpgsqlCommand(query, conn);
                DateTime dtFrom;
                if (!DateTime.TryParse(from, out dtFrom)) return result;
                DateTime dtTo;
                if (!DateTime.TryParse(to, out dtTo)) return result;
                cmd.Parameters.AddWithValue("from", NpgsqlDbType.Timestamp, dtFrom.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.Parameters.AddWithValue("to", NpgsqlDbType.Timestamp, dtTo.ToString("yyyy-MM-dd HH:mm:ss"));
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DateTime dt;
                    if (!DateTime.TryParse(rdr[0].ToString(), out dt)) continue;
                    var systemLogMessage = new SystemAction(dt)
                    {
                        Subject = rdr[1].ToString(),
                        Object = rdr[2].ToString(),
                        Action = rdr[3].ToString(),
                        Description = rdr[4].ToString()
                    };
                    result.Add(systemLogMessage);
                }
            }
            return result;
        }

        //public void LogDatagram(Datagram datagram)
        //{
        //    using (var conn = new NpgsqlConnection(Config.ConnectionString))
        //    {
        //        if (conn.State != System.Data.ConnectionState.Open)
        //            conn.Open();

        //        var query = string.Format(@"insert into signals_log 
        //                                    (datetime, device, chanel, type, points)
        //                                    values('{0}', {1}, {2}, '{3}', '{{{4}}}')"
        //        , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), datagram.DeviceId, datagram.ChanelId, datagram.DatagramType, string.Join(",", datagram.Points.ToArray()));
        //        var cmd = new NpgsqlCommand(query, conn);
        //        cmd.ExecuteNonQuery();
        //    }
        //}

        public void LogAction(ZoneAction action)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var query = "insert into actions (id, datetime, zone_id, x, status, RecognizeType) values(:id, :dt, :zoneId, :x, :status, :recognizeType)";
                //, action.TimeTicket, action.ZoneId, action.X, (int)action.Status);
                var cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.Add("id", NpgsqlDbType.Uuid).Value = action.Id;
                cmd.Parameters.Add("dt", NpgsqlDbType.Timestamp).Value = action.TimeTicket;
                cmd.Parameters.Add("zoneId", NpgsqlDbType.Integer).Value = action.ZoneId;
                cmd.Parameters.Add("x", NpgsqlDbType.Integer).Value = action.X;
                cmd.Parameters.Add("status", NpgsqlDbType.Integer).Value = action.Status;
                cmd.Parameters.Add("recognizeType", NpgsqlDbType.Bigint).Value = action.RecognizeType;
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateAction(ZoneAction action)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                const string query = "update actions set confirm = :confirmed where id = :actionId";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.Add("actionId", NpgsqlDbType.Uuid).Value = action.Id;
                cmd.Parameters.Add("confirmed", NpgsqlDbType.Boolean).Value = action.Confirmed;
                cmd.ExecuteNonQuery();
            }
        }

        public void LogRaw(Datagram datagram)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                var cmd = new NpgsqlCommand($"insert into receiver_log (dt, receiver_id, raw) values('{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', :receiverId, :raw)", conn);
                cmd.Parameters.Add("receiverId", NpgsqlDbType.Uuid).Value = datagram.ReceiverId;
                cmd.Parameters.Add("raw", NpgsqlDbType.Bytea).Value = datagram.Raw.ToArray();
                cmd.ExecuteNonQuery();
            }
        }

        public List<Region> GetRegions()
        {
            var regions = new List<Region>();

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var query = @"select id, parent_id, svg, description from regions order by id";
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    regions.Add(new Region
                    {
                        Id = (int)rdr[0],
                        ParentId = (int)rdr[1],
                        Svg = rdr[2].ToString(),
                        Description = rdr[3].ToString()
                    });
                }
            }

            return regions;
        }
        public List<Zone> GetZones()
        {
            var zones = new List<Zone>();

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var query = @"select 
                                                zones.id, 
                                                zones.receiver_id, 
                                                zones.fiber_x1, 
                                                zones.fiber_x2, 
                                                regions.id, 
                                                x1, 
                                                y1, 
                                                x2, 
                                                y2, 
                                                zones.status,
                                                zones.description,
                                                zones.recognize
                                            from zones
                                            join regions on zones.region_id = regions.id";
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Guid receiverId;
                    if (!Guid.TryParse(rdr[1].ToString(), out receiverId)) continue;
                    var zone = new Zone((Status)(int)rdr[9])
                    {
                        Id = (int)rdr[0],
                        ReceiverId = receiverId,
                        FiberX1 = (int)rdr[2],
                        FiberX2 = (int)rdr[3],
                        RegionId = (int)rdr[4],
                        X1 = (decimal)rdr[5],
                        Y1 = (decimal)rdr[6],
                        X2 = (decimal)rdr[7],
                        Y2 = (decimal)rdr[8],
                        Description = rdr[10].ToString(),
                        Recognize = (bool)rdr[11]
                    };
                    zone.InitDampers(GetDampers(zone.Id));
                    zones.Add(zone);
                }
            }

            return zones;
        }

        public Zone GetZone(int zoneId)
        {
            return GetZones().Find(zone => zone.Id == zoneId);
        }

        public bool UpdateZone(Zone zone)
        {
            string query;
            bool insert = zone.Id == 0;
            if (insert)
            {
                query =
                    @"Insert into zones (receiver_id, fiber_x1, fiber_x2, region_id, x1, y1, x2, y2, description, recognize)
                            values (:receiver_id, :fiber_x1, :fiber_x2, :region_id, :x1, :y1, :x2, :y2, :description, :recognize) returning id";
            }
            else
            {
                query = @"update zones set receiver_id=:receiver_id, fiber_x1 = :fiber_x1, fiber_x2 = :fiber_x2, region_id = :region_id, x1 = :x1, y1 = :y1, 
                            x2 = :x2, y2 = :y2, description = :description, recognize = :recognize Where id = :id";
            }

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.Add("receiver_id", NpgsqlDbType.Uuid).Value = zone.ReceiverId;
                    cmd.Parameters.Add("fiber_x1", NpgsqlDbType.Integer).Value = (int)zone.FiberX1;
                    cmd.Parameters.Add("fiber_x2", NpgsqlDbType.Integer).Value = (int)zone.FiberX2;
                    cmd.Parameters.Add("region_id", NpgsqlDbType.Integer).Value = zone.RegionId;
                    cmd.Parameters.Add("x1", NpgsqlDbType.Numeric).Value = zone.X1;
                    cmd.Parameters.Add("y1", NpgsqlDbType.Numeric).Value = zone.Y1;
                    cmd.Parameters.Add("x2", NpgsqlDbType.Numeric).Value = zone.X2;
                    cmd.Parameters.Add("y2", NpgsqlDbType.Numeric).Value = zone.Y2;
                    cmd.Parameters.Add("description", NpgsqlDbType.Text).Value = zone.Description;
                    cmd.Parameters.Add("recognize", NpgsqlDbType.Boolean).Value = zone.Recognize;
                    if (!insert)
                    {
                        cmd.Parameters.Add("id", NpgsqlDbType.Integer).Value = zone.Id;
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        using (var rdr = cmd.ExecuteReader())
                            if (rdr.Read())
                                zone.Id = (int)rdr[0];
                            else
                                throw new Exception("Error UpdateZone");
                    }
                }
                using (var cmdDamper = new NpgsqlCommand($@"delete from dampers where zoneid = {zone.Id}", conn))
                    cmdDamper.ExecuteNonQuery();
                using (
                    var cmdDamper =
                        new NpgsqlCommand(
                            $@"insert into dampers values (:zoneid, :x1, :x2, :timeout, :warning_count, :alert_count, :range, :split, :enable, :overflow)",
                            conn))
                {
                    foreach (var damper in zone.Dampers)
                    {
                        cmdDamper.Parameters.Add("zoneid", NpgsqlDbType.Integer).Value = zone.Id;
                        cmdDamper.Parameters.Add("x1", NpgsqlDbType.Integer).Value = damper.X1;
                        cmdDamper.Parameters.Add("x2", NpgsqlDbType.Integer).Value = damper.X2;
                        cmdDamper.Parameters.Add("timeout", NpgsqlDbType.Integer).Value = damper.Timeout;
                        cmdDamper.Parameters.Add("warning_count", NpgsqlDbType.Integer).Value =
                            damper.WarningStatusCount;
                        cmdDamper.Parameters.Add("alert_count", NpgsqlDbType.Integer).Value = damper.AlarmStatusCount;
                        cmdDamper.Parameters.Add("range", NpgsqlDbType.Integer).Value = damper.Range;
                        cmdDamper.Parameters.Add("split", NpgsqlDbType.Boolean).Value = damper.Split;
                        cmdDamper.Parameters.Add("enable", NpgsqlDbType.Boolean).Value = damper.Enable;
                        cmdDamper.Parameters.Add("overflow", NpgsqlDbType.Boolean).Value = damper.Overflow;
                        cmdDamper.ExecuteNonQuery();
                    }
                }
            }
            return true;
        }

        public bool DeleteZone(Zone zone)
        {
            throw new NotImplementedException();
        }

        public List<ZoneAction> GetActionsHistory(string from, string to, int actionType = 0, int? regionId = null)
        {
            var actions = new List<ZoneAction>();
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                string whereActionType = actionType == 0 ? "" : $"and ac.status = {actionType}";
                string whereRegionid = !regionId.HasValue ? "" : $@"
                    and z.region_id in (WITH RECURSIVE r AS (SELECT * FROM regions WHERE parent_id = {regionId.Value} or id = {regionId.Value}
                    UNION SELECT regions.* FROM regions JOIN r ON regions.parent_id = r.id )
                    select r.id from r)";
                string query = $@"
                    select ac.zone_id, ac.x, ac.datetime::text, ac.status, ac.confirm, ac.recognizeType, ac.id 
                    from actions as ac
                    inner join zones as z on ac.zone_id = z.id
                    where ac.datetime between :from and :to {whereActionType} {whereRegionid} order by datetime desc
                    ";

                var cmd = new NpgsqlCommand(query, conn);
                DateTime dtFrom;
                if (!DateTime.TryParse(from, out dtFrom)) return actions;
                DateTime dtTo;
                if (!DateTime.TryParse(to, out dtTo)) return actions;
                cmd.Parameters.AddWithValue("from", NpgsqlDbType.Timestamp, dtFrom.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.Parameters.AddWithValue("to", NpgsqlDbType.Timestamp, dtTo.ToString("yyyy-MM-dd HH:mm:ss"));

                //cmd.Parameters.AddWithValue("from", NpgsqlDbType.Timestamp, DateTime.ParseExact(from, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss"));
                //cmd.Parameters.AddWithValue("to", NpgsqlDbType.Timestamp, DateTime.ParseExact(to, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss"));
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var reflex = new ZoneAction
                    {
                        TimeTicket = rdr[2].ToString(),
                        ZoneId = (int)rdr[0],
                        X = (int)rdr[1],
                        Status = (Status)(int)rdr[3],
                        Confirmed = (bool)rdr[4],
                        RecognizeType = (long)rdr[5],
                        Id = Guid.Parse(rdr[6].ToString())
                    };
                    actions.Add(reflex);
                }
            }

            return actions;
        }

        public ZoneAction GetAction(Guid actionId)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                ZoneAction result = null;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                const string query = "select zone_id, x, datetime::text, status, confirm, id, recognizeType from actions where id = :actionId limit 1";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.AddWithValue("actionId", NpgsqlDbType.Uuid, actionId);
                var rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    result = new ZoneAction
                    {
                        TimeTicket = rdr[2].ToString(),
                        ZoneId = (int)rdr[0],
                        X = (int)rdr[1],
                        Status = (Status)(int)rdr[3],
                        Confirmed = (bool)rdr[4],
                        Id = Guid.Parse(rdr[5].ToString()),
                        RecognizeType = (long)rdr[6]
                    };
                }
                return result;
            }
        }

        public Dictionary<int, Status> GetZoneStatus(int zoneId)
        {
            var statuses = new Dictionary<int, Status>();

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var cmd = new NpgsqlCommand { Connection = conn };

                string query;
                if (zoneId <= 0)
                {
                    query = @"select id, status from zones order by id";
                }
                else
                {
                    query = @"select id, status from zones where id = :id";
                    cmd.Parameters.AddWithValue("id", NpgsqlDbType.Integer, zoneId);
                }
                cmd.CommandText = query;

                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var regionId = (int)rdr[0];
                    var status = (int)rdr[1];
                    if (statuses.ContainsKey(regionId))
                    {
                        statuses[regionId] = (Status)status;
                    }
                    else
                    {
                        statuses.Add(regionId, (Status)status);
                    }
                }
            }

            return statuses;
        }

        public void UpdateZoneStatus(int zoneId, Status status)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                const string query = @"update zones set status = :status where id = :id";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.AddWithValue("status", NpgsqlDbType.Integer, status);
                cmd.Parameters.AddWithValue("id", NpgsqlDbType.Integer, zoneId);
                cmd.ExecuteNonQuery();
            }
        }


        public static List<Damper> GetDampers(int zoneId)
        {
            var result = new List<Damper>();

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                const string query = "select x1, x2, timeout, warning_count, alert_count, range, split, enable, overflow from dampers where zoneId = :zoneId";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.AddWithValue("zoneId", NpgsqlDbType.Integer, zoneId);
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    result.Add(new Damper
                    {
                        X1 = (int)rdr[0],
                        X2 = (int)rdr[1],
                        Timeout = (int)rdr[2],
                        WarningStatusCount = (int)rdr[3],
                        AlarmStatusCount = (int)rdr[4],
                        Range = (int)rdr[5],
                        Split = (bool)rdr[6],
                        Enable = (bool)rdr[7],
                        Overflow = (bool)rdr[8]
                    });
                }
            }

            return result;
        }


        public bool Login(string login, string pass)
        {
            var result = false;

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var query = string.Format(@"select count(id)::integer from users where name = '" + login + "' and password = '" + pass + "'");
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                if (!rdr.Read()) return false;
                var count = (int)rdr[0];
                if (count > 0)
                    result = true;
            }

            return result;
        }

        public DateTime TimeToLastRequest(string login)
        {
            var result = DateTime.MinValue;

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var query = string.Format(@"select last_request from users where name = '" + login + "'");
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                if (!rdr.Read()) return result;
                if (!DateTime.TryParse(rdr[0].ToString(), out result))
                {
                    result = DateTime.Now.AddDays(-1);
                }

                result = result < DateTime.Now.AddDays(-1) ? DateTime.Now.AddDays(-1) : result;
            }

            return result;
        }

        public User GetUserInfo(User user)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                const string query = @"select description from users where name = :login limit 1";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.Add("login", NpgsqlDbType.Text).Value = user.Login;
                var rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    user.Description = rdr[0].ToString();
                }
            }

            return user;
        }

        public List<Sector> GetSectors()
        {
            var sectors = new List<Sector>();

            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                const string query = @"select id, parent_id, svg, description from sectors";
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    sectors.Add(new Sector
                    {
                        Id = Guid.Parse(rdr[0].ToString()),
                        ParentId = Guid.Parse(rdr[1].ToString()),
                        Svg = rdr[2].ToString(),
                        Description = rdr[3].ToString()
                    });
                }
            }

            return sectors;
        }

        public bool UpdateSector(Sector sector)
        {
            var toUpdate = false;
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();
                    const string query = @"select id from sectors where id = :sectorId limit 1";
                    using (var cmd = new NpgsqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("sectorId", NpgsqlDbType.Uuid, sector.Id);
                        using (var rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read())
                            {
                                Guid workerId;
                                toUpdate = Guid.TryParse(rdr[0].ToString(), out workerId);
                            }
                        }
                    }
                    if (!toUpdate)
                    {
                        const string queryInsert = "insert into sectors (id, parent_id, name, svg, description) values (:sectorId, :parentId, :name, :svg, :description)";
                        var cmdInsert = new NpgsqlCommand(queryInsert, conn);
                        cmdInsert.Parameters.AddWithValue("sectorId", NpgsqlDbType.Uuid, sector.Id);
                        cmdInsert.Parameters.AddWithValue("parentId", NpgsqlDbType.Uuid, sector.ParentId);
                        cmdInsert.Parameters.AddWithValue("name", NpgsqlDbType.Text, sector.Name);
                        cmdInsert.Parameters.AddWithValue("svg", NpgsqlDbType.Text, sector.Svg);
                        cmdInsert.Parameters.AddWithValue("description", NpgsqlDbType.Text, sector.Description);
                        cmdInsert.ExecuteNonQuery();
                    }
                    else
                    {
                        const string queryUpdate = "update sectors set parent_id = :parentId, name = :name, svg = :svg, description = :description where id = :sectorId;";
                        var cmdUpdate = new NpgsqlCommand(queryUpdate, conn);
                        cmdUpdate.Parameters.AddWithValue("sectorId", NpgsqlDbType.Uuid, sector.Id);
                        cmdUpdate.Parameters.AddWithValue("parentId", NpgsqlDbType.Uuid, sector.ParentId);
                        cmdUpdate.Parameters.AddWithValue("name", NpgsqlDbType.Text, sector.Name);
                        cmdUpdate.Parameters.AddWithValue("svg", NpgsqlDbType.Text, sector.Svg);
                        cmdUpdate.Parameters.AddWithValue("description", NpgsqlDbType.Text, sector.Description);
                        cmdUpdate.ExecuteNonQuery();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
        }

        public bool DeleteSector(Sector sector)
        {
            throw new NotImplementedException();
        }

        public List<Barrier> GetBarriers()
        {
            var barriers = new List<Barrier>();
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                const string query = @"select id, sector_id, name, description, x1, x2, y1, y2, status from barriers";
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var id = Guid.Parse(rdr[0].ToString());
                    barriers.Add(new Barrier(id, (Status)(int)rdr[8])
                    {
                        SectorId = Guid.Parse(rdr[1].ToString()),
                        Name = rdr[2].ToString(),
                        Description = rdr[3].ToString(),
                        Location = new Location()
                        {
                            X1 = (decimal)rdr[4],
                            X2 = (decimal)rdr[5],
                            Y1 = (decimal)rdr[6],
                            Y2 = (decimal)rdr[7]
                        }
                    });
                }
            }
            return barriers;
        }

        public bool UpdateBarrier(Barrier barrier)
        {
            var toUpdate = false;
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();
                    const string query = @"select id from barriers where id = :barrierId limit 1";
                    using (var cmd = new NpgsqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("barrierId", NpgsqlDbType.Uuid, barrier.Id);
                        using (var rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read())
                            {
                                Guid workerId;
                                toUpdate = Guid.TryParse(rdr[0].ToString(), out workerId);
                            }
                        }
                    }
                    if (!toUpdate)
                    {
                        const string queryInsert = "insert into barriers (id, sector_id, name, description, x1, x2, y1, y2, status) values (:id, :sectorId, :name, :description, :x1, :x2, :y1, :y2, :status)";
                        var cmdInsert = new NpgsqlCommand(queryInsert, conn);
                        cmdInsert.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, barrier.Id);
                        cmdInsert.Parameters.AddWithValue("sectorId", NpgsqlDbType.Uuid, barrier.SectorId);
                        cmdInsert.Parameters.AddWithValue("name", NpgsqlDbType.Text, barrier.Name);
                        cmdInsert.Parameters.AddWithValue("description", NpgsqlDbType.Text, barrier.Description);
                        cmdInsert.Parameters.AddWithValue("x1", NpgsqlDbType.Numeric, barrier.Location.X1);
                        cmdInsert.Parameters.AddWithValue("x2", NpgsqlDbType.Numeric, barrier.Location.X2);
                        cmdInsert.Parameters.AddWithValue("y1", NpgsqlDbType.Numeric, barrier.Location.Y1);
                        cmdInsert.Parameters.AddWithValue("y2", NpgsqlDbType.Numeric, barrier.Location.Y2);
                        cmdInsert.Parameters.AddWithValue("status", NpgsqlDbType.Numeric, barrier.Status);
                        cmdInsert.ExecuteNonQuery();
                    }
                    else
                    {
                        const string queryUpdate = "update sectors set sector_id = :sectorId, name = :name, description = :description, x1 = :x1, x2 = :x2, y1 = :y1, y2 = :y2, status = :status where id = :id;";
                        var cmdUpdate = new NpgsqlCommand(queryUpdate, conn);
                        cmdUpdate.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, barrier.Id);
                        cmdUpdate.Parameters.AddWithValue("sectorId", NpgsqlDbType.Uuid, barrier.SectorId);
                        cmdUpdate.Parameters.AddWithValue("name", NpgsqlDbType.Text, barrier.Name);
                        cmdUpdate.Parameters.AddWithValue("description", NpgsqlDbType.Text, barrier.Description);
                        cmdUpdate.Parameters.AddWithValue("x1", NpgsqlDbType.Numeric, barrier.Location.X1);
                        cmdUpdate.Parameters.AddWithValue("x2", NpgsqlDbType.Numeric, barrier.Location.X2);
                        cmdUpdate.Parameters.AddWithValue("y1", NpgsqlDbType.Numeric, barrier.Location.Y1);
                        cmdUpdate.Parameters.AddWithValue("y2", NpgsqlDbType.Numeric, barrier.Location.Y2);
                        cmdUpdate.Parameters.AddWithValue("status", NpgsqlDbType.Numeric, barrier.Status);
                        cmdUpdate.ExecuteNonQuery();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
        }

        public bool DeleteBarrier(Barrier barrier)
        {
            throw new NotImplementedException();
        }

        public bool UpdateBarrierStatus(Barrier barrier)
        {
            return true;
        }

        public void Logout(string login)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var query = $@"update users set last_request = now()::timestamp where name = '{login}'";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateLastRequest(string login)
        {
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                var query = $@"update users set last_request = now()::timestamp where name = '{login}'";
                var cmd = new NpgsqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
        }

        public List<Range> GetRanges()
        {
            var result = new List<Range>();
            using (var sqlConnection = new NpgsqlConnection(Config.ConnectionString))
            {
                if (sqlConnection.State != ConnectionState.Open)
                    sqlConnection.Open();

                var query = $@"select ch1.id as range_id, ch1.receiver_id as range_receiver_id, ch1.x1 as range_x1, ch1.x2 as range_x2,
ch2.id as check_id, ch2.receiver_id as check_receiver_id, ch2.x1 as check_x1, ch2.x2 as check_x2, ch2.reverse as check_reverse from checkers as ch1 
left join checkers as ch2 on ch2.check_id = ch1.id and ch2.id <> ch2.check_id 
where ch1.id = ch1.check_id";
                using (var sqlCommand = new NpgsqlCommand(query, sqlConnection))
                {
                    using (var sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            Checker checker = null;
                            Guid checkerId;
                            Guid rangeId;
                            Guid receiverId;
                            if (!Guid.TryParse(sqlDataReader["range_id"].ToString(), out rangeId))
                                throw new Exception("Error: try parse range id");
                            if (!Guid.TryParse(sqlDataReader["range_receiver_id"].ToString(), out receiverId))
                                throw new Exception("Error: try parse range receiver_id");
                            if (Guid.TryParse(sqlDataReader["check_id"].ToString(), out checkerId))
                            {
                                Guid checkReceiverId;
                                if (!Guid.TryParse(sqlDataReader["check_receiver_id"].ToString(), out checkReceiverId))
                                    throw new Exception("Error: try parse check receiver_id");
                                checker = new Checker()
                                {
                                    Id = checkerId,
                                    ReceiverId = checkReceiverId,
                                    CheckedId = rangeId,
                                    X1 = (int)sqlDataReader["check_x1"],
                                    X2 = (int)sqlDataReader["check_x2"],
                                    Reverse = (bool)sqlDataReader["check_reverse"]
                                };
                            }

                            result.Add(new Range(checker)
                            {
                                Id = rangeId,
                                ReceiverId = receiverId,
                                X1 = (int)sqlDataReader["range_x1"],
                                X2 = (int)sqlDataReader["range_x2"],
                            });
                        }
                    }
                }

            }
            return result;
        }

        public Range GetRange(Guid id)
        {
            var ranges = GetRanges();
            return ranges.FirstOrDefault(r => r.Id == id);
        }

        public Range SetRange(Range range)
        {
            Range result = null;
            using (var sqlConnection = new NpgsqlConnection(Config.ConnectionString))
            {
                if (sqlConnection.State != ConnectionState.Open)
                    sqlConnection.Open();
                using (var sqlUpdateCommand = new NpgsqlCommand($@"update checkers set 
                    receiver_id = '{range.ReceiverId.ToString()}', 
                    check_id = '{range.Id.ToString()}', 
                    x1= {range.X1}, x2 = {range.X2}, reverse = true 
                where id = '{range.Id}'", sqlConnection))
                {
                    if (sqlUpdateCommand.ExecuteNonQuery() == 0)
                    {
                        using (var sqlInsertCommand = new NpgsqlCommand($@"insert into checkers (id, receiver_id, check_id, x1, x2, reverse)
                        values('{range.Id.ToString()}', '{range.ReceiverId.ToString()}', '{range.Id.ToString()}',
                        {range.X1}, {range.X2}, true)", sqlConnection))
                        {
                            sqlInsertCommand.ExecuteNonQuery();
                        }
                    }
                }

                if (range.Checker == null)
                {
                    using (var sqlDeleteCommand = new NpgsqlCommand($@"delete from checkers where check_id = '{range.Id}' and id <> '{range.Id}'", sqlConnection))
                    {
                        sqlDeleteCommand.ExecuteNonQuery();
                    }
                }
                else
                {
                    using (var sqlUpdateCommand = new NpgsqlCommand($@"update checkers set 
                        receiver_id = '{range.Checker.ReceiverId.ToString()}', 
                        check_id = '{range.Id.ToString()}', 
                        x1= {range.Checker.X1}, x2 = {range.Checker.X2}, reverse = {range.Checker.Reverse} 
                    where id = '{range.Checker.Id}'", sqlConnection))
                    {
                        if (sqlUpdateCommand.ExecuteNonQuery() == 0)
                        {
                            using (var sqlInsertCommand = new NpgsqlCommand($@"insert into checkers (id, receiver_id, check_id, x1, x2, reverse)
                        values('{range.Checker.Id.ToString()}', '{range.Checker.ReceiverId.ToString()}', '{range.Id.ToString()}',
                        {range.Checker.X1}, {range.Checker.X2}, {range.Checker.Reverse})", sqlConnection))
                            {
                                sqlInsertCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
                result = GetRange(range.Id);
            }
            return result;
        }
    }
}
