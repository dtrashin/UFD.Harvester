﻿using System.Configuration;

namespace UFD.Harvester.Core.DAL.PSQL
{
    public class PgsqlDataFacadeConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("ConnectionString", IsRequired = true)]
        public string ConnectionString
        {
            get { return (string)this["ConnectionString"]; }
            set { this["ConnectionString"] = value; }
        }

        public static PgsqlDataFacadeConfiguration GetConfig()
        {
            Configuration execonfig = null;

            try
            {
                execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            catch
            {
                // ignored
            }

            if (execonfig == null)
                return new PgsqlDataFacadeConfiguration
                {
                    ConnectionString = "Server = 127.0.0.1; User Id = postgres; Password = postgres; DataBase = debug;"
                };

            return (PgsqlDataFacadeConfiguration)execonfig.Sections["Harvester.Data"];
        }
    }
}
