﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UFD.Harvester.Core.Helpers
{
    internal static class IntegrationHelper
    {
        private static List<Guid> _integrationIds = new List<Guid>
        {
            Guid.Parse("E461B5FF-87D9-4095-BD29-2706D8818007"),  // Sms service.
            Guid.Parse("B15F35E7-434B-423D-A894-D9BB60C6F55D")   // Tobol service.
        };

        public static bool CheckId(Guid integrationId)
        {
            return _integrationIds.Where(id => id == integrationId).Any();
        }

    }
}
