﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UFD.Harvester.Core.Exceptions;
using UFD.Harvester.Core.BO;

namespace UFD.Harvester.Core.Helpers
{
    public static class RequestHelper
    {
        public static JObject GetJObject(this Stream stream)
        {
            string strBody;
            using (var rdr = new StreamReader(stream))
                strBody = rdr.ReadToEnd();
            var jBody = JObject.Parse(strBody);
            if (jBody == null)
                throw new InvalidRequestParametersException("Error: invalid request parameter.");
            return jBody;
        }

        public static Guid GetUserId(this JObject jObject)
        {
            var strUserId = jObject["UserId"]?.ToString() ?? "";
            Guid userId;
            Guid.TryParse(strUserId, out userId);
            return userId;
        }

        public static Guid GetRangeId(this JObject jObject)
        {
            if (jObject["RangeId"] == null) return default(Guid);
            var strGangeId = jObject["RangeId"].ToString();
            Guid rangeId;
            Guid.TryParse(strGangeId, out rangeId);
            return rangeId;
        }


        public static int GetZoneId(this JObject jObject)
        {
            var strZoneId = jObject["ZoneId"]?.ToString() ?? "0";
            int zoneId;
            if (!int.TryParse(strZoneId, out zoneId))
                throw new Exception("Invalid parameters ZoneId.");

            return zoneId;
        }
        public static object GetEntity(this JObject jObject, Type type)
        {
            var strEntity = jObject["Data"];
            if (strEntity == null)
                throw new Exception("Not found parameters Data.");

            var zone = JsonConvert.DeserializeObject(strEntity.ToString(), type);
            return zone;
        }

        public static int GetZoneState(this JObject jObject)
        {
            var strZoneStatus = jObject["Status"]?.ToString() ?? "";
            int zoneState;
            if (!int.TryParse(strZoneStatus, out zoneState))
                throw new Exception("Invalid parameters Status.");
            return zoneState;
        }

        public static string GetSubject(this JObject jObject)
        {
            var subject = jObject["Subject"]?.ToString() ?? "";
            if (string.IsNullOrWhiteSpace(subject))
                throw new Exception("Invalid parameters Subject.");
            return subject;

        }
    }
}
