﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Core.Attributes
{
    public class NotNegativeNumberAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is int)
                return (int) value >= 0;
            if (value is double)
                return (double) value >= 0;
            throw new Exception("Property type is not number.");
        }
    }
}
