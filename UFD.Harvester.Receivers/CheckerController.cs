﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Receivers.DAL;
using UFD.Harvester.Receivers.Events;

namespace UFD.Harvester.Receivers
{


    public class CheckerController
    {
        private readonly IReceiverDataFacade _receiverDataFacade = ReceiverDataFacadeFactory.GetFacade("PGSQL");

        //private readonly List<Checker> _checkers;
        private readonly List<Line> _lines = new List<Line>();
        private readonly Stopwatch _sw = new Stopwatch();

        public event EventHandler<DatagramReceivedEventArgs> DatagramChecked;
        public event EventHandler<LineBrokenEventArgs> LineBroken;

        public CheckerController()
        {
            //_checkers = _receiverDataFacade.GetCheckers();
            var ranges = _receiverDataFacade.GetRanges();

            var lineIds = ranges.Select(r => r.ReceiverId).Distinct();
            foreach (var line in lineIds.Select(lineId => new Line(ranges.Where(r => r.ReceiverId == lineId).ToList()) { Id = lineId }))
            {
                _lines.Add(line);
            }
        }

        public void Start()
        {
            try
            {
                var config = ReceiversConfiguration.GetConfig();
                for (var i = 0; i < config.Receivers.Count; i++)
                {
                    var receiver = ReceiverFactory.CreateReceiver(config.Receivers[i]);
                    if (receiver == null) continue;
                    receiver.DatagramReceived += DatagramCheck;
                    receiver.StatusChanged += ReceiverStatusChange;
                    receiver.Broken += Broken;
                    new Thread(() => receiver.Start()).Start();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Receivers: " + ex.Message);
            }
        }

        private void Broken(object s, BrokenEventArgs e)
        {
            _lines.Select(l => l.Ranges)
                .SelectMany(r => r)
                .ToList()
                .Where(r => r.Checker != null && r.Checker.ReceiverId == e.Id && r.Checker.X2 >= e.X)
                .Select(r => r.Checker)
                .ToList()
                .ForEach(ch => ch.BreakChecker(e.X));

            var line = _lines.FirstOrDefault(l => l.Id == e.Id);
            if (line == null) return;
            var stopBroken = line.BreakLine(e.X);
            if (e.X != stopBroken)
                LineBroken?.Invoke(this, new LineBrokenEventArgs(e.Id, e.X, stopBroken));
        }

        private void ReceiverStatusChange(object s, StatusChangedEventArgs e)
        {

        }

        private void DatagramCheck(object s, DatagramReceivedEventArgs e)
        {
            _sw.Reset();
            _sw.Start();

            //var checkers = _checkers.Where(ch => ch.ReceiverId == e.Datagram.ReceiverId).ToList();
            var checkers =
                _lines.Select(l => l.Ranges)
                    .SelectMany(r => r)
                    .ToList()
                    .Where(r => r.Checker != null && r.Checker.ReceiverId == e.Datagram.ReceiverId)
                    .Select(r => r.Checker)
                    .ToList();
            if (checkers.Any())
            {
                checkers.ForEach(ch => ch.UpdateChecker(e.Datagram));
            }

            var line = _lines.FirstOrDefault(l => l.Id == e.Datagram.ReceiverId);
            if (line != null)
            {
                var datagram = line.CheckDatagram(e.Datagram);
                //if (datagram.Impacts.Count > 0)
                //    Debug.WriteLine("Есть сработки.");
                DatagramChecked?.Invoke(this, new DatagramReceivedEventArgs(e.Datagram.ReceiverId, datagram));
            }

            _sw.Stop();
            //Debug.WriteLine("Checker time (ms): {0}", _sw.ElapsedMilliseconds);
        }
    }
}
