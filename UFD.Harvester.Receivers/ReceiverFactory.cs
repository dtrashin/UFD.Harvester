﻿using UFD.Harvester.Receivers.Receivers;

namespace UFD.Harvester.Receivers
{
    public static class ReceiverFactory
    {
        public static IReceiver CreateReceiver(ReceiverConfiguration receiverConfiguration)
        {
            if (receiverConfiguration == null) return null;
            IReceiver receiver;
            switch (receiverConfiguration.Type)
            {
                case "CR1":
                    receiver = new ReceiverCR1(receiverConfiguration);
                    break;
                case "CR2":
                    receiver = new ReceiverCR2(receiverConfiguration);
                    break;
                case "Generator":
                    receiver = new ReceiverGenerator(receiverConfiguration);
                    break;
                default:
                    receiver = null;
                    break;
            }
            return receiver;
        }
    }
}
