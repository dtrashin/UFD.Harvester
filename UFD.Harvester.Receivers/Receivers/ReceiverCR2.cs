﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Receivers.Events;

namespace UFD.Harvester.Receivers.Receivers
{
    public class ReceiverCR2 : IReceiver
    {
        private TcpClient _client;
        private readonly ReceiverConfiguration _config;
        private readonly DatagramController _datagramController;

        public event EventHandler<DatagramReceivedEventArgs> DatagramReceived;
        public event EventHandler<StatusChangedEventArgs> StatusChanged;
        public event EventHandler<BrokenEventArgs> Broken;

        public ReceiverCR2(ReceiverConfiguration config)
        {
            _config = config;
            _datagramController = new DatagramController(_config.Id, 2);
        }

        public int ImpulseDuration { get; private set; }

        public void Start()
        {
            Reconnect();
        }

        private void Reconnect()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Receiver try connect to {0}:{1}", _config.Host, _config.Port);
                    _client = TryConnect();
                    Console.WriteLine("Receiver connected to {0}:{1}", _config.Host, _config.Port);
                    StatusChanged?.Invoke(this, new StatusChangedEventArgs(_config.Id, 0));
                    ImpulseDuration = _config.ImpulseDuration;
                    ReceiveData();
                    break;
                }
                catch (Exception ex)
                {
                    StatusChanged?.Invoke(this, new StatusChangedEventArgs(_config.Id, 1));
                    Console.WriteLine(ex.Message);
                    Thread.Sleep(5000);
                    //Reconnect();
                }
            }
        }

        private TcpClient TryConnect()
        {
            var client = new TcpClient();
            var state = new State
            {
                Client = client,
                Success = true
            };
            IAsyncResult ar = client.BeginConnect(_config.Host, _config.Port, EndConnect, state);
            state.Success = ar.AsyncWaitHandle.WaitOne(5, false);

            if (!state.Success || !client.Connected)
                throw new Exception("Failed to connect.");

            return client;
        }

        private void EndConnect(IAsyncResult ar)
        {
            var state = (State)ar.AsyncState;
            TcpClient client = state.Client;

            try
            {
                client.EndConnect(ar);
            }
            catch (Exception)
            {
                // ignored
            }

            if (client.Connected && state.Success)
                return;

            client.Close();
        }

        private void ReceiveData()
        {
            try
            {
                using (NetworkStream stream = _client.GetStream())
                {
                    stream.ReadTimeout = 3000;

                    while (true)
                    {
                        byte[] buff = new byte[2];
                        stream.Read(buff, 0, 2);
                        Array.Reverse(buff);
                        ushort packSize = BitConverter.ToUInt16(buff, 0);
                        Debug.WriteLine("Packet size: " + packSize);

                        if (packSize == 0)
                        {
                            throw new Exception("Failed to read.");
                        }

                        byte[] data = new byte[1];
                        byte[] buffData;
                        using (MemoryStream ms = new MemoryStream())
                        {
                            int numBytesRead = 0;
                            while (true)
                            {
                                numBytesRead += stream.Read(data, 0, data.Length);
                                ms.Write(data, 0, data.Length);
                                if (numBytesRead == packSize)
                                    break;
                            }
                            buffData = ms.ToArray();
                        }
                        try
                        {
                            Datagram datagram = _datagramController.GetDatagram(buffData);
                            if (datagram != null)
                            {
                                datagram.Raw.AddRange(buff);
                                datagram.Raw.AddRange(buffData);
                                if (datagram.DatagramType == 1)
                                    Broken?.Invoke(this, new BrokenEventArgs(_config.Id, datagram.Points.First()));
                                else 
                                    DatagramReceived?.Invoke(this, new DatagramReceivedEventArgs(_config.Id, datagram));
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("DatagramContorller.GetDatagram: " + ex.Message + Environment.NewLine + ex.StackTrace);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Reconnect();
            }
        }

        private class State
        {
            public TcpClient Client { get; set; }
            public bool Success { get; set; }
        }
    }
}
