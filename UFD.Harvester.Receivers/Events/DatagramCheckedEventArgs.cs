﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Receivers.Events
{
    public class DatagramCheckedEventArgs : EventArgs
    {
        public readonly Datagram Datagram;

        public DatagramCheckedEventArgs(Datagram datagram)
        {
            Datagram = datagram;
        }
    }
}
