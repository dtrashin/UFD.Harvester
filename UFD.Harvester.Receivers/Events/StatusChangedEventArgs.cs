﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Receivers.Events
{
    public class StatusChangedEventArgs : EventArgs
    {
        public Guid Id;
        public readonly int Status;

        public StatusChangedEventArgs(Guid id, int status)
        {
            Id = id;
            Status = status;
        }
    }
}
