﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Receivers.Events
{
    public class BrokenEventArgs : EventArgs
    {
        public Guid Id;
        public readonly int X;

        public BrokenEventArgs(Guid id, int x)
        {
            Id = id;
            X = x;
        }
    }
}
