﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Receivers.Events
{
    public class LineBrokenEventArgs : EventArgs
    {
        public readonly Guid Id;
        public readonly int X1;
        public readonly int X2;

        public LineBrokenEventArgs(Guid id, int x1, int x2)
        {
            Id = id;
            X1 = x1;
            X2 = x2;
        }
    }
}
