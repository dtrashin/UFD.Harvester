﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Receivers.Events
{
    public class DatagramReceivedEventArgs : EventArgs
    {
        public readonly Datagram Datagram;

        public DatagramReceivedEventArgs(Guid id, Datagram datagram)
        {
            Datagram = datagram;
        }
    }
}
