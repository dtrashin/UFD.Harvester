﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Receivers
{
    public class Line
    {
        private bool _lineBroken;
        private int _lineBrokenCount;

        public Guid Id { get; set; }

        public List<Range> Ranges { get; }

        public Line(List<Range> ranges)
        {
            Ranges = ranges;
        }

        public Datagram CheckDatagram(Datagram datagram)
        {
            var imps = new List<Impact>();
            foreach (var range in Ranges)
            {
                var impacts = datagram.Impacts.Where(i => (i.Start >= range.X1 && i.Start <= range.X2) || (i.Stop >= range.X1 && i.Stop <= range.X2)).ToList();
                if (!impacts.Any()) continue;
                imps.AddRange(range.Checker == null ? impacts : range.CheckImpacts(impacts));
            }
            datagram.Impacts.Clear();
            datagram.AddImpact(imps);
            if (!_lineBroken) return datagram;
            if (--_lineBrokenCount <= 0)
                _lineBroken = false;
            return datagram;
        }

        public int BreakLine(int x)
        {
            if (_lineBroken) return x;
            _lineBroken = true;
            _lineBrokenCount = 10;
            return ++x;
        }
    }
}
