﻿using System;
using System.Security.Cryptography.X509Certificates;
using UFD.Harvester.Datagrams;
using UFD.Harvester.Receivers.Events;

namespace UFD.Harvester.Receivers
{
    public interface IReceiver
    {
        event EventHandler<DatagramReceivedEventArgs> DatagramReceived;
        event EventHandler<StatusChangedEventArgs> StatusChanged;
        event EventHandler<BrokenEventArgs> Broken;

        int ImpulseDuration { get; }

        void Start();
    }
}
