﻿using UFD.Harvester.Receivers.DAL.PGSQL;

namespace UFD.Harvester.Receivers.DAL
{
    public class ReceiverDataFacadeFactory
    {
        public static IReceiverDataFacade GetFacade(string name)
        {
            IReceiverDataFacade facade;
            switch (name)
            {
                case "PGSQL":
                    facade = new PgsqlReceiverDataFacade();
                    break;
                default:
                    facade = null;
                    break;
            }
            return facade;
        }
    }
}
