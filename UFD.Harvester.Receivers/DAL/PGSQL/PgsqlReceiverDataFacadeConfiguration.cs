﻿using System.Configuration;

namespace UFD.Harvester.Receivers.DAL.PGSQL
{
    public class PgsqlReceiverDataFacadeConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("ConnectionString", IsRequired = true)]
        public string ConnectionString
        {
            get { return (string)this["ConnectionString"]; }
            set { this["ConnectionString"] = value; }
        }

        public static PgsqlReceiverDataFacadeConfiguration GetConfig()
        {
            Configuration execonfig = null;

            try
            {
                execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            catch
            {
                // ignored
            }

            if (execonfig == null)
                return new PgsqlReceiverDataFacadeConfiguration
                {
                    ConnectionString = "Server = 127.0.0.1; User Id = postgres; Password = postgres; DataBase = debug;"
                };

            return (PgsqlReceiverDataFacadeConfiguration)execonfig.Sections["Harvester.ReceiversData"];
        }
    }

}
