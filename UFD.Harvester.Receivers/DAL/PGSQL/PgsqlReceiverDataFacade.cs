﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;

namespace UFD.Harvester.Receivers.DAL.PGSQL
{
    class PgsqlReceiverDataFacade : IReceiverDataFacade
    {
        private static readonly PgsqlReceiverDataFacadeConfiguration Config = PgsqlReceiverDataFacadeConfiguration.GetConfig();

        public List<Range> GetRanges()
        {
            var ranges = new List<Range>();
            var checkers = GetCheckers();
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != System.Data.ConnectionState.Open)
                    conn.Open();
                const string query = @"select id, receiver_id, x1, x2, reverse from checkers where id = check_id";
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Guid id;
                    if (!Guid.TryParse(rdr[0].ToString(), out id)) continue;
                    Guid receiverId;
                    if (!Guid.TryParse(rdr[1].ToString(), out receiverId)) continue;
                    var range = new Range(checkers.FirstOrDefault(ch => ch.CheckedId == id))
                    {
                        Id = id,
                        ReceiverId = receiverId,
                        X1 = (int) rdr[2],
                        X2 = (int) rdr[3]
                    };
                    ranges.Add(range);
                }
            }
            return ranges;
        }

        public List<Checker> GetCheckers()
        {
            var checkers = new List<Checker>();
            using (var conn = new NpgsqlConnection(Config.ConnectionString))
            {
                if (conn.State != System.Data.ConnectionState.Open)
                    conn.Open();

                const string query = @"select id, receiver_id, check_id, x1, x2, reverse from checkers where id != check_id";
                var cmd = new NpgsqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Guid id;
                    if (!Guid.TryParse(rdr[0].ToString(), out id)) continue;
                    Guid receiverId;
                    if (!Guid.TryParse(rdr[1].ToString(), out receiverId)) continue;
                    Guid checkId;
                    if (!Guid.TryParse(rdr[2].ToString(), out checkId)) continue;
                    var checker = new Checker(id, (int) rdr[3], (int) rdr[4], (bool) rdr[5])
                    {
                        ReceiverId = receiverId,
                        CheckedId = checkId
                    };
                    checkers.Add(checker);
                }
            }
            return checkers;
        }
    }
}
