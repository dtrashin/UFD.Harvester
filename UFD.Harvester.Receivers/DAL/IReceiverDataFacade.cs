﻿using System.Collections.Generic;

namespace UFD.Harvester.Receivers.DAL
{
    public interface IReceiverDataFacade
    {
        List<Range> GetRanges();
        List<Checker> GetCheckers();
    }
}
