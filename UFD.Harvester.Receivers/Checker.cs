﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Receivers
{
    public class Checker
    {
        //private readonly SortedList<int, bool> _fiber = new SortedList<int, bool>();
        private readonly SortedList<int, Impact> _fiberImpact = new SortedList<int, Impact>();
        private int _brokenPosition;
        private int _brokenCount;


        /// <summary>
        /// Id контролера.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id ресивера.
        /// </summary>
        public Guid ReceiverId { get; set; }

        /// <summary>
        /// Id зоны, которую проверяет контролер.
        /// </summary>
        public Guid CheckedId { get; set; }

        /// <summary>
        /// Начало зоны контроля.
        /// </summary>
        public int X1 { get; set; }

        /// <summary>
        /// Конец зоны контроля.
        /// </summary>
        public int X2 { get; set; }

        /// <summary>
        /// Разворот координат.
        /// </summary>
        public bool Reverse { get; set; }

        /// <summary>
        /// Признак обрыва линии.
        /// </summary>
        public bool IsBroken => _brokenCount > 0;


        // .ctor
        public Checker() { }

        public Checker(Guid id, int x1, int x2, bool reverse)
        {
            Id = id;
            X1 = x1;
            X2 = x2;
            Reverse = reverse;
            ResetFiber();
        }

        public SortedList<int, Impact> CheckLine(SortedList<int, Impact> line)
        {
            var result = new SortedList<int, Impact>();
            lock (this)
            {
                if (_brokenPosition > 0)
                    BrokenFiber(_brokenPosition);
                var cnt = _fiberImpact.Count >= line.Count ? line.Count : _fiberImpact.Count;
                for (var i = 0; i < cnt; i++)
                {
                    result.Add(line.Keys[i], line.Values[i] & _fiberImpact.Values[Reverse ? (cnt - 1) - i : i]);
                }
            }
            return result;
        }

        [Obsolete]
        public SortedList<int, bool> CheckLine(SortedList<int, bool> line)
        {
            var result = new SortedList<int, bool>();
            lock (this)
            {
                //foreach (var kvp1 in line)
                //{
                //    var val = kvp1.Value ? 1 : 0;
                //    Console.Write($"|{kvp1.Key}:{val}");
                //}
                //Console.WriteLine("");
                //foreach (var kvp in _fiber)
                //{
                //    var val = kvp.Value ? 1 : 0;
                //    Console.Write($"|{kvp.Key}:{val}");
                //}
                //Console.WriteLine("\n");

                // Version CR1
                //if (_brokenPosition > 0)
                //    BrokenFiber(_brokenPosition);
                //var cnt = _fiber.Count >= line.Count ? line.Count : _fiber.Count;
                //for (var i = 0; i < cnt; i++)
                //    result.Add(line.Keys[i], line.Values[i] && _fiber.Values[Reverse ? (cnt - 1) - i : i]);
            }
            return result;
        }

        public void UpdateChecker(Datagram datagram)
        {
            lock (this)
            {
                ResetFiber();
                _brokenCount--;
                var impacts = datagram.Impacts.Where(i => (i.Start >= X1 && i.Start <= X2) || (i.Stop >= X1 && i.Stop <= X2)).ToList();
                if (!impacts.Any()) return;
                //Console.WriteLine("Checker impacts: "+impacts.Count);
                foreach (var impact in impacts)
                {
                    //var imp = impact;
                    //imp.Start -= 5;
                    //imp.Stop += 5;
                    for (var i = impact.Start - 5; i <= impact.Stop + 5; i++)
                    {
                        if (_fiberImpact.ContainsKey(i))
                            _fiberImpact[i] = impact;
                    }
                }

                if (IsBroken) BrokenFiber(_brokenPosition);
                //foreach (var val in _fiber.Select(kvp => kvp.Value ? 1 : 0))
                //{
                //    Console.Write($"|{val}");
                //}
                //Console.WriteLine("\n");
            }
        }

        public void BreakChecker(int x)
        {
            _brokenPosition = x;
            _brokenCount = 10;
        }

        private void ResetFiber()
        {
            _fiberImpact.Clear();
            for (var i = X1; i <= X2; i++)
                _fiberImpact.Add(i, new Impact() {Type = 0});
        }

        private void BrokenFiber(int x)
        {
            for (var i = x; i <= X2; i++)
                _fiberImpact.Remove(i);
            for (var i = x; i <= X2; i++)
                _fiberImpact.Add(i, new Impact() { Type = 2 });
        }
    }
}
