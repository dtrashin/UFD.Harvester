﻿using System;
using System.Configuration;

namespace UFD.Harvester.Receivers
{
    public class ReceiversConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Receivers")]
        public ReceiversCollection Receivers => (ReceiversCollection)base["Receivers"];


        public static ReceiversConfiguration GetConfig()
        {
            ReceiversConfiguration config = null;

            try
            {
                var execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config = (ReceiversConfiguration)execonfig.Sections["Harvester.Receivers"];
            }
            catch
            {
                // ignored
            }

            return config;
        }
    }

    [ConfigurationCollection(typeof(ReceiverConfiguration), AddItemName = "Receiver")]
    public class ReceiversCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ReceiverConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ReceiverConfiguration)(element));
        }

        public ReceiverConfiguration this[int idx] => (ReceiverConfiguration)BaseGet(idx);
    }

    public class ReceiverConfiguration : ConfigurationElement
    {
        [ConfigurationProperty("Id", IsRequired = true)]
        public Guid Id
        {
            get { return (Guid)this["Id"]; }
            set { this["Id"] = value; }
        }

        [ConfigurationProperty("Type", IsRequired = true)]
        public string Type
        {
            get { return (string)this["Type"]; }
            set { this["Type"] = value; }
        }

        [ConfigurationProperty("Port", IsRequired = true)]
        public int Port
        {
            get { return (int)this["Port"]; }
            set { this["Port"] = value; }
        }

        [ConfigurationProperty("Host", IsRequired = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        [ConfigurationProperty("ImpulseDuration", DefaultValue = 10)]
        public int ImpulseDuration
        {
            get { return (int) this["ImpulseDuration"]; }
            set { this["ImpulseDuration"] = value; }
        }

        //[ConfigurationProperty("BigEndian", IsRequired = true)]
        //public bool BigEndian
        //{
        //    get { return (bool)this["BigEndian"]; }
        //    set { this["BigEndian"] = value; }
        //}

        //[ConfigurationProperty("DeviceId", IsRequired = true)]
        //public ushort DeviceId
        //{
        //    get { return (ushort)this["DeviceId"]; }
        //    set { this["DeviceId"] = value; }
        //}

        //[ConfigurationProperty("ChanelId", IsRequired = true)]
        //public byte ChanelId
        //{
        //    get { return (byte)this["ChanelId"]; }
        //    set { this["ChanelId"] = value; }
        //}

        //[ConfigurationProperty("IsChecker", IsRequired = true)]
        //public bool IsChecker
        //{
        //    get { return (bool)this["IsChecker"]; }
        //    set { this["IsChecker"] = value; }
        //}

        //[ConfigurationProperty("PacketVersion", IsRequired = true)]
        //public int PacketVersion
        //{
        //    get { return (int)this["PacketVersion"]; }
        //    set { this["PacketVersion"] = value; }
        //}
    }
}
