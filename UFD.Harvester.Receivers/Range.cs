﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFD.Harvester.Datagrams;

namespace UFD.Harvester.Receivers
{
    public class Range
    {
        private readonly Checker _checker;
        //private SortedList<int, bool> _line = new SortedList<int, bool>();
        private SortedList<int, Impact> _lineImpact = new SortedList<int, Impact>();
        private int _brokenPosition = 0;

        public Guid Id { get; set; }
        public Guid ReceiverId { get; set; }
        public int X1 { get; set; }
        public int X2 { get; set; }
        public Checker Checker => _checker;

        public Range(Checker checker)
        {
            _checker = checker;
        }

        public List<Impact> CheckImpacts(List<Impact> impacts)
        {
            ResetLine();
            if (_checker != null)
            {
                foreach (var impact in impacts)
                {
                    for (var i = impact.Start; i <= impact.Stop; i++)
                    {
                        if (_lineImpact.ContainsKey(i))
                            _lineImpact[i] = impact;
                    }
                }
                if (_brokenPosition > 0)
                    BrokenLine(_brokenPosition);

                _lineImpact = _checker.CheckLine(_lineImpact);
                return GetImpacts(_lineImpact);
            }

            return impacts;
        }

        private void ResetLine()
        {
            _lineImpact.Clear(); //_line.Clear();  Version CR1
            for (var i = X1; i <= X2; i++)
                _lineImpact.Add(i, new Impact()); //_line.Add(i, false));  Version CR1
        }

        private List<Impact> GetImpacts(SortedList<int, Impact> line)
        {
            var imps = new List<Impact>();
            Impact? impact = null;
            var x1 = 0;
            for (var i = 0; i < line.Count; i++)
            {
                if (!impact.HasValue && line.Values[i].Type > 0)
                {
                    x1 = line.Keys[i];
                    impact = line.Values[i];
                }
                if (!impact.HasValue || line.Values[i].Type > 0) continue;
                var x2 = line.Keys[i - 1];

                imps.Add(new Impact
                {
                    Start = x1,
                    Stop = x2,
                    Distance = x2 - x1,
                    Point = (x1 + x2) / 2,
                    Power = impact.Value.Power,
                    MaxLevel = impact.Value.MaxLevel,
                    Integral = impact.Value.Integral,
                    RecognizeType = impact.Value.RecognizeType,
                    Type = impact.Value.Type
                });
                impact = null;
            }
            if (impact.HasValue)
                imps.Add(new Impact
                {
                    Start = x1,
                    Stop = line.Keys[line.Count - 1],
                    Distance = line.Keys[line.Count - 1] - x1,
                    Point = (x1 + line.Keys[line.Count - 1]) / 2,
                    Power = impact.Value.Power,
                    MaxLevel = impact.Value.MaxLevel,
                    Integral = impact.Value.Integral,
                    RecognizeType = impact.Value.RecognizeType,
                    Type = impact.Value.Type
                });
            return imps;
        }

        [Obsolete]
        private List<Impact> GetImpacts(SortedList<int, bool> line)
        {
            var imps = new List<Impact>();
            var inImpact = false;
            var x1 = 0;
            for (var i = 0; i < line.Count; i++)
            {
                if (!inImpact && line.Values[i])
                {
                    x1 = line.Keys[i];
                    inImpact = true;
                }
                if (!inImpact || line.Values[i]) continue;
                var x2 = line.Keys[i - 1];
                inImpact = false;
                imps.Add(new Impact
                {
                    Start = x1,
                    Stop = x2,
                    Distance = x2 - x1,
                    Point = (x1 + x2) / 2,
                    Power = 0
                });
            }
            if (inImpact)
                imps.Add(new Impact
                {
                    Start = x1,
                    Stop = line.Keys[line.Count - 1],
                    Distance = line.Keys[line.Count - 1] - x1,
                    Point = (x1 + line.Keys[line.Count - 1]) / 2,
                    Power = 0
                });
            return imps;
        }

        private void BrokenLine(int x)
        {
            for (var i = x; i <= X2; i++)
                _lineImpact.Remove(i); // _line.Remove(i);  Version CR1
            for (var i = x; i <= X2; i++)
                _lineImpact.Add(i, new Impact() { Type = 2 });  // _line.Add(i, true); Version CR1
        }
    }
}
