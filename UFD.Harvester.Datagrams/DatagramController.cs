﻿using System;
using System.Diagnostics;

namespace UFD.Harvester.Datagrams
{
    public class DatagramController
    {
        private readonly Guid _receiverId;

        private delegate Datagram GetDatagramDelegate(byte[] data);
        private GetDatagramDelegate _getDatagram;

        public DatagramController(Guid receiverId, int datagramVersion = 3)
        {
            _receiverId = receiverId;
            InitDatagramVersion(datagramVersion);
        }

        public Datagram GetDatagram(byte[] data)
        {
            try
            {
                if (_getDatagram == null)
                    throw new Exception("Invalid datagram version.");
                Datagram datagram = _getDatagram(data);
                //if (datagram.Impacts.Count > 0)
                //    Console.WriteLine(datagram.Impacts.Count);
                return datagram;
            }
            catch (Exception )
            {

                return null;
            }
        }

        private void InitDatagramVersion(int version)
        {
            switch (version)
            {
                case 1:
                    _getDatagram = GetDatagramV1; //CR1
                    break;
                case 2:
                    _getDatagram = GetDatagramV2; //CR2
                    break;
                case 3:
                    _getDatagram = GetDatagramV3; //Generator
                    break;
                default:
                    _getDatagram = null;
                    break;
            }
        }
        private Datagram GetDatagramV1(byte[] data)
        {
            if (data.Length == 0)
                return null;

            var datagram = new Datagram
            {
                ReceiverId = _receiverId
            };

            byte[] buffSize = new byte[4];
            Array.Copy(data, 0, buffSize, 0, 4);
            Array.Reverse(buffSize);
            int strSize = BitConverter.ToInt32(buffSize, 0);
            datagram.DatagramType = strSize == 12 ? (byte)0 : (byte)1;

            for (int i = 4 + strSize; i < data.Length; i += 8)
            {
                byte[] buffX1 = new byte[4];
                byte[] buffX2 = new byte[4];

                Array.Copy(data, i, buffX1, 0, 4);
                Array.Reverse(buffX1);

                if (datagram.DatagramType == 0)
                {
                    Array.Copy(data, i + 4, buffX2, 0, 4);
                    Array.Reverse(buffX2);
                }
                else
                {
                    Array.Copy(buffX1, 0, buffX2, 0, 4);
                    //buffX2 = BitConverter.GetBytes(int.MaxValue);
                }

                int x1 = BitConverter.ToInt32(buffX1, 0);
                int x2 = BitConverter.ToInt32(buffX2, 0);
                int recoType = BitConverter.ToInt32(buffX2, 0);

                datagram.AddImpact(x1, x2, recognizeType: recoType);
            }

            return datagram;
        }

        private Datagram GetDatagramV2(byte[] data)
        {
            if (data.Length == 0)
                return null;

            var datagram = new Datagram
            {
                ReceiverId = _receiverId
            };

            byte[] buffSize = new byte[4];
            Array.Copy(data, 0, buffSize, 0, 4);
            Array.Reverse(buffSize);
            int strSize = BitConverter.ToInt32(buffSize, 0);
            datagram.DatagramType = strSize == 32 ? (byte)0 : (byte)1;  // "ReflexDiagnostic" - 0, "LineBroken" - 1

            if (datagram.DatagramType == 0)
            {
                for (int i = 4 + strSize; i < data.Length; i += 24)
                {
                    byte[] buffX1 = new byte[4];
                    byte[] buffX2 = new byte[4];
                    byte[] buffMaxLevel = new byte[4];
                    byte[] buffIntegral = new byte[4];
                    byte[] buffReserved = new byte[8];

                    Array.Copy(data, i, buffX1, 0, 4);
                    Array.Reverse(buffX1);

                    if (datagram.DatagramType == 0)
                    {
                        Array.Copy(data, i + 4, buffX2, 0, 4);
                        Array.Reverse(buffX2);

                        Array.Copy(data, i + 8, buffMaxLevel, 0, 4);
                        Array.Reverse(buffMaxLevel);

                        Array.Copy(data, i + 12, buffIntegral, 0, 4);
                        Array.Reverse(buffIntegral);

                        Array.Copy(data, i + 16, buffReserved, 0, 8);
                        Array.Reverse(buffReserved);
                    }
                    else
                    {
                        Array.Copy(buffX1, 0, buffX2, 0, 4);
                        //buffX2 = BitConverter.GetBytes(int.MaxValue);
                    }

                    int x1 = BitConverter.ToInt32(buffX1, 0);
                    int x2 = BitConverter.ToInt32(buffX2, 0);
                    float maxLevel = BitConverter.ToSingle(buffMaxLevel, 0);
                    float integral = BitConverter.ToSingle(buffIntegral, 0);
                    long recognizeType = BitConverter.ToInt64(buffReserved, 0);

                    if (x1 != 0 || x2 != 0)
                        datagram.AddImpact(x1, x2, maxLevel, integral, recognizeType);
                }
            }
            else
            {
                byte[] buffX1 = new byte[4];
                Array.Copy(data, 4 + strSize, buffX1, 0, 4);
                Array.Reverse(buffX1);
                int x1 = BitConverter.ToInt32(buffX1, 0);
                datagram.AddBroken(x1, x1);
            }
            if (datagram.Impacts.Count > 0)
                return datagram;
            return datagram;
        }

        private Datagram GetDatagramV3(byte[] data)
        {
            var datagram = new Datagram
            {
                ReceiverId = _receiverId,
                DatagramType = data[3]
            };
//            if (datagram.DatagramType == 1)
//            Debug.WriteLine($"Datagram Type = {datagram.DatagramType}");

            for (int i = 4; i <= data.Length - 6; i += 6)
            {
                ushort x1 = BitConverter.ToUInt16(data, i);
                ushort x2 = BitConverter.ToUInt16(data, i + 2);
                ushort recognizeType = BitConverter.ToUInt16(data, i + 4);

                datagram.AddImpact(x1, x2, recognizeType: recognizeType);

            }

            return datagram;
        }
    }
}
