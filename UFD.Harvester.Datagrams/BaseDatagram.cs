﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace UFD.Harvester.Datagrams
{
    [Serializable]
    public abstract class BaseDatagram
    {
        public Guid ReceiverId { get; set; }
        public List<byte> Raw { get; set; }

        protected BaseDatagram()
        {
            Raw = new List<byte>();
        }

        public override string ToString()
        {
            return $"Receiver Id: {ReceiverId} Data: {DataToString()}";
        }

        public static byte[] Serialize<T>(T datagram)
        {
            using (var memoryStream = new MemoryStream())
            {
                (new BinaryFormatter()).Serialize(memoryStream, datagram);
                return memoryStream.ToArray();
            }
        }

        public static T Deserialize<T>(byte[] datagram)
        {
            using (var memoryStream = new MemoryStream(datagram))
                return (T)(new BinaryFormatter()).Deserialize(memoryStream);
        }

        internal abstract string DataToString();
    }
}
