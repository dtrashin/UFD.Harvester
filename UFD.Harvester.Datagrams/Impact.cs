﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFD.Harvester.Datagrams
{
    [Serializable]
    public struct Impact
    {
        public int Point;
        public decimal Power;
        public int Distance;
        public int Start;
        public int Stop;
        public float MaxLevel;
        public float Integral;
        public long RecognizeType;
        public byte Type; // 0 - нет активности, 1 - активность, 3 - обрыв
        public static Impact operator +(Impact impactLine, Impact impactChecker)
        {
            Impact result = new Impact(); // нет сработки
            if (impactLine.Type == 1 && impactChecker.Type > 0) result = impactLine; // основная линия подтверждена или обрыв на дублирующей линии
            else if (impactChecker.Type == 1 && impactLine.Type == 2) result = impactChecker; // активность на дублирующей линии при обрыве основной
            else if (impactLine.Type == 2 && impactChecker.Type == 2) result.Type = 2; // обрыв на этом участке по обоем линиям
            return result;
        }
        public static Impact operator &(Impact impactLine, Impact impactChecker)
        {
            Impact result = new Impact() { Type = 0 }; // нет сработки
            if (impactLine.Type == 0 || impactChecker.Type == 0) return result;
            
            if (impactLine.Type == 1 && impactChecker.Type > 0) result = impactLine; // основная линия подтверждена или обрыв на дублирующей линии
            else if (impactChecker.Type == 1 && impactLine.Type == 2) result = impactChecker; // активность на дублирующей линии при обрыве основной
            else if (impactLine.Type == 2 && impactChecker.Type == 2) result.Type = 2; // обрыв на этом участке по обоем линиям
            return result;
        }

    }
}
