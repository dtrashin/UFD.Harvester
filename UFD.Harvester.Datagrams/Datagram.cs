﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;

namespace UFD.Harvester.Datagrams
{
    [Serializable]
    public class Datagram : BaseDatagram
    {
        public byte DatagramType { get; set; }
        public List<int> Points { get { return Impacts.Select(impulse => impulse.Point).ToList(); } }
        public List<Impact> Impacts { get; } = new List<Impact>();

        public Datagram()
        {
            DatagramType = 0;
        }

        public void AddImpact(Impact impact)
        {
            Impacts.Add(impact);
        }

        public void AddImpact(List<Impact> impacts)
        {
            Impacts.AddRange(impacts);
        }

        public void AddBroken(int x1, int x2)
        {
            Impacts.Add(new Impact() {Start = x1, Stop = x2, Type = 2, Point = x1});
        }
        public void AddImpact(int x1, int x2, float maxLevel = 0, float integral = 0, long recognizeType = 0)
        {
            var impact = new Impact
            {
                Start = x1,
                Stop = x2,
                Distance = x2 - x1,
                Point = (x1 + x2) / 2,
                Power = 0,
                MaxLevel = maxLevel,
                Integral = integral,
                RecognizeType = recognizeType,
                Type = 1
            };
            Impacts.Add(impact);
        }

        [Obsolete]
        public void AddImpact(int point, int distance, decimal power)
        {
            var impact = new Impact
            {
                Point = point,
                Power = power,
                Distance = distance
            };
            var start = point - distance / 2;
            impact.Start = start < 0 ? 0 : start;
            impact.Stop = point + distance / 2;
            Impacts.Add(impact);
        }

        internal override string DataToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Impacts.Count);
            return sb.ToString();
        }
    }
}
