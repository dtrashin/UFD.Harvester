﻿using System.Configuration;

namespace UFD.Harvester.Emulator.Service
{
    internal class ServiceConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Host", IsRequired = true)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        public static ServiceConfiguration GetConfig()
        {
            Configuration execonfig = null;
            try
            {
                execonfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                return (ServiceConfiguration)execonfig.Sections["Harvester.Emulator.Service"];
            }
            catch
            {
                return null;
            }
        }
    }
}
