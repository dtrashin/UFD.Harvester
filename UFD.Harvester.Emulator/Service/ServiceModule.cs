﻿using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UFD.Harvester.Emulator.BL;
using UFD.Harvester.Emulator.BO;

namespace UFD.Harvester.Emulator.Service
{
    public class ServiceModule : NancyModule
    {
        public class Bootstrapper : DefaultNancyBootstrapper
        {
            protected virtual NancyInternalConfiguration InteranalConfiguration => NancyInternalConfiguration.Default;

            protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
            {
                pipelines.AfterRequest.AddItemToEndOfPipeline(ctx =>
                {
                    ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                        .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                        .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
                });
                base.RequestStartup(container, pipelines, context);
            }
        }

        public ServiceModule()
        {
            Get["/"] = parameters => $"{DateTime.Now} Welcome to harvester emulator service...";

            Post["/Integration/GetZones"] = parameters => IntegrationGetZones(Request.Body);
            Post["/Integration/GetZoneStatus"] = parameters => IntegrationGetZoneStatus(Request.Body);
            Post["/Integration/SetZoneStatus"] = parameters => IntegrationSetZoneStatus(Request.Body);
            Post["/Integration/GetActions"] = parameters => IntegrationGetActions(Request.Body);
        }

        private string IntegrationGetZones(Stream body)
        {
            var result = new Result<List<Zone>>();
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!CheckUserId(userId))
                    return "Error: user not registered.";
                var zones = ZoneController.GetController().GetZones();
                result.Success = true;
                result.Data = zones;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

        private string IntegrationGetZoneStatus(Stream body)
        {
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!CheckUserId(userId))
                    return "Error: user not registered.";
                var strZoneId = jBody["ZoneId"]?.ToString() ?? "";
                int zoneId;
                if (!int.TryParse(strZoneId, out zoneId))
                    return "Error: invalid parameters ZoneId.";
                var statuses = ZoneController.GetController().GetZoneStatus(zoneId);
                var arr = new JArray();
                foreach (var obj in statuses.Select(kvp => new JObject { { "ZoneId", new JValue(kvp.Key) }, { "Status", new JValue(kvp.Value) } }))
                {
                    arr.Add(obj);
                }

                var jobj = new JObject { { "ZoneStatus", arr } };
                return jobj.ToString();
            }
            catch (Exception)
            {
                return "Error: invalid parameters.";
            }
        }

        private string IntegrationSetZoneStatus(Stream body)
        {
            var result = new Result<ZoneState>();
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!CheckUserId(userId))
                    return "Error: user not registered.";
                var strZoneId = jBody["ZoneId"]?.ToString() ?? "";
                int zoneId;
                if (!int.TryParse(strZoneId, out zoneId))
                    throw new Exception("Invalid parameters ZoneId.");
                var strZoneStatus = jBody["Status"]?.ToString() ?? "";
                int zoneStatus;
                if (!int.TryParse(strZoneStatus, out zoneStatus))
                    throw new Exception("Invalid parameters Status.");
                var zoneState= ZoneController.GetController().SetZoneStatus(zoneId, (State)zoneStatus);
                ZoneState data = new ZoneState(zoneId, zoneState);
                result.Success = true;
                result.Data = data;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

        private string IntegrationGetActions(Stream body)
        {
            var result = new Result<List<ZoneAction>>();
            try
            {
                string strBody;
                using (var rdr = new StreamReader(body))
                    strBody = rdr.ReadToEnd();
                var jBody = JObject.Parse(strBody);
                if (jBody == null)
                    return "Error: invalid parameter UserId.";
                var strUserId = jBody["UserId"]?.ToString() ?? "";
                Guid userId;
                Guid.TryParse(strUserId, out userId);
                if (!CheckUserId(userId))
                    return "Error: user not registered.";
                var zas = ZoneController.GetController().GetActions();
                result.Success = true;
                result.Data = zas;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
                return JObject.Parse(JsonConvert.SerializeObject(result)).ToString();
            }
        }

        private bool CheckUserId(Guid userId)
        {
            return userId == Guid.Empty;
        }
    }
}
