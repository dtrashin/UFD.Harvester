﻿using Nancy.Hosting.Self;
using System;
using System.Threading;
using UFD.Harvester.Emulator.BL;
using UFD.Harvester.Emulator.Service;

namespace UFD.Harvester.Emulator
{
    class Emulator
    {
        static void Main(string[] args)
        {
            try
            {
                new Thread(ServiceStart).Start();
                new Thread(ZoneControllerStart).Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }

        private static void ServiceStart()
        {
            try
            {
                var config = ServiceConfiguration.GetConfig();
                if (null == config)
                    throw new NullReferenceException("Couldn't create harvester emulator configuration.");
                var conf = new HostConfiguration { AllowAuthorityFallback = true };
                using (var host = new NancyHost(conf, new Uri(config.Host)))
                {
                    host.Start();
                    Console.WriteLine($"Service is started on {config.Host}");
                    Thread.Sleep(Timeout.Infinite);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Service: {ex.Message}");
            }
        }

        private static void ZoneControllerStart()
        {
            try
            {
                ZoneController controller = ZoneController.GetController();
                controller.Start();
                Thread.Sleep(Timeout.Infinite);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Zone controller: {ex.Message}");
            }
        }
    }
}
