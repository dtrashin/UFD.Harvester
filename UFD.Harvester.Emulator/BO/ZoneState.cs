﻿namespace UFD.Harvester.Emulator.BO
{
    internal class ZoneState
    {
        /// <summary>
        /// Id зоны.
        /// </summary>
        public int ZoneId { get; }

        /// <summary>
        /// Состояние зоны.
        /// </summary>
        public State Status { get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="zoneId">Id зоны.</param>
        /// <param name="state">Состояние зоны.</param>
        public ZoneState(int zoneId, State state)
        {
            ZoneId = zoneId;
            Status = state;
        }
    }
}
