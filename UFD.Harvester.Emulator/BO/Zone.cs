﻿using System;

namespace UFD.Harvester.Emulator.BO
{
    /// <summary>
    /// Зона контроля периметра.
    /// </summary>
    internal class Zone
    {
        private static Random _randomizer = new Random(DateTime.Now.Millisecond);
        private State _state = State.Enabled;

        public event Action<int, State> StateChanged;
        public event Action<int, int, State> ImpulseDetected;

        /// <summary>
        /// Id зоны.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название зоны.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Координата начала зоны на сенсоре.
        /// </summary>
        public double FiberX1 { get; set; }

        /// <summary>
        /// Координата конца зоны на сенсоре.
        /// </summary>
        public double FiberX2 { get; set; }

        /// <summary>
        /// Id раздела, в который включена зона.
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// Id канала, с которого проходят данные об октивности.
        /// </summary>
        public Guid ReceiverId { get; set; }

        /// <summary>
        /// Состояние зоны.
        /// </summary>
        public State Status
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    OnStateChanged(Id, value);
                }
            }
        }

        /// <summary>
        /// Генерация воздействия.
        /// </summary>
        public void AddImpulse()
        {
            bool active = _randomizer.Next(DateTime.Now.Millisecond * 1000) % 123 == 0;
            if (active && Status == State.Enabled)
            {
                Status = State.Alarm;
                int x = _randomizer.Next((int)FiberX1, (int)FiberX2);
                OnImpulseDetected(Id, x, Status);
            }
        }

        private void OnStateChanged(int zoneId, State state)
        {
            var handler = StateChanged;
            handler?.Invoke(zoneId, state);
        }

        private void OnImpulseDetected(int zoneId, int x, State state)
        {
            var handler = ImpulseDetected;
            handler?.Invoke(zoneId, x, state);
        }
    }
}
