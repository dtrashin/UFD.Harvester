﻿using System;
using System.Globalization;

namespace UFD.Harvester.Emulator.BO
{
    /// <summary>
    /// Событие в зоне.
    /// </summary>
    internal class ZoneAction
    {
        /// <summary>
        /// Id события.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Время возникновения события.
        /// </summary>
        public string Time { get; }

        /// <summary>
        /// Id зоны, в которой произошло событие.
        /// </summary>
        public int ZoneId { get; set; }

        /// <summary>
        /// Координата по сенсору, в которой произошло событие.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public State Status { get; set; }

        /// <summary>
        /// Подтверждение обработки события.
        /// </summary>
        public bool Confirmed { get; }

        /// <summary>
        /// Тип объекта, который вызвал событие.
        /// </summary>
        public int RecognizeType { get; set; }

        /// <summary>
        /// Направление движения.
        /// </summary>
        public int Direction { get; set; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        public ZoneAction()
        {
            Id = Guid.NewGuid();
            Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            Confirmed = false;
        }
    }
}