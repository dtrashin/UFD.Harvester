﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using UFD.Harvester.Emulator.BO;

namespace UFD.Harvester.Emulator.BL
{
    /// <summary>
    /// Имитатор контроля состояний зон.
    /// </summary>
    internal class ZoneController
    {
        private List<Zone> _zones = new List<Zone>();
        private Timer _timer = new Timer();
        private static ZoneController _instance;
        private List<ZoneAction> _actions = new List<ZoneAction>();


        /// <summary>
        /// Закрытый конструктор.
        /// </summary>
        private ZoneController()
        {
            _timer.AutoReset = true;
            _timer.Interval = 500;
            _timer.Elapsed += OnTimerElapsed;
            ZoneInit();
        }

        /// <summary>
        /// Получение экземпляра приложения.
        /// </summary>
        /// <returns></returns>
        public static ZoneController GetController()
        {
            if (_instance == null)
                _instance = new ZoneController();
            return _instance;
        }

        /// <summary>
        /// Запуск имитации контроля периметра.
        /// </summary>
        public void Start()
        {
            _timer.Enabled = true;
        }

        /// <summary>
        /// Получение всех зон периметра.
        /// </summary>
        /// <returns></returns>
        public List<Zone> GetZones()
        {
            return _zones;
        }

        /// <summary>
        /// Получение состояния зон.
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public Dictionary<int, State> GetZoneStatus(int zoneId)
        {
            var result = new Dictionary<int, State>();
            if (zoneId > 0)
            {
                var zone = _zones.FirstOrDefault(z => z.Id == zoneId);
                result.Add(zoneId, zone?.Status ?? State.Unknown);
            }
            else
            {
                foreach (var zone in _zones)
                {
                    var z = zone;
                    result.Add(z.Id, z.Status);
                }
            }
            return result;
        }

        /// <summary>
        /// Установка статуса зоны.
        /// </summary>
        /// <param name="zoneId">Id зоны, для которой устанавливается статус.</param>
        /// <param name="status">Статус зоны, который будет установлен.</param>
        /// <returns></returns>
        public State SetZoneStatus(int zoneId, State status)
        {
            var zone = _zones.FirstOrDefault(z => z.Id == zoneId);
            if (zone == null)
                return State.Unknown;
            if (status != State.Disabled && status != State.Enabled)
                return zone.Status;
            zone.Status = status;
            return zone.Status;
        }

        /// <summary>
        /// Получение всех событий с момента последнего запроса.
        /// </summary>
        /// <returns></returns>
        public List<ZoneAction> GetActions()
        {
            lock (this)
            {
                List<ZoneAction> za = new List<ZoneAction>(_actions);
                _actions.Clear();
                return za;
            }
        }

        private void ZoneInit()
        {
            Guid receiverId = Guid.NewGuid();

            for (int i = 1; i <= 10; i++)
            {

                Zone zone = new Zone
                {
                    Id = i,
                    Description = "Зона " + i,
                    FiberX1 = (i - 1) * 100 + 1,
                    FiberX2 = i * 100,
                    Status = State.Enabled,
                    RegionId = 1,
                    ReceiverId = receiverId
                };
                zone.StateChanged += Zone_StateChanged;
                zone.ImpulseDetected += Zone_ImpulseDetected;
                _zones.Add(zone);
            }
        }

        private void Zone_ImpulseDetected(int zoneId, int x, State state)
        {
            lock (this)
            {
                _actions.Add(new ZoneAction { ZoneId = zoneId, X = x, Status = state, RecognizeType = 0, Direction = 0 });
            }
            Console.WriteLine($"Zone {zoneId} - {x} - {state}");
        }

        private void Zone_StateChanged(int zoneId, State state)
        {
            Console.WriteLine($"Zone {zoneId} change status: {state}");
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _zones.ForEach(zone => zone.AddImpulse());
        }
    }
}
